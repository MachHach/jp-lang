const markdownIt = require("markdown-it");
const markdownItAttrs = require('markdown-it-attrs');

const markdownLibrary = markdownIt({
    html: true,
}).use(markdownItAttrs, {
    allowedAttributes: ['id', 'class'],
});

const _badge = (text, cls) => {
    return `<span class="badge ${cls}">${text}</span>`;
};

const icls = (value) => {
    return _badge(value, 'bg-secondary');
};

const jlpt = (value) => {
    return _badge(`N${value}`, 'bg-danger');
};

const ruby = (value, hintValue) => {
    return `<ruby>${value}<rp>(</rp><rt>${hintValue}</rt><rp>)</rp></ruby>`;
};

module.exports = (eleventyConfig) => {
    eleventyConfig.setDataDeepMerge(true);
    // putting in "_includes" allow live reload upon file modified
    eleventyConfig.addPassthroughCopy({
        'src/_img/gitlab.svg': 'img/gitlab.svg',
        'src/_includes/main.css': 'css/main.css',
        'src/_includes/main.js': 'js/main.js',
    });
    eleventyConfig.setLibrary("md", markdownLibrary);

    for (const callable of [
        icls,
        jlpt,
        ruby,
    ]) {
        eleventyConfig.addFilter(callable.name, callable);
    }

    eleventyConfig.addPairedShortcode("verbWord", function(content) {
        return `<span class="text-1-25">${content}</span>`
    });
    eleventyConfig.addPairedShortcode("verbEg", function(content) {
        return `<span class="fst-italic gray">${content}</span>`
    });

    return {
        dir: {
            input: "src",
            output: "public",
        },
    };
};
