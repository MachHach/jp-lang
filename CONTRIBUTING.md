# Contribution Guidelines

## Development

### Setup development environment

Although there is no hard requirement, two types of environments will be described here.

#### Standard

The project recommends using a transient development environment with
[**Visual Studio Code - Remote Containers**][vscode-rcontainer-guide].

To set it up:

1. Install these software:
    - [Docker][docker-download]
    - [Visual Studio Code][vscode-home]
2. In VSCode, from menu -> View -> Extensions, search for and install [`ms-vscode-remote.remote-containers`][vscode-rcontainer-ext].
3. [Set up HTTPS credentials / SSH keys for the Git repository][vscode-rcontainer-guide-git]. Example for SSH in Windows host:
    ```powershell
    # Run PowerShell as Administrator

    # Make "OpenSSH Authentication Agent" Windows service start automatically upon boot
    Set-Service ssh-agent -StartupType Automatic
    Start-Service ssh-agent
    Get-Service ssh-agent
    ```
    ```powershell
    # Add SSH private key to the OpenSSH agent
    ssh-add $env:USERPROFILE/.ssh/id_rsa
    ```
4. Back to VSCode, press F1 -> *Remote-Containers: Clone Repository in Container Volume...* to create the environment.
5. If needed, set up Git user name and email for the repository, example:
    ```bash
    git config user.name "Foo Bar"
    git config user.email "foo.bar@example.com"
    ```
6. At the project's root directory, and install the Node packages required:
    ```bash
    npm install
    ```

#### Manual

Alternatively, set up a development environment directly.

1. Install these software:
    - [Git][git-download]
    - [Node.js 14][nodejs-download]
2. Clone the Git repository.
3. At the project's root directory, and install the Node packages required:
    ```bash
    npm install
    ```

### Workflow

The project uses [Eleventy][eleventy-home] Static Site Generator.

To start HTTP server for development (auto-reload upon file changes):

```bash
npx @11ty/eleventy --serve
```

To build the project (usually not required):

```bash
npx @11ty/eleventy
```

[docker-download]: https://docs.docker.com/get-docker/
[eleventy-home]: https://www.11ty.dev/
[git-download]: https://git-scm.com/downloads
[nodejs-download]: https://nodejs.org/en/download/
[vscode-home]: https://code.visualstudio.com/
[vscode-rcontainer-ext]: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers
[vscode-rcontainer-guide]: https://code.visualstudio.com/docs/remote/containers
[vscode-rcontainer-guide-git]: https://code.visualstudio.com/docs/remote/containers#_sharing-git-credentials-with-your-container
