---
layout: reader.html
title: 'Vocabulary: Counters'
navbar:
  vocab: 'active'
---

# Counters {{'助'|ruby:'じょ'}}{{'数'|ruby:'すう'}}{{'詞'|ruby:'し'}}

## Building floors {#building-floors}

{{5|jlpt}} {{'B00.L03'|icls}}

Roman | Word
----: | ---:
6|<span class="red">{{'六'|ruby:'ろっ'}}</span>{{'階'|ruby:'かい'}}
5|{{'五'|ruby:'ご'}}{{'階'|ruby:'かい'}}
4|{{'四'|ruby:'よん'}}{{'階'|ruby:'かい'}}
3|{{'三'|ruby:'さん'}}<span class="red">{{'階'|ruby:'がい'}}</span>
2|{{'二'|ruby:'に'}}{{'階'|ruby:'かい'}}
1|<span class="red">{{'一'|ruby:'いっ'}}</span>{{'階'|ruby:'かい'}}
B1|{{'地'|ruby:'ち'}}{{'下'|ruby:'か'}}<span class="red">{{'一'|ruby:'いっ'}}</span>{{'階'|ruby:'かい'}}
B2|{{'地'|ruby:'ち'}}{{'下'|ruby:'か'}}{{'二'|ruby:'に'}}{{'階'|ruby:'かい'}}
?|{{'何'|ruby:'なん'}}<span class="red">{{'階'|ruby:'がい'}}</span>

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## Things {#things}

{{5|jlpt}} {{'E01.L11'|icls}}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'ひと'}}</span>つ
2|<span class="red">{{'二'|ruby:'ふた'}}</span>つ
3|{{'三'|ruby:'みっ'}}つ
4|{{'四'|ruby:'よっ'}}つ
5|{{'五'|ruby:'いつ'}}つ
6|{{'六'|ruby:'むっ'}}つ
7|<span class="red">{{'七'|ruby:'なな'}}</span>つ
8|<span class="red">{{'八'|ruby:'やっ'}}</span>つ
9|{{'九'|ruby:'ここの'}}つ
10|<span class="red">{{'十'|ruby:'とお'}}</span>
?|いくつ

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## People {#people}

{{5|jlpt}} {{'E01.L11'|icls}}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'ひと'}}{{'人'|ruby:'り'}}</span>
2|<span class="red">{{'二'|ruby:'ふた'}}{{'人'|ruby:'り'}}</span>
3|{{'三'|ruby:'さん'}}{{'人'|ruby:'にん'}}
4|<span class="red">{{'四'|ruby:'よ'}}</span>{{'人'|ruby:'にん'}}
5|{{'五'|ruby:'ご'}}{{'人'|ruby:'にん'}}
6|{{'六'|ruby:'ろく'}}{{'人'|ruby:'にん'}}
7|{{'七'|ruby:'なな'}}{{'人'|ruby:'にん'}}
8|{{'八'|ruby:'はち'}}{{'人'|ruby:'にん'}}
9|{{'九'|ruby:'きゅう'}}{{'人'|ruby:'にん'}}
10|{{'十'|ruby:'じゅう'}}{{'人'|ruby:'にん'}}
?|{{'何'|ruby:'なん'}}{{'人'|ruby:'にん'}}

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## Frequency {#frequency}

{{5|jlpt}} {{'E01.L11'|icls}}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'いっ'}}</span>{{'回'|ruby:'かい'}}
2|{{'二'|ruby:'に'}}{{'回'|ruby:'かい'}}
3|{{'三'|ruby:'さん'}}{{'回'|ruby:'かい'}}
4|{{'四'|ruby:'よん'}}{{'回'|ruby:'かい'}}
5|{{'五'|ruby:'ご'}}{{'回'|ruby:'かい'}}
6|<span class="red">{{'六'|ruby:'ろっ'}}</span>{{'回'|ruby:'かい'}}
7|{{'七'|ruby:'なな'}}{{'回'|ruby:'かい'}}
8|<span class="red">{{'八'|ruby:'はっ'}}</span>{{'回'|ruby:'かい'}}
9|{{'九'|ruby:'きゅう'}}{{'回'|ruby:'かい'}}
10|<span class="red">{{'十'|ruby:'じゅっ'}}</span>{{'回'|ruby:'かい'}}、<span class="red">{{'十'|ruby:'じっ'}}</span>{{'回'|ruby:'かい'}}
?|{{'何'|ruby:'なん'}}{{'回'|ruby:'かい'}}

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}
