---
layout: reader.html
title: 'Vocabulary: Nouns'
navbar:
  vocab: 'active'
---

# Nouns {{'名'|ruby:'めい'}}{{'詞'|ruby:'し'}}

## General {#general}

Noun | Meaning | Refer
---- | ------- | -----
～{{'歳'|ruby:'さい'}}|~ years old|B00.L01
{{'何'|ruby:'なん'}}{{'歳'|ruby:'さい'}}|how old (informal)|B00.L01
おいくつ|how old (polite)|B00.L01
はい|yes (polite)|B00.L01
いいえ|no (polite)|B00.L01
そう|so|B00.L02
～{{'円'|ruby:'えん'}}|~ yen|B00.L03
いくら|how much|B00.L03
{{'次'|ruby:'つぎ'}}の|next|B00.L05
{{'何'|ruby:'なに'}}|what|B00.L06
{{'一'|ruby:'いっ'}}{{'緒'|ruby:'しょ'}}に|together|B00.L06
それから|after that, and then|B00.L06
ええ|yes|B00.L06
もう|already|B00.L07
まだ|not yet|B00.L07
これから|from now on, soon|B00.L07
どう|how|B00.L08
どうな～|what kind of ~|B00.L08
そして|and (used to connect sentences)|B00.L08
～が、～|~, but ~|B00.L08
～から|because ~|E01.L09
どうして|why|E01.L09
～や～［など］|~, ~, and so on|E01.L10
～ぐらい|about ~|E01.L11
どのくらい|how long|E01.L11
～だけ|only ~|E01.L11
～ごろ|about ~ (time)|E01.L13
{{'何'|ruby:'なに'}}か|something|E01.L13
どこか|somewhere, some place|E01.L13
{{'誰'|ruby:'だれ'}}か|someone|E01.L13
いつか|some time|E01.L13, I01.L27
すみません|I'm sorry|E01.L15
{{'皆'|ruby:'みな'}}さん|everybody|E01.L15
～{{'番'|ruby:'ばん'}}|number ~|E01.L16
どうやって|in what way, how|E01.L16
{{'次'|ruby:'つぎ'}}に|next, as a next step|E01.L16
２、３{{'日'|ruby:'にち'}}|two or three days|E02.L17
２、３～|two or three ~ (where ~ is a counter suffix)|E02.L17
ですから|therefore, so|E02.L17
～メートル|~ meter|E02.L18
{{'日'|ruby:'ひ'}}|day, date|E02.L19
でも|but (informal)|E02.L19
うん|yes (informal)|E02.L20
ううん|no (informal)|E02.L20
{{'初'|ruby:'はじ'}}め|the beginning|E02.L20
{{'終'|ruby:'お'}}わり|the end of ~, The End|E02.L20
～けど|~, but (informal of が)|E02.L20
５{{'月'|ruby:'がつ'}}の{{'初'|ruby:'はじ'}}めごろ|beginning of May (1st-10th)|E02.L20
５{{'月'|ruby:'がつ'}}の{{'中'|ruby:'なか'}}ごろ|middle of May (11th-20th)|E02.L20
５{{'月'|ruby:'がつ'}}の{{'終'|ruby:'お'}}わりごろ|end of May (21st-31st)|E02.L20
そんなに|not so (used with negatives)|E02.L21
～について|about ~, concerning ~|E02.L21
～{{'目'|ruby:'め'}}|the ~th (indicating order)|E02.L23
{{'自'|ruby:'じ'}}{{'分'|ruby:'ぶん'}}で|by oneself|E02.L24
［～の］こと|thing (about ~)|E02.L25
いつでも|any time|I01.L26
どこでも|anywhere|I01.L26
{{'誰'|ruby:'だれ'}}でも|anybody|I01.L26
{{'何'|ruby:'なん'}}でも|anything|I01.L26
～{{'後'|ruby:'ご'}}|after ~ (duration of time, e.g. {{'一'|ruby:'いち'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}{{'後'|ruby:'ご'}}に)|I01.L27
～しか|only ~ (used with negatives)|I01.L27
{{'例'|ruby:'たと'}}えば|for example|I01.L27
{{'自'|ruby:'じ'}}{{'分'|ruby:'ぶん'}}|oneself|I01.L27, I01.L28
それに|in addition|I01.L28
それで|and so|I01.L28

{.table .table-hover .table-sm .table-ruby-1}

## People {{'人'|ruby:'ひと'}} {#people}

Noun | Meaning | Refer
---- | ------- | -----
{{'人'|ruby:'ひと'}}|person, people|B00.L05
{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}|friend|B00.L05
{{'彼'|ruby:'かれ'}}|he, boyfriend, lover|B00.L05
{{'彼'|ruby:'かれ'}}し|boyfriend|B00.L05
{{'彼'|ruby:'かの'}}{{'女'|ruby:'じょ'}}|she, girlfriend, lover|B00.L05
{{'家'|ruby:'か'}}{{'族'|ruby:'ぞく'}}|family|B00.L05
{{'一人'|ruby:'ひとり'}}で|alone, by oneself|B00.L05
{{'父'|ruby:'ちち'}}|(my) father|B00.L07
{{'母'|ruby:'はは'}}|(my) mother|B00.L07
お{{'父'|ruby:'とう'}}さん|(own / someone else's) father|B00.L07
お{{'母'|ruby:'かあ'}}さん|(own / someone else's) mother|B00.L07
ご{{'主'|ruby:'しゅ'}}{{'人'|ruby:'じん'}}|(someone else's) husband|E01.L09
{{'夫'|ruby:'おっと'}} / {{'主'|ruby:'しゅ'}}{{'人'|ruby:'じん'}}|(my) husband|E01.L09
{{'奥'|ruby:'おく'}}さん|(someone else's) wife|E01.L09
{{'妻'|ruby:'つま'}} / {{'家'|ruby:'か'}}{{'内'|ruby:'ない'}}|(my) wife|E01.L09
{{'子'|ruby:'こ'}}{{'供'|ruby:'ども'}}|child|E01.L09
{{'男'|ruby:'おとこ'}}の{{'人'|ruby:'ひと'}}|man|E01.L10
{{'女'|ruby:'おんな'}}の{{'人'|ruby:'ひと'}}|woman|E01.L10
{{'男'|ruby:'おとこ'}}の{{'子'|ruby:'こ'}}|boy|E01.L10
{{'女'|ruby:'おんな'}}の{{'子'|ruby:'こ'}}|girl|E01.L10
{{'両'|ruby:'りょう'}}{{'親'|ruby:'しん'}}|parents|E01.L11
{{'兄'|ruby:'きょう'}}{{'弟'|ruby:'だい'}}|brothers and sisters|E01.L11
{{'兄'|ruby:'あに'}}|(my) elder brother|E01.L11
お{{'兄'|ruby:'にい'}}さん|(someone else's) elder brother|E01.L11
{{'姉'|ruby:'あね'}}|(my) elder sister|E01.L11
お{{'姉'|ruby:'ねえ'}}さん |(someone else's) elder sister|E01.L11
{{'弟'|ruby:'おとうと'}}|(my) younger brother|E01.L11
{{'弟'|ruby:'おとうと'}}さん|(someone else's) younger brother|E01.L11
{{'妹'|ruby:'いもうと'}}|(my) younger sister|E01.L11
{{'妹'|ruby:'いもうと'}}さん|(someone else's) younger sister|E01.L11
おじいさん|(someone else's) grandfather, old man|E02.L24
おじいちゃん|(my) grandfather|E02.L24
おばあさん|(someone else's) grandmother, old woman|E02.L24
おばあちゃん|(my) grandmother|E02.L24
{{'方'|ruby:'かた'}}|person (polite of {{'人'|ruby:'ひと'}})|I01.L27
{{'子'|ruby:'こ'}}{{'供'|ruby:'ども'}}たち|children|I01.L27
{{'息子'|ruby:'むすこ'}}|(my) son|I01.L28
{{'息子'|ruby:'むすこ'}}さん|(someone else's) son|I01.L28
{{'娘'|ruby:'むすめ'}}|(my) daughter|I01.L28
{{'娘'|ruby:'むすめ'}}さん|(someone else's) daughter|I01.L28

{.table .table-hover .table-sm .table-ruby-1}

## Animal {{'動'|ruby:'どう'}}{{'物'|ruby:'ぶつ'}} {#animal}

Noun | Meaning | Refer
---- | ------- | -----
{{'犬'|ruby:'いぬ'}}|dog|E01.L10
{{'猫'|ruby:'ねこ'}}|cat|E01.L10
パンダ|panda|E01.L10
{{'象'|ruby:'ぞう'}}|elephant|E01.L10
{{'動'|ruby:'どう'}}{{'物'|ruby:'ぶつ'}}|animal|E02.L18
{{'馬'|ruby:'うま'}}|horse|E02.L18
カンガルー|kangaroo|E02.L21
ペット|pet|I01.L27
{{'鳥'|ruby:'とり'}}|bird|I01.L27

{.table .table-hover .table-sm .table-ruby-1}

## Body part {#body-part}

Noun | Meaning | Refer
---- | ------- | -----
{{'手'|ruby:'て'}}|hand, arm|B00.L07
{{'表'|ruby:'ひょう'}}{{'情'|ruby:'じょう'}}|facial expression|E01.L16
{{'体'|ruby:'からだ'}}|body, health|E01.L16
{{'頭'|ruby:'あたま'}}|head, brain|E01.L16
{{'髪'|ruby:'かみ'}}|hair|E01.L16
{{'顔'|ruby:'かお'}}|face|E01.L16
{{'目'|ruby:'め'}}|eye|E01.L16
{{'耳'|ruby:'みみ'}}|ear|E01.L16
{{'鼻'|ruby:'はな'}}|nose|E01.L16
{{'口'|ruby:'くち'}}|mouth|E01.L16
{{'歯'|ruby:'は'}}|tooth|E01.L16
お{{'腹'|ruby:'なか'}}|stomach|E01.L16
{{'足'|ruby:'あし'}}|leg, foot|E01.L16
{{'背'|ruby:'せ'}}|height|E01.L16
しっぽ|tail|E01.L16
{{'肌'|ruby:'はだ'}}|skin|E01.L16
{{'喉'|ruby:'のど'}}|throat|E02.L17

{.table .table-hover .table-sm .table-ruby-1}

## Apparel {#apparel}

Noun | Meaning | Refer
---- | ------- | -----
{{'靴'|ruby:'くつ'}}|shoes|B00.L03
ネクタイ|tie, necktie|B00.L03
シャツ|shirt|B00.L07
{{'服'|ruby:'ふく'}}|clothes|E01.L15
{{'上'|ruby:'うわ'}}{{'着'|ruby:'ぎ'}}|jacket, outerwear|E02.L17
{{'下'|ruby:'した'}}{{'着'|ruby:'ぎ'}}|underwear|E02.L17
{{'着'|ruby:'き'}}{{'物'|ruby:'もの'}}|kimono (traditional Japanese attire)|E02.L20
コート|coat|E02.L22
セーター|sweater|E02.L22
スーツ|suit|E02.L22
{{'帽'|ruby:'ぼう'}}{{'子'|ruby:'し'}}|hat, cap|E02.L22
{{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}|glasses|E02.L22

{.table .table-hover .table-sm .table-ruby-1}

## Occupation {{'職'|ruby:'しょく'}}{{'業'|ruby:'ぎょう'}} {#occupation}

Noun | Meaning | Refer
---- | ------- | -----
{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}|teacher, instructor (not used when referring to one's own job)|B00.L01
{{'教'|ruby:'きょう'}}{{'師'|ruby:'し'}}|teacher, instructor|B00.L01
{{'学'|ruby:'がく'}}{{'生'|ruby:'せい'}}|student|B00.L01
{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}{{'員'|ruby:'いん'}}|company employee|B00.L01
～{{'社'|ruby:'しゃ'}}{{'員'|ruby:'いん'}}|employee of ~ company (used with a company's name)|B00.L01
{{'銀'|ruby:'ぎん'}}{{'行'|ruby:'こう'}}{{'員'|ruby:'いん'}}|bank employee|B00.L01
{{'医'|ruby:'い'}}{{'者'|ruby:'しゃ'}}|(medical) doctor|B00.L01
{{'研'|ruby:'けん'}}{{'究'|ruby:'きゅう'}}{{'者'|ruby:'しゃ'}}|researcher, scholar|B00.L01
{{'留'|ruby:'りゅう'}}{{'学'|ruby:'がく'}}{{'生'|ruby:'せい'}}|foreign student|E01.L11
{{'歯'|ruby:'は'}}{{'医'|ruby:'い'}}{{'者'|ruby:'しゃ'}}|dentist, dentist's|E01.L15
{{'課'|ruby:'か'}}{{'長'|ruby:'ちょう'}}|section head|E02.L18
{{'部'|ruby:'ぶ'}}{{'長'|ruby:'ちょう'}}|department head|E02.L18
{{'社'|ruby:'しゃ'}}{{'長'|ruby:'ちょう'}}|company president|E02.L18
{{'宇'|ruby:'う'}}{{'宙'|ruby:'ちゅう'}}{{'飛'|ruby:'ひ'}}{{'行'|ruby:'こう'}}{{'士'|ruby:'し'}}|astronaut|I01.L26
{{'主'|ruby:'しゅ'}}{{'人'|ruby:'じん'}}{{'公'|ruby:'こう'}}|hero, heroine|I01.L27
{{'歌'|ruby:'か'}}{{'手'|ruby:'しゅ'}}|singer|I01.L28
{{'小'|ruby:'しょう'}}{{'説'|ruby:'せつ'}}{{'家'|ruby:'か'}}|novelist|I01.L28
～{{'家'|ruby:'か'}}|-er, -ist, etc. (e.g. painter, novelist)|I01.L28
{{'駅'|ruby:'えき'}}{{'員'|ruby:'いん'}}|station attendant|I01.L29
レポーター|reporter|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Place {{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}} {#place}

Noun | Meaning | Refer
---- | ------- | -----
{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}|university|B00.L01
{{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}|hospital|B00.L01
{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}|classroom|B00.L03
{{'食'|ruby:'しょく'}}{{'堂'|ruby:'どう'}}|dining hall, canteen|B00.L03
{{'事'|ruby:'じ'}}{{'務'|ruby:'む'}}{{'所'|ruby:'しょ'}}|office|B00.L03
{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}{{'室'|ruby:'しつ'}}|conference room, meeting room|B00.L03
{{'受'|ruby:'うけ'}}{{'付'|ruby:'つけ'}}|reception desk|B00.L03
ロビー|lobby|B00.L03
{{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}|room|B00.L03
お{{'手'|ruby:'て'}}{{'洗'|ruby:'あら'}}い|toilet, rest room (polite)|B00.L03
{{'階'|ruby:'かい'}}{{'段'|ruby:'だん'}}|staircase|B00.L03
エレベーター|lift, elevator|B00.L03
エスカレーター|escalator|B00.L03
「お」{{'国'|ruby:'くに'}}|country|B00.L03
{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}|company|B00.L03
{{'家'|ruby:'うち'}}|house, home|B00.L03
{{'売'|ruby:'うり'}}{{'場'|ruby:'ば'}}|department, counter (in a department store)|B00.L03
{{'地'|ruby:'ち'}}{{'下'|ruby:'か'}}|basement|B00.L03
デパート|department store|B00.L04
{{'銀'|ruby:'ぎん'}}{{'行'|ruby:'こう'}}|bank|B00.L04
{{'郵'|ruby:'ゆう'}}{{'便'|ruby:'びん'}}{{'局'|ruby:'きょく'}}|post office|B00.L04
{{'図'|ruby:'と'}}{{'書'|ruby:'しょ'}}{{'館'|ruby:'かん'}}|library|B00.L04
{{'美'|ruby:'び'}}{{'術'|ruby:'じゅつ'}}{{'館'|ruby:'かん'}}|art museum, art gallery|B00.L04
{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}|school|B00.L05
スーパー|supermarket|B00.L05
{{'駅'|ruby:'えき'}}|station|B00.L05
{{'店'|ruby:'みせ'}}|shop, store|B00.L06
{{'庭'|ruby:'にわ'}}|garden|B00.L06
{{'山'|ruby:'やま'}}|mountain|B00.L08
{{'町'|ruby:'まち'}}|town, city|B00.L08
{{'所'|ruby:'ところ'}}|place|B00.L08
{{'寮'|ruby:'りょう'}}|dormitory|B00.L08
レストラン|restaurant|B00.L08
コンビニ|convenience store|E01.L10
{{'公'|ruby:'こう'}}{{'園'|ruby:'えん'}}|park|E01.L10
{{'喫'|ruby:'きっ'}}{{'茶'|ruby:'さ'}}{{'店'|ruby:'てん'}}|cafe, coffee shop|E01.L10
～{{'屋'|ruby:'や'}}|~ shop, ~ store|E01.L10
{{'乗'|ruby:'の'}}り{{'場'|ruby:'ば'}}|a fixed place to catch taxis, trains, etc|E01.L10
{{'県'|ruby:'けん'}}|prefecture|E01.L10
{{'外'|ruby:'がい'}}{{'国'|ruby:'こく'}}|foreign country|E01.L11
ホテル|hotel|E01.L12
{{'空'|ruby:'くう'}}{{'港'|ruby:'こう'}}|airport|E01.L12
{{'海'|ruby:'うみ'}}|sea, ocean|E01.L12
{{'世'|ruby:'せ'}}{{'界'|ruby:'かい'}}|world|E01.L12
プール|swimming pool|E01.L13
{{'川'|ruby:'かわ'}}|river|E01.L13
{{'市'|ruby:'し'}}{{'役'|ruby:'やく'}}{{'所'|ruby:'しょ'}}|municipal office, city hall|E01.L15
{{'高'|ruby:'こう'}}{{'校'|ruby:'こう'}}|senior high school|E01.L15
［お］{{'寺'|ruby:'てら'}}|Buddhist temple|E01.L16
{{'神'|ruby:'じん'}}{{'社'|ruby:'じゃ'}}|Shinto shrine|E01.L16
ダイニングキッチン|kitchen with a dining area|E02.L22
{{'和'|ruby:'わ'}}{{'室'|ruby:'しつ'}}|Japanese-style room|E02.L22
{{'道'|ruby:'みち'}}|road, way|E02.L23
{{'交'|ruby:'こう'}}{{'差'|ruby:'さ'}}{{'点'|ruby:'てん'}}|crossroads|E02.L23
{{'角'|ruby:'かど'}}|corner|E02.L23
{{'駐'|ruby:'ちゅう'}}{{'車'|ruby:'しゃ'}}{{'場'|ruby:'じょう'}}|car park, parking lot|E02.L23
{{'田舎'|ruby:'いなか'}}|countryside, hometown|E02.L25
フリーマーケット|flea market|I01.L26
{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}|place|I01.L26
{{'国'|ruby:'こっ'}}{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}{{'事'|ruby:'じ'}}{{'堂'|ruby:'どう'}}|the Diet Building|I01.L26
{{'宇'|ruby:'う'}}{{'宙'|ruby:'ちゅう'}}|space, universe|I01.L26
{{'宇'|ruby:'う'}}{{'宙'|ruby:'ちゅう'}}ステーション|space station|I01.L26
{{'家'|ruby:'いえ'}}|house, home|I01.L27
マンション|condominium, apartment house, blocks of flats|I01.L27
キッチン|kitchen|I01.L27
パーティールーム|party room|I01.L27
{{'空'|ruby:'そら'}}|sky|I01.L27
{{'美'|ruby:'び'}}{{'容'|ruby:'よう'}}{{'院'|ruby:'いん'}}|hair salon|I01.L28
{{'台'|ruby:'だい'}}{{'所'|ruby:'どころ'}}|kitchen|I01.L28
{{'体'|ruby:'たい'}}{{'育'|ruby:'いく'}}{{'館'|ruby:'かん'}}|gymnasium|I01.L28
{{'交'|ruby:'こう'}}{{'番'|ruby:'ばん'}}|police box|I01.L29
{{'駅'|ruby:'えき'}}{{'前'|ruby:'まえ'}}|the area in front of a station|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Direction {#direction}

Noun | Meaning | Refer
---- | ------- | -----
{{'上'|ruby:'うえ'}}|on, above, over|E01.L10
{{'下'|ruby:'した'}}|under, below, beneath|E01.L10
{{'前'|ruby:'まえ'}}|front, before|E01.L10
{{'後'|ruby:'うし'}}ろ|back, behind|E01.L10
{{'右'|ruby:'みぎ'}}|right (side)|E01.L10
{{'左'|ruby:'ひだり'}}|left (side)|E01.L10
{{'中'|ruby:'なか'}}|in, inside|E01.L10
{{'外'|ruby:'そと'}}|outside|E01.L10
{{'隣'|ruby:'となり'}}|next, next door|E01.L10
{{'近'|ruby:'ちか'}}く|near, vicinity|E01.L10
{{'間'|ruby:'あいだ'}}|between, among|E01.L10
{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}{{'下'|ruby:'した'}}|the bottom|E01.L10
～{{'側'|ruby:'がわ'}}|~ side (e.g. {{'外'|ruby:'そと'}}{{'側'|ruby:'がわ'}})|I01.L29
～{{'辺'|ruby:'へん'}}|around ~, ~ about (e.g. その{{'辺'|ruby:'へん'}})|I01.L29
{{'西'|ruby:'にし'}}|west|I01.L29
～の{{'方'|ruby:'ほう'}}|direction of ~|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Country {{'国'|ruby:'くに'}} {#country}

Noun | Meaning | Refer
---- | ------- | -----
アメリカ|U.S.A|B00.L01
イギリス|U.K.|B00.L01
インド|India|B00.L01
インドネシア|Indonesia|B00.L01
{{'韓'|ruby:'かん'}}{{'国'|ruby:'こく'}}|South Korea|B00.L01
タイ|Thailand|B00.L01
{{'中'|ruby:'ちゅう'}}{{'国'|ruby:'ごく'}}|China|B00.L01
ドイツ|Germany|B00.L01
{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}|Japan|B00.L01
ブラジル|Brazil|B00.L01
イタリア|Italy|B00.L03
スイス|Switzerland|B00.L03
フランス|France|B00.L03
ジャカルタ|Jakarta|B00.L03
バンコク|Bangkok|B00.L03
ベルリン|Berlin|B00.L03
ニューヨーク|New York|B00.L04
ペキン|Beijing|B00.L04
ロサンゼルス|Los Angeles|B00.L04
ロンドン|London|B00.L04
メキシコ|Mexico|B00.L06
スペイン|Spain|B00.L07
シャンハイ|Shanghai|B00.L08
オーストラリア|Australia|E01.L11
ホンコン|Hong Kong|E01.L12
シンガポール|Singapore|E01.L12
バンドン|Bandung (in Indonesia)|E01.L16
フランケン|Franken (in Germany)|E01.L16
ベラクルス|Veracruz (in Mexico)|E01.L16
{{'梅'|ruby:'うめ'}}{{'田'|ruby:'だ'}}|Umeda (a district in Osaka)|E01.L16
{{'秋'|ruby:'あき'}}{{'葉'|ruby:'は'}}{{'原'|ruby:'ばら'}}|Akihabara (a district in Tokyo)|E02.L18
{{'吉'|ruby:'よし'}}{{'野'|ruby:'の'}}{{'山'|ruby:'やま'}}|Mt. Yoshino (a mountain in Nara prefecture)|E02.L21
パリ|Paris|E02.L22
{{'万'|ruby:'ばん'}}{{'里'|ruby:'り'}}の{{'長'|ruby:'ちょう'}}{{'城'|ruby:'じょう'}}|the Great Wall of China|E02.L22
ベトナム|Vietnam|E02.L25
{{'新'|ruby:'しん'}}{{'宿'|ruby:'じゅく'}}|Shinjuku (a district in Tokyo)|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Vehicle {{'乗'|ruby:'のり'}}{{'物'|ruby:'もの'}} {#vehicle}

Noun | Meaning | Refer
---- | ------- | -----
{{'車'|ruby:'くるま'}}|car, vehicle|B00.L02
{{'飛'|ruby:'ひ'}}{{'行'|ruby:'こう'}}{{'機'|ruby:'き'}}|aeroplane, airplane|B00.L05
{{'船'|ruby:'ふね'}}|ship|B00.L05
{{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}|electric train|B00.L05
{{'地'|ruby:'ち'}}{{'下'|ruby:'か'}}{{'鉄'|ruby:'てつ'}}|underground, subway|B00.L05
{{'新'|ruby:'しん'}}{{'幹'|ruby:'かん'}}{{'線'|ruby:'せん'}}|the Shinkansen, the bullet train|B00.L05
バス|bus|B00.L05
タクシー|taxi|B00.L05
{{'自'|ruby:'じ'}}{{'転'|ruby:'てん'}}{{'車'|ruby:'しゃ'}}|bicycle|B00.L05
{{'歩'|ruby:'ある'}}いて|on foot|B00.L05
{{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}|local (train)|B00.L05
{{'急'|ruby:'きゅう'}}{{'行'|ruby:'こう'}}|rapid (train)|B00.L05
{{'特'|ruby:'とっ'}}{{'急'|ruby:'きゅう'}}|express (train)|B00.L05
{{'自'|ruby:'じ'}}{{'動'|ruby:'どう'}}{{'車'|ruby:'しゃ'}}|car, automobile|E02.L21
{{'宇'|ruby:'う'}}{{'宙'|ruby:'ちゅう'}}{{'船'|ruby:'せん'}}|spaceship|I01.L26

{.table .table-hover .table-sm .table-ruby-1}

## Thing {{'物'|ruby:'もの'}} {#thing}

Noun | Meaning | Refer
---- | ------- | -----
{{'本'|ruby:'ほん'}}|book|B00.L02
{{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}|dictionary|B00.L02
{{'雑'|ruby:'ざっ'}}{{'誌'|ruby:'し'}}|magazine|B00.L02
{{'新'|ruby:'しん'}}{{'聞'|ruby:'ぶん'}}|newspaper|B00.L02
ノート|notebook|B00.L02
{{'手'|ruby:'て'}}{{'帳'|ruby:'ちょう'}}|personal organizer|B00.L02
{{'名'|ruby:'めい'}}{{'刺'|ruby:'し'}}|business card|B00.L02
カード|(credit) card|B00.L02
{{'鉛'|ruby:'えん'}}{{'筆'|ruby:'ぴつ'}}|pencil|B00.L02
ボールペン|ballpoint pen|B00.L02
シャープペンシル / シャーペン|mechanical pencil, propelling pencil|B00.L02
{{'鍵'|ruby:'かぎ'}}|key|B00.L02
{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}|watch, clock|B00.L02
{{'傘'|ruby:'かさ'}}|umbrella|B00.L02
{{'鞄'|ruby:'かばん'}}|bag, briefcase|B00.L02
CD|CD, compact disc|B00.L02
テレビ|television|B00.L02
ラジオ|radio|B00.L02
カメラ|camera|B00.L02
コンピューター|computer|B00.L02
{{'机'|ruby:'つくえ'}}|desk|B00.L02
{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}|chair|B00.L02
チョコレート|chocolate|B00.L02
コーヒー|coffee|B00.L02
「お」{{'土産'|ruby:'みやげ'}}|souvenir, present|B00.L02
{{'英'|ruby:'えい'}}{{'語'|ruby:'ご'}}|the English language|B00.L02
{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}|the Japanese language|B00.L02
～{{'語'|ruby:'ご'}}|~ language|B00.L02
{{'自'|ruby:'じ'}}{{'動'|ruby:'どう'}}{{'販'|ruby:'はん'}}{{'売'|ruby:'ばい'}}{{'機'|ruby:'き'}}|vending machine|B00.L03
{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}|telephone handset, telephone call|B00.L03
ワイン|wine|B00.L03
{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}|film, movie|B00.L04
たばこ|tobacco, cigarette|B00.L06
{{'手'|ruby:'て'}}{{'紙'|ruby:'がみ'}}|letter|B00.L06
レポート|report|B00.L06
{{'写'|ruby:'しゃ'}}{{'真'|ruby:'しん'}}|photograph|B00.L06
ビデオ|video (tape), video deck|B00.L06
{{'箸'|ruby:'はし'}}|chopsticks|B00.L07
スプーン|spoon|B00.L07
ナイフ|knife|B00.L07
フォーク|fork|B00.L07
はさみ|scissors|B00.L07
パソコン|personal computer|B00.L07
ケータイ|mobile phone, cell phone|B00.L07
メール|e-mail|B00.L07
{{'年'|ruby:'ねん'}}{{'賀'|ruby:'が'}}{{'状'|ruby:'じょう'}}|New Year's greeting card|B00.L07
パンチ|punch|B00.L07
ホッチキス|stapler|B00.L07
セロテープ|Sellotape, Scotch tape, clear adhesive tape|B00.L07
{{'消'|ruby:'け'}}しゴム|rubber, eraser|B00.L07
{{'紙'|ruby:'かみ'}}|paper|B00.L07
{{'花'|ruby:'はな'}}|flower, blossom|B00.L07
プレゼント|present, gift|B00.L07
{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}|luggage, baggage, parcel|B00.L07
お{{'金'|ruby:'かね'}}|money|B00.L07
{{'切'|ruby:'きっ'}}{{'符'|ruby:'ぷ'}}|ticket (bus, train)|B00.L07
{{'桜'|ruby:'さくら'}}|cherry (blossom)|B00.L08
{{'音'|ruby:'おん'}}{{'楽'|ruby:'がく'}}|music|E01.L09
{{'歌'|ruby:'うた'}}|song|E01.L09
クラシック|classical music|E01.L09
ジャズ|jazz|E01.L09
{{'絵'|ruby:'え'}}|picture, drawing|E01.L09
{{'字'|ruby:'じ'}}|letter, character|E01.L09
{{'漢'|ruby:'かん'}}{{'字'|ruby:'じ'}}|Chinese character|E01.L09
ひらがな|hiragana script|E01.L09
かたかな|katakana script|E01.L09
ローマ{{'字'|ruby:'じ'}}|the Roman alphabet|E01.L09
{{'細'|ruby:'こま'}}かいお{{'金'|ruby:'かね'}}|small change (coins, e.g. 1-500 yen)|E01.L09
チケット|ticket (aeroplane, ship)|E01.L09
{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}|time|E01.L09
{{'用'|ruby:'よう'}}{{'事'|ruby:'じ'}}|something to do, errand|E01.L09
{{'約'|ruby:'やく'}}{{'束'|ruby:'そく'}}|appointment, promise|E01.L09
{{'木'|ruby:'き'}}|tree, wood|E01.L10
{{'物'|ruby:'もの'}}|thing|E01.L10
{{'電'|ruby:'でん'}}{{'池'|ruby:'ち'}}|battery|E01.L10
{{'箱'|ruby:'はこ'}}|box|E01.L10
スイッチ|switch|E01.L10
{{'冷'|ruby:'れい'}}{{'蔵'|ruby:'ぞう'}}{{'庫'|ruby:'こ'}}|refrigerator|E01.L10
テーブル|table (for eating/drinking)|E01.L10
ベッド|bed|E01.L10
{{'棚'|ruby:'たな'}}|shelf|E01.L10
ドア|door|E01.L10
{{'窓'|ruby:'まど'}}|window|E01.L10
ポスト|postbox, mailbox|E01.L10
ビル|building|E01.L10
ATM|cash machine, ATM (Automatic Teller Machine)|E01.L10
コーナー|corner, section|E01.L10
{{'切'|ruby:'きっ'}}{{'手'|ruby:'て'}}|postage stamp|E01.L11
はがき|postcard|E01.L11
{{'封'|ruby:'ふう'}}{{'筒'|ruby:'とう'}}|envelope|E01.L11
{{'船'|ruby:'ふな'}}{{'便'|ruby:'びん'}}|sea mail|E01.L11
{{'航'|ruby:'こう'}}{{'空'|ruby:'くう'}}{{'便'|ruby:'びん'}} / エアメール|airmail|E01.L11
{{'紅葉'|ruby:'もみじ'}}|maple. red leaves of autumn|E01.L12
{{'美'|ruby:'び'}}{{'術'|ruby:'じゅつ'}}|fine arts|E01.L13
{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}|electricity, light|E01.L14
エアコン|air conditioner|E01.L14
パスポート|passport|E01.L14
{{'地'|ruby:'ち'}}{{'図'|ruby:'ず'}}|map|E01.L14
お{{'釣'|ruby:'つ'}}り|change (money)|E01.L14
{{'資'|ruby:'し'}}{{'料'|ruby:'りょう'}}|materials, data|E01.L15
カタログ|catalogue|E01.L15
{{'時'|ruby:'じ'}}{{'刻'|ruby:'こく'}}{{'表'|ruby:'ひょう'}}|timetable (train, bus, Shinkansen)|E01.L15
{{'製'|ruby:'せい'}}{{'品'|ruby:'ひん'}}|products|E01.L15
ソフト|software|E01.L15
{{'電'|ruby:'でん'}}{{'子'|ruby:'し'}}{{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}|electronic dictionary|E01.L15
{{'緑'|ruby:'みどり'}}|green, greenery|E01.L16
キャッシュカード|cash dispensing card|E01.L16
{{'暗'|ruby:'あん'}}{{'証'|ruby:'しょう'}}{{'番'|ruby:'ばん'}}{{'号'|ruby:'ごう'}}|personal identification number, PIN|E01.L16
ボタン|button|E01.L16
［{{'健'|ruby:'けん'}}{{'康'|ruby:'こう'}}］{{'保'|ruby:'ほ'}}{{'険'|ruby:'けん'}}{{'証'|ruby:'しょう'}}|(health) insurance card|E02.L17
{{'薬'|ruby:'くすり'}}|medicine|E02.L17
ピアノ|piano|E02.L18
{{'日'|ruby:'にっ'}}{{'記'|ruby:'き'}}|diary|E02.L18
ビザ|visa|E02.L20
アニメ|anime (Japanese animated film)|E02.L21
マンガ|comic book|E02.L21
{{'地'|ruby:'ち'}}{{'球'|ruby:'きゅう'}}|earth|E02.L21
{{'月'|ruby:'つき'}}|moon|E02.L21
ロボット|robot|E02.L22
{{'押'|ruby:'お'}}し{{'入'|ruby:'い'}}れ|Japanese-style closet|E02.L22
{{'布'|ruby:'ふ'}}{{'団'|ruby:'とん'}}|Japanese-style mattress and quilt|E02.L22
{{'音'|ruby:'おと'}}|sound|E02.L23
{{'信'|ruby:'しん'}}{{'号'|ruby:'ごう'}}|traffic lights|E02.L23
{{'橋'|ruby:'はし'}}|bridge|E02.L23
{{'建'|ruby:'たて'}}{{'物'|ruby:'もの'}}|building|E02.L23
{{'財'|ruby:'さい'}}{{'布'|ruby:'ふ'}}|wallet, purse|I01.L26
ごみ|garbage, rubbish, dust|I01.L26
{{'燃'|ruby:'も'}}えるごみ|burnable rubbish|I01.L26
{{'瓶'|ruby:'びん'}}|bottle|I01.L26
{{'缶'|ruby:'かん'}}|can|I01.L26
ガス|gas|I01.L26
{{'声'|ruby:'こえ'}}|voice|I01.L27
{{'波'|ruby:'なみ'}}|wave, instability|I01.L27
{{'花'|ruby:'はな'}}{{'火'|ruby:'び'}}|fireworks|I01.L27
{{'道'|ruby:'どう'}}{{'具'|ruby:'ぐ'}}|tool, instrument, equipment|I01.L27
{{'家'|ruby:'か'}}{{'具'|ruby:'ぐ'}}|furniture|I01.L27
{{'本'|ruby:'ほん'}}{{'棚'|ruby:'だな'}}|bookcase|I01.L27
ポケット|pocket|I01.L27, I01.L29
ガム|chewing gum|I01.L28
{{'品'|ruby:'しな'}}{{'物'|ruby:'もの'}}|goods|I01.L28
{{'小'|ruby:'しょう'}}{{'説'|ruby:'せつ'}}|novel|I01.L28
～{{'機'|ruby:'き'}}|~ machine (e.g. vacuum {{'掃'|ruby:'そう'}}{{'除'|ruby:'じ'}}{{'機'|ruby:'き'}}, washing machine {{'洗'|ruby:'せん'}}{{'濯'|ruby:'たっ'}}{{'機'|ruby:'き'}})|I01.L28
［お］{{'皿'|ruby:'さら'}}|plate, dish|I01.L29
［お］{{'茶'|ruby:'ちゃ'}}{{'碗'|ruby:'わん'}}|(rice) bowl|I01.L29
コップ|glass (vessel)|I01.L29
ガラス|glass (material)|I01.L29
{{'袋'|ruby:'ぶくろ'}}|bag|I01.L29
ビニール{{'袋'|ruby:'ぶくろ'}}|plastic bag|I01.L29
{{'書'|ruby:'しょ'}}{{'類'|ruby:'るい'}}|document, papers|I01.L29
{{'枝'|ruby:'えだ'}}|branch, twig|I01.L29
{{'網'|ruby:'あみ'}}{{'棚'|ruby:'だな'}}|overhead rack|I01.L29
{{'壁'|ruby:'かべ'}}|wall|I01.L29
{{'針'|ruby:'はり'}}|hands (of a clock)|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Abstract {#abstract}

Noun | Meaning | Refer
---- | ------- | -----
{{'生'|ruby:'せい'}}{{'活'|ruby:'かつ'}}|life|-|B00.L08
{{'名'|ruby:'な'}}{{'前'|ruby:'まえ'}}|name|E01.L14
{{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}|address|E01.L14
{{'問'|ruby:'もん'}}{{'題'|ruby:'だい'}}|question, problem, trouble|E01.L14
{{'答'|ruby:'こた'}}え|answer|E01.L14
{{'読'|ruby:'よ'}}み{{'方'|ruby:'かた'}}|how to read, way of reading|E01.L14
～{{'方'|ruby:'かた'}}|how to ~, way of ~ing|E01.L14
{{'経'|ruby:'けい'}}{{'済'|ruby:'ざい'}}|economy|-|E01.L15
{{'独'|ruby:'どく'}}{{'身'|ruby:'しん'}}|single, unmarried|E01.L15
サービス|service|E01.L16
{{'金'|ruby:'きん'}}{{'額'|ruby:'がく'}}|amount of money|E01.L16
{{'禁'|ruby:'きん'}}{{'煙'|ruby:'えん'}}|no smoking|-|E02.L17
{{'熱'|ruby:'ねつ'}}|temperature, fever|E02.L17
{{'病'|ruby:'びょう'}}{{'気'|ruby:'き'}}|illness, disease|E02.L17
かぜ|cold, flu|E02.L17
{{'現'|ruby:'げん'}}{{'金'|ruby:'きん'}}|cash (payment method)|E02.L18
インターネット|the Internet|E02.L18
{{'調'|ruby:'ちょう'}}{{'子'|ruby:'し'}}|condition (of person/thing, e.g. {{'調'|ruby:'ちょう'}}{{'子'|ruby:'し'}}<span class="red">が</span>いい)|E02.L19
{{'言'|ruby:'こと'}}{{'葉'|ruby:'ば'}}|word, language|E02.L20
{{'本'|ruby:'ほん'}}{{'当'|ruby:'とう'}}|true|E02.L21
{{'噓'|ruby:'うそ'}}|lie|E02.L21
{{'交'|ruby:'こう'}}{{'通'|ruby:'つう'}}|transport, traffic|E02.L21
{{'物'|ruby:'ぶっ'}}{{'価'|ruby:'か'}}|(commodity) prices|E02.L21
{{'放'|ruby:'ほう'}}{{'送'|ruby:'そう'}}|announcement, broadcast|E02.L21
ニュース|news|E02.L21
デザイン|design, artwork|E02.L21
{{'夢'|ruby:'ゆめ'}}|dream|E02.L21
{{'天'|ruby:'てん'}}{{'才'|ruby:'さい'}}|genius|E02.L21
{{'意'|ruby:'い'}}{{'見'|ruby:'けん'}}|opinion|E02.L21
ユーモア|humor|E02.L22
{{'都'|ruby:'つ'}}{{'合'|ruby:'ごう'}}|convenience|E02.L22
{{'家'|ruby:'や'}}{{'賃'|ruby:'ちん'}}|rent|E02.L22
サイズ|size|E02.L23
ホームステイ|homestay|E02.L24
チャンス|chance|E02.L25
{{'意'|ruby:'い'}}{{'味'|ruby:'み'}}|meaning|E02.L25
{{'暇'|ruby:'ひま'}}|free time|E02.L25
～{{'弁'|ruby:'べん'}}|~ dialect|I01.L26
{{'横'|ruby:'よこ'}}|side|I01.L26
~ {{'会'|ruby:'がい'}}{{'社'|ruby:'しゃ'}}|~ company (name)|I01.L26
~{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}|~ class|I01.L27
{{'形'|ruby:'かたち'}}|shape|I01.L27
{{'将'|ruby:'しょう'}}{{'来'|ruby:'らい'}}|future|I01.L27, I01.L28
{{'景'|ruby:'け'}}{{'色'|ruby:'しき'}}|scenery, view|I01.L28
{{'力'|ruby:'ちから'}}|power|I01.L28
{{'人'|ruby:'にん'}}{{'気'|ruby:'き'}}|popularity (e.g. be popular with students: {{'学'|ruby:'がく'}}{{'生'|ruby:'せい'}}に{{'人'|ruby:'にん'}}{{'気'|ruby:'き'}}があります)|I01.L28
{{'形'|ruby:'かたち'}}|form, shape|I01.L28
{{'色'|ruby:'いろ'}}|colour|I01.L28
{{'味'|ruby:'あじ'}}|taste|I01.L28
{{'値'|ruby:'ね'}}{{'段'|ruby:'だん'}}|price|I01.L28
{{'給'|ruby:'きゅう'}}{{'料'|ruby:'りょう'}}|salary|I01.L28
ボーナス|bonus|I01.L28
ゲーム|(computer) game|I01.L28
{{'番'|ruby:'ばん'}}{{'組'|ruby:'ぐみ'}}|programme (e.g. テレビの{{'番'|ruby:'ばん'}}{{'組'|ruby:'ぐみ'}})|I01.L28
ドラマ|drama|I01.L28
{{'会'|ruby:'かい'}}{{'話'|ruby:'わ'}}|conversation|I01.L28
お{{'知'|ruby:'し'}}らせ|notice|I01.L28
{{'忘'|ruby:'わす'}}れ{{'物'|ruby:'もの'}}|things left behind, lost property|I01.L29

{.table .table-hover .table-sm .table-ruby-1}

## Food {{'食'|ruby:'た'}}べ{{'物'|ruby:'もの'}} {#food}

Noun | Meaning | Refer
---- | ------- | -----
ご{{'飯'|ruby:'はん'}}|a meal, cooked rice|B00.L06
{{'朝'|ruby:'あさ'}}ご{{'飯'|ruby:'はん'}}|breakfast|B00.L06
{{'昼'|ruby:'ひる'}}ご{{'飯'|ruby:'はん'}}|lunch|B00.L06
{{'晩'|ruby:'ばん'}}ご{{'飯'|ruby:'はん'}}|supper|B00.L06
パン|bread|B00.L06
{{'卵'|ruby:'たまご'}}|egg|B00.L06
{{'肉'|ruby:'にく'}}|meat|B00.L06
{{'魚'|ruby:'さかな'}}|fish|B00.L06
{{'野'|ruby:'や'}}{{'菜'|ruby:'さい'}}|vegetable|B00.L06
{{'果'|ruby:'くだ'}}{{'物'|ruby:'もの'}}|fruit|B00.L06
{{'水'|ruby:'みず'}}|water|B00.L06
お{{'茶'|ruby:'ちゃ'}}|tea, green tea|B00.L06
{{'紅'|ruby:'こう'}}{{'茶'|ruby:'ちゃ'}}|black tea|B00.L06
{{'牛'|ruby:'ぎゅう'}}{{'乳'|ruby:'にゅう'}} / ミルク|milk|B00.L06
ジュース|juice|B00.L06
ビール|beer|B00.L06
お{{'酒'|ruby:'さけ'}}|alcohol, Japanese rice wine|B00.L06
{{'食'|ruby:'た'}}べ{{'物'|ruby:'もの'}}|food|B00.L08
{{'飲'|ruby:'の'}}み{{'物'|ruby:'もの'}}|drinks|E01.L09
ナンプラー|nam pla (Thai fish sauce)|E01.L10
りんご|apple|E01.L11
みかん|mandarin orange|E01.L11
サンドイッチ|sandwich|E01.L11
カレー［ライス］|curry (with rice)|E01.L11
アイスクリーム|ice cream|E01.L11
すき{{'焼'|ruby:'や'}}き|sukiyaki (beef and vegetable hot pot)|E01.L12
{{'刺'|ruby:'さし'}}{{'身'|ruby:'み'}}|sashimi (sliced raw fish)|E01.L12
［お］すし|sushi (vinegared rice topped with raw fish)|E01.L12
{{'天'|ruby:'てん'}}ぷら|tempura (seafood and vegetables deep fried in batter)|E01.L12
{{'豚'|ruby:'ぶた'}}{{'肉'|ruby:'にく'}}|pork|E01.L12
{{'鶏'|ruby:'とり'}}{{'肉'|ruby:'にく'}}|chicken|E01.L12
{{'牛'|ruby:'ぎゅう'}}{{'肉'|ruby:'にく'}}|beef|E01.L12
レモン|lemon|E01.L12
{{'定'|ruby:'てい'}}{{'食'|ruby:'しょく'}}|set meal|E01.L13
{{'牛'|ruby:'ぎゅう'}}{{'丼'|ruby:'どん'}}|bowl of rice topped with beef|E01.L13
{{'塩'|ruby:'しお'}}|salt|E01.L14
{{'砂'|ruby:'さ'}}{{'糖'|ruby:'とう'}}|sugar|E01.L14
ケーキ|cake|E02.L22
［お］{{'弁'|ruby:'べん'}}{{'当'|ruby:'とう'}}|box lunch|E02.L22
［お］{{'湯'|ruby:'ゆ'}}|hot water|E02.L23
［お］{{'菓'|ruby:'か'}}{{'子'|ruby:'し'}}|sweets, snacks|E02.L24

{.table .table-hover .table-sm .table-ruby-1}

## Weather {{'天'|ruby:'てん'}}{{'気'|ruby:'き'}} {#weather}

Noun | Meaning | Refer
---- | ------- | -----
{{'季'|ruby:'き'}}{{'節'|ruby:'せつ'}}|season|E01.L12
{{'春'|ruby:'はる'}}|spring|E01.L12
{{'夏'|ruby:'なつ'}}|summer|E01.L12
{{'秋'|ruby:'あき'}}|autumn, fall|E01.L12
{{'冬'|ruby:'ふゆ'}}|winter|E01.L12
{{'天'|ruby:'てん'}}{{'気'|ruby:'き'}}|weather|E01.L12
{{'雨'|ruby:'あめ'}}|rain, rainy|E01.L12
{{'雪'|ruby:'ゆき'}}|snow, snowy|E01.L12
{{'曇'|ruby:'くも'}}り|cloudy|E01.L12
{{'晴'|ruby:'は'}}れ|sunny|E01.L12

{.table .table-hover .table-sm .table-ruby-1}

## Event {#event}

Noun | Meaning | Example | Refer
---- | ------- | ------- | -----
{{'休'|ruby:'やす'}}み|rest, a holiday, a day off|-|B00.L04
{{'昼'|ruby:'ひる'}}{{'休'|ruby:'やす'}}み|lunchtime|-|B00.L04
{{'試'|ruby:'し'}}{{'験'|ruby:'けん'}}|examination, test|-|B00.L04
{{'誕'|ruby:'たん'}}{{'生'|ruby:'じょう'}}{{'日'|ruby:'び'}}|birthday|-|B00.L05
クリスマス|Christmas|-|B00.L07
コンサート|concert|-|E01.L09
カラオケ|karaoke|-|E01.L09
{{'歌'|ruby:'か'}}{{'舞'|ruby:'ぶ'}}{{'伎'|ruby:'き'}}|Kabuki (traditional Japanese musical drama)|-|E01.L09
クラス|class|-|E01.L11
［お］{{'祭'|ruby:'まつ'}}り|festival|-|E01.L12
［お］{{'正'|ruby:'しょう'}}{{'月'|ruby:'がつ'}}|New Year's Day|-|E01.L13
{{'雪'|ruby:'ゆき'}}{{'祭'|ruby:'まつ'}}り|snow festival|-|E01.L16
お{{'茶'|ruby:'ちゃ'}}|tea ceremony|-|E02.L19
{{'故'|ruby:'こ'}}{{'障'|ruby:'しょう'}}|breakdown|{{'故'|ruby:'こ'}}{{'障'|ruby:'しょう'}}<span class="red">を</span>します|E02.L23
{{'母'|ruby:'はは'}}の{{'日'|ruby:'ひ'}}|Mother's Day|-|E02.L24
{{'運'|ruby:'うん'}}{{'動'|ruby:'どう'}}{{'会'|ruby:'かい'}}|athletic meeting, sports day|-|I01.L26
{{'盆'|ruby:'ぼん'}}{{'踊'|ruby:'おど'}}り|Bon Festival dance|-|I01.L26
イベント|event|-|I01.L28
{{'地'|ruby:'じ'}}{{'震'|ruby:'しん'}}|earthquake|-|I01.L29

{.table .table-hover .table-sm .table-ruby-1 .table-ruby-3}

## Activity {#activity}

Noun | Meaning | Example | Refer
---- | ------- | ------- | -----
{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}|meeting, conference|{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}<span class="red">を</span>します|B00.L04
{{'宿'|ruby:'しゅく'}}{{'題'|ruby:'だい'}}|homework (do)|{{'宿'|ruby:'しゅく'}}{{'題'|ruby:'だい'}}<span class="red">を</span>します|B00.L06
テニス|tennis (play)|テニス<span class="red">を</span>します|B00.L06
サッカー|soccer, football (play)|サッカー<span class="red">を</span>します|B00.L06
［お］{{'花'|ruby:'はな'}}{{'見'|ruby:'み'}}|cherry-blossom viewing|{{'花'|ruby:'はな'}}{{'見'|ruby:'み'}}<span class="red">を</span>します|B00.L06
［お］{{'仕'|ruby:'し'}}{{'事'|ruby:'ごと'}}|work, business|{{'仕'|ruby:'し'}}{{'事'|ruby:'ごと'}}<span class="red">を</span>します|B00.L08
{{'料'|ruby:'りょう'}}{{'理'|ruby:'り'}}|dish (cooked food), cooking|{{'料'|ruby:'りょう'}}{{'理'|ruby:'り'}}<span class="red">を</span>します|E01.L09
スポーツ|sport|スポーツ<span class="red">を</span>します|E01.L09
{{'野'|ruby:'や'}}{{'球'|ruby:'きゅう'}}|baseball|{{'野'|ruby:'や'}}{{'球'|ruby:'きゅう'}}<span class="red">を</span>します|E01.L09
ダンス|dance|ダンス<span class="red">を</span>します|E01.L09
{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}|trip, tour|{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}<span class="red">を</span>します|E01.L09
アルバイト|side job|アルバイト<span class="red">を</span>します|E01.L09
パーティー|party|パーティー<span class="red">を</span>します|E01.L12
{{'生'|ruby:'い'}}け{{'花'|ruby:'ばな'}}|flower arrangement (practice)|{{'生'|ruby:'い'}}け{{'花'|ruby:'ばな'}}<span class="red">を</span>します|E01.L12
{{'釣'|ruby:'つ'}}り|fishing|{{'釣'|ruby:'つ'}}り<span class="red">を</span>します|E01.L13
スキー|skiing|スキー<span class="red">を</span>します|E01.L13
ジョギング|jogging|ジョギング<span class="red">を</span>します|E01.L16
シャワー|shower|シャワー<span class="red">を</span>{{'浴'|ruby:'あ'}}びる|E01.L16
{{'確'|ruby:'かく'}}{{'認'|ruby:'にん'}}|confirmation|{{'確'|ruby:'かく'}}{{'認'|ruby:'にん'}}<span class="red">を</span>します|E01.L16
お{{'風'|ruby:'ふ'}}{{'呂'|ruby:'ろ'}}|bath|お{{'風'|ruby:'ふ'}}{{'呂'|ruby:'ろ'}}<span class="red">に</span>{{'入'|ruby:'はい'}}る|E02.L17
{{'趣'|ruby:'しゅ'}}{{'味'|ruby:'み'}}|hobby|-|E02.L18
お{{'祈'|ruby:'いの'}}り|prayer|お{{'祈'|ruby:'いの'}}り<span class="red">を</span>します|E02.L18
{{'練'|ruby:'れん'}}{{'習'|ruby:'しゅう'}}|practice|{{'練'|ruby:'れん'}}{{'習'|ruby:'しゅう'}}［<span class="red">を</span>］します|E02.L19
ゴルフ|golf|ゴルフ<span class="red">を</span>します|E02.L19
{{'相'|ruby:'す'}}{{'撲'|ruby:'もう'}}|sumo wrestling|{{'相'|ruby:'す'}}{{'撲'|ruby:'もう'}}<span class="red">を</span>します|E02.L19
ダイエット|diet|ダイエット<span class="red">を</span>します|E02.L19
{{'試'|ruby:'し'}}{{'合'|ruby:'あい'}}|game, match|{{'試'|ruby:'し'}}{{'合'|ruby:'あい'}}<span class="red">を</span>します|E02.L21
{{'話'|ruby:'はなし'}}|talk, speech, what one says, story|{{'話'|ruby:'はなし'}}<span class="red">を</span>します|E02.L21
{{'準'|ruby:'じゅん'}}{{'備'|ruby:'び'}}|preparation|{{'準'|ruby:'じゅん'}}{{'備'|ruby:'び'}}<span class="red">を</span>します|E02.L24
{{'引'|ruby:'ひ'}}っ{{'越'|ruby:'こ'}}し|moving out|{{'引'|ruby:'ひ'}}っ{{'越'|ruby:'こ'}}し<span class="red">を</span>します|E02.L24
{{'転'|ruby:'てん'}}{{'勤'|ruby:'きん'}}|transfer|{{'転'|ruby:'てん'}}{{'勤'|ruby:'きん'}}<span class="red">を</span>します|E02.L25
ボランティア|volunteer|-|I01.L26
クリーニング|(dry) cleaning, laundry|-|I01.L27
{{'経'|ruby:'けい'}}{{'験'|ruby:'けん'}}|experience / be experienced|{{'経'|ruby:'けい'}}{{'験'|ruby:'けん'}}<span class="red">を</span>します<br/>{{'経'|ruby:'けい'}}{{'験'|ruby:'けん'}}<span class="red">が</span>あります|I01.L28
スピーチ|speech|スピーチ<span class="red">を</span>します|I01.L29
{{'返'|ruby:'へん'}}{{'事'|ruby:'じ'}}|reply, answer|{{'返'|ruby:'へん'}}{{'事'|ruby:'じ'}}<span class="red">を</span>します|I01.L29

{.table .table-hover .table-sm .table-ruby-1 .table-ruby-3}
