---
layout: reader.html
title: 'Vocabulary: Kanji'
navbar:
  vocab: 'active'
---

# Kanji {{'漢'|ruby:'かん'}}{{'字'|ruby:'じ'}}

Kanji is a word that written like Chinese script. In general, it can be pronounced in two ways:

- _**Kun** reading (**くん**{{'読'|ruby:'よ'}}み)_: Japanese origin
- _**On** reading (**おん**{{'読'|ruby:'よ'}}み)_: Chinese borrowed
- _Special reading_: neither くん nor おん

<div class="table-responsive">

Kanji | くん | おん | Special
-|-|-|-
日|ひ、び（{{'月'|ruby:'げつ'}}{{'曜'|ruby:'よう'}}<span class="red">{{'日'|ruby:'び'}}</span>）<br/>か（{{'十'|ruby:'とお'}}<span class="red">{{'日'|ruby:'か'}}</span>）|にち（{{'毎'|ruby:'まい'}}<span class="red">{{'日'|ruby:'にち'}}</span>、<span class="red">{{'日'|ruby:'にち'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）<br/>に（<span class="red">{{'日'|ruby:'に'}}</span>{{'本'|ruby:'ほん'}}）<br/>じつ（{{'平'|ruby:'へい'}}<span class="red">{{'日'|ruby:'じつ'}}</span>）|-
月|つき（<span class="red">{{'月'|ruby:'つき'}}</span>）|げつ（<span class="red">{{'月'|ruby:'げつ'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）<br/>がつ（{{'四'|ruby:'し'}}<span class="red">{{'月'|ruby:'がつ'}}</span>）|-
火|ひ（<span class="red">{{'火'|ruby:'ひ'}}</span>）<br/>び（{{'花'|ruby:'はな'}}<span class="red">{{'火'|ruby:'び'}}</span>）|か（<span class="red">{{'火'|ruby:'か'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）|-
水|みず（<span class="red">{{'水'|ruby:'みず'}}</span>）|すい（<span class="red">{{'水'|ruby:'すい'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）|-
木|き（<span class="red">{{'木'|ruby:'き'}}</span>）|もく（<span class="red">{{'木'|ruby:'もく'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）|-
金|かね（お<span class="red">{{'金'|ruby:'かね'}}</span>）|きん（<span class="red">{{'金'|ruby:'きん'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）|-
土|-|ど（<span class="red">{{'土'|ruby:'ど'}}</span>{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}）|お{{'土産'|ruby:'みやげ'}}
山|やま（<span class="red">{{'山'|ruby:'やま'}}</span>）|さん（{{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}<span class="red">{{'山'|ruby:'さん'}}</span>）|-
川|かわ、がわ（<span class="red">{{'川'|ruby:'かわ'}}</span>、{{'信濃'|ruby:'しなの'}}<span class="red">{{'川'|ruby:'がわ'}}</span>）|-|-
田|た、だ（<span class="red">{{'田'|ruby:'た'}}</span>{{'中'|ruby:'なか'}}さん、{{'山'|ruby:'やま'}}<span class="red">{{'田'|ruby:'だ'}}</span>さん）|-|{{'田舎'|ruby:'いなか'}}
一|ひとつ（{{'一つ'|ruby:'ひとつ'}}）|いち（<span class="red">{{'一'|ruby:'いち'}}</span>{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}、<span class="red">{{'一'|ruby:'いち'}}</span>{{'月'|ruby:'がつ'}}）<br/>いっ（もう<span class="red">{{'一'|ruby:'いっ'}}</span>{{'杯'|ruby:'ぱい'}}、<span class="red">{{'一'|ruby:'いっ'}}</span>{{'緒'|ruby:'しょ'}}に、<span class="red">{{'一'|ruby:'いっ'}}</span>{{'生'|ruby:'しょう'}}{{'懸'|ruby:'けん'}}{{'命'|ruby:'めい'}}）|{{'一日'|ruby:'ついたち'}}
二|ふたつ（{{'二つ'|ruby:'ふたつ'}}）|に（<span class="red">{{'二'|ruby:'に'}}</span>{{'月'|ruby:'がつ'}}）|{{'二日'|ruby:'ふつか'}}
三|みっつ（{{'三つ'|ruby:'みっつ'}}）|さん（<span class="red">{{'三'|ruby:'さん'}}</span>{{'月'|ruby:'がつ'}}）|-
四|よっつ（{{'四つ'|ruby:'よっつ'}}）<br/>よん（<span class="red">{{'四'|ruby:'よん'}}</span>{{'分'|ruby:'ふん'}}）<br/>よ（<span class="red">{{'四'|ruby:'よ'}}</span>{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}）|し（<span class="red">{{'四'|ruby:'し'}}</span>{{'月'|ruby:'がつ'}}）|-
五|いつつ（{{'五つ'|ruby:'いつつ'}}）|ご（<span class="red">{{'五'|ruby:'ご'}}</span>{{'回'|ruby:'かい'}}、<span class="red">{{'五'|ruby:'ご'}}</span>{{'階'|ruby:'かい'}}）|-
六|むっつ（{{'六つ'|ruby:'むっつ'}}）<br/>むい（<span class="red">{{'六'|ruby:'むい'}}</span>{{'日'|ruby:'か'}}）|ろく（<span class="red">{{'六'|ruby:'ろく'}}</span>{{'人'|ruby:'にん'}}）|-
七|なな（<span class="red">{{'七'|ruby:'なな'}}</span>つ）<br/>なの（<span class="red">{{'七'|ruby:'なの'}}</span>{{'日'|ruby:'か'}}）|しち（<span class="red">{{'七'|ruby:'しち'}}</span>{{'時'|ruby:'じ'}}）|-
八|やっつ（{{'八つ'|ruby:'やっつ'}}）<br/>よう（<span class="red">{{'八'|ruby:'よう'}}</span>{{'日'|ruby:'か'}}）|はち（<span class="red">{{'八'|ruby:'はち'}}</span>{{'月'|ruby:'がつ'}}）|-
九|ここの（<span class="red">{{'九'|ruby:'ここの'}}</span>{{'日'|ruby:'か'}}、<span class="red">{{'九'|ruby:'ここの'}}</span>つ）|きゅう（<span class="red">{{'九'|ruby:'きゅう'}}</span>{{'人'|ruby:'にん'}}）<br/>く（<span class="red">{{'九'|ruby:'く'}}</span>{{'月'|ruby:'がつ'}}、<span class="red">{{'九'|ruby:'く'}}</span>{{'時'|ruby:'じ'}}）|-
十|とお（<span class="red">{{'十'|ruby:'とお'}}</span>{{'日'|ruby:'か'}}）|じゅう（<span class="red">{{'十'|ruby:'じゅう'}}</span>{{'台'|ruby:'だい'}}）|{{'二十日'|ruby:'はつか'}}、{{'二十歳'|ruby:'はたち'}}
百|-|ひゃく（<span class="red">{{'百'|ruby:'ひゃく'}}</span>、{{'三'|ruby:'さん'}}<span class="red">{{'百'|ruby:'びゃく'}}</span>、{{'六'|ruby:'ろっ'}}<span class="red">{{'百'|ruby:'ぴゃく'}}</span>）|-
千|-|せん（<span class="red">{{'千'|ruby:'せん'}}</span>、{{'三'|ruby:'さん'}}<span class="red">{{'千'|ruby:'ぜん'}}</span>）|-
万|-|まん（{{'一'|ruby:'いち'}}<span class="red">{{'万'|ruby:'まん'}}</span>）|-
円|-|えん（{{'二'|ruby:'に'}}{{'千'|ruby:'せん'}}{{'五'|ruby:'ご'}}{{'百'|ruby:'ひゃく'}}<span class="red">{{'円'|ruby:'えん'}}</span>）|-
学|-|がく、がっ（<span class="red">{{'学'|ruby:'がく'}}</span>{{'生'|ruby:'せい'}}、{{'大'|ruby:'だい'}}<span class="red">{{'学'|ruby:'がく'}}</span>、<span class="red">{{'学'|ruby:'がっ'}}</span>{{'校'|ruby:'こう'}}）|-
生|うまれる（<span class="red">{{'生'|ruby:'う'}}</span>まれる）<br/>いける（<span class="red">{{'生'|ruby:'い'}}</span>ける）|せい（{{'先'|ruby:'せん'}}<span class="red">{{'生'|ruby:'せい'}}</span>、{{'学'|ruby:'がく'}}<span class="red">{{'生'|ruby:'せい'}}</span>、<span class="red">{{'生'|ruby:'せい'}}</span>{{'命'|ruby:'めい'}}）<br/>しょう、じょう（{{'誕'|ruby:'たん'}}<span class="red">{{'生'|ruby:'じょう'}}</span>{{'日'|ruby:'び'}}、{{'一'|ruby:'いっ'}}<span class="red">{{'生'|ruby:'しょう'}}</span>{{'懸'|ruby:'けん'}}{{'命'|ruby:'めい'}}）|-
先|さき（<span class="red">{{'先'|ruby:'さき'}}</span>に）|せん（<span class="red">{{'先'|ruby:'せん'}}</span>{{'週'|ruby:'しゅう'}}、<span class="red">{{'先'|ruby:'せん'}}</span>{{'月'|ruby:'げつ'}}、<span class="red">{{'先'|ruby:'せん'}}</span>{{'生'|ruby:'せい'}}）|-
会|あう（<span class="red">{{'会'|ruby:'あ'}}</span>う）|かい、がい（{{'運'|ruby:'うん'}}{{'動'|ruby:'どう'}}<span class="red">{{'会'|ruby:'かい'}}</span>、<span class="red">{{'会'|ruby:'かい'}}</span>{{'社'|ruby:'しゃ'}}、ガス<span class="red">{{'会'|ruby:'がい'}}</span>{{'社'|ruby:'しゃ'}}、{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="red">{{'会'|ruby:'がい'}}</span>{{'社'|ruby:'しゃ'}}）|-
社|-|しゃ、じゃ（{{'会'|ruby:'かい'}}<span class="red">{{'社'|ruby:'しゃ'}}</span>、{{'神'|ruby:'じん'}}<span class="red">{{'社'|ruby:'じゃ'}}</span>）|-
員|-|いん（{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}<span class="red">{{'員'|ruby:'いん'}}</span>、{{'銀'|ruby:'ぎん'}}{{'行'|ruby:'こう'}}<span class="red">{{'員'|ruby:'いん'}}</span>）|-
医|-|い（<span class="red">{{'医'|ruby:'い'}}</span>{{'者'|ruby:'しゃ'}}）|-
者|-|しゃ、じゃ（{{'研'|ruby:'けん'}}{{'究'|ruby:'きゅう'}}<span class="red">{{'者'|ruby:'しゃ'}}</span>、{{'忍'|ruby:'にん'}}<span class="red">{{'者'|ruby:'じゃ'}}</span>）|-
本|もと（{{'松'|ruby:'まつ'}}<span class="red">{{'本'|ruby:'もと'}}</span>さん）|ほん、ぼん、ぽん（{{'日'|ruby:'に'}}<span class="red">{{'本'|ruby:'ほん'}}</span>、{{'三'|ruby:'さん'}}<span class="red">{{'本'|ruby:'ぼん'}}</span>、{{'六'|ruby:'ろっ'}}<span class="red">{{'本'|ruby:'ぽん'}}</span>{{'木'|ruby:'ぎ'}}）|-
中|なか（<span class="red">{{'中'|ruby:'なか'}}</span>）|ちゅう（<span class="red">{{'中'|ruby:'ちゅう'}}</span>{{'国'|ruby:'ごく'}}）、じゅう（{{'世'|ruby:'せ'}}{{'界'|ruby:'かい'}}<span class="red">{{'中'|ruby:'じゅう'}}</span>）|-
国|くに（<span class="red">{{'国'|ruby:'くに'}}</span>）|こく、ごく、こっ（{{'中'|ruby:'ちゅう'}}<span class="red">{{'国'|ruby:'ごく'}}</span>、{{'外'|ruby:'がい'}}<span class="red">{{'国'|ruby:'こく'}}</span>、{{'入'|ruby:'にゅう'}}<span class="red">{{'国'|ruby:'こく'}}</span>、<span class="red">{{'国'|ruby:'こっ'}}</span>{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}{{'事'|ruby:'じ'}}{{'堂'|ruby:'どう'}}）|-
人|ひと（<span class="red">{{'人'|ruby:'ひと'}}</span>）|じん（{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">{{'人'|ruby:'じん'}}</span>）<br/>にん（{{'三'|ruby:'さん'}}<span class="red">{{'人'|ruby:'にん'}}</span>、{{'四'|ruby:'よ'}}<span class="red">{{'人'|ruby:'にん'}}</span>、<span class="red">{{'人'|ruby:'にん'}}</span>{{'間'|ruby:'げん'}}）|{{'一'|ruby:'ひと'}}<span class="red">{{'人'|ruby:'り'}}</span>、{{'二'|ruby:'ふた'}}<span class="red">{{'人'|ruby:'り'}}</span>

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-3 .table-ruby-4}

</div>
