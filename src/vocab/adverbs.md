---
layout: reader.html
title: 'Vocabulary: Adverbs'
navbar:
  vocab: 'active'
---

# Adverb の {{'言'|ruby:'こと'}}{{'葉'|ruby:'ば'}}

Adverb | Meaning | Refer
------ | ------- | -----
ちょっと|a little while, a little bit|B00.L06
いつも|always, usually|B00.L06
{{'時'|ruby:'とき'}}{{'々'|ruby:'どき'}}|sometimes|B00.L06
とても|very|B00.L08
あまり|not so (used with negatives)|B00.L08
よく|well, much; often|E01.L09, E02.L22
だいたい|mostly, roughly|E01.L09
たくさん|many, much|E01.L09
{{'少'|ruby:'すこ'}}し|a little, a few|E01.L09
{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}}|not at all (used with negatives)|E01.L09
{{'早'|ruby:'はや'}}く|early, quickly, fast|E01.L09
{{'全'|ruby:'ぜん'}}{{'部'|ruby:'ぶ'}}で|in total|E01.L11
みんな|all, everything, everyone|E01.L11
どちらも|both|E01.L12
{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}|the most|E01.L12
ずっと|by far|E01.L12
{{'初'|ruby:'はじ'}}めて|for the first time|E01.L12
{{'別'|ruby:'べつ'}}{{'々'|ruby:'べつ'}}に|separately|E01.L13
まっすぐ|straight|E01.L14
ゆっくり|slowly, leisurely|E01.L14
すぐ|immediately|E01.L14
また|again|E01.L14
あとで|later|E01.L14
もう{{'少'|ruby:'すこ'}}し|a little more|E01.L14
もう～|~ more, another ~|E01.L14
まず|first of all|E01.L16
～までに|before ~, by ~ (indicating time limit)|E02.L17
{{'特'|ruby:'とく'}}に|especially|E02.L18
なかなか|not easily (used with negatives)|E02.L18
{{'是'|ruby:'ぜ'}}{{'非'|ruby:'ひ'}}|by all means, really (used for asserting)|E02.L18
{{'前'|ruby:'まえ'}}に|before, ago|E02.L18
{{'一'|ruby:'いち'}}{{'度'|ruby:'ど'}}|once (formal)|E02.L19
{{'一'|ruby:'いっ'}}{{'回'|ruby:'かい'}}|once (informal)|E02.L19
{{'一'|ruby:'いち'}}{{'度'|ruby:'ど'}}も|not once, never (used with negatives)|E02.L19
だんだん|gradually|E02.L19
もうすぐ|soon|E02.L19
みんなで|all together|E02.L20
{{'最'|ruby:'さい'}}{{'近'|ruby:'きん'}}|recently, these days|E02.L21
{{'多'|ruby:'た'}}{{'分'|ruby:'ぶん'}}|probably, perhaps, maybe|E02.L21
きっと|surely, definitely (high probability)|E02.L21
{{'本'|ruby:'ほん'}}{{'当'|ruby:'とう'}}に|really (can used for both positives/negatives, also for e.g. thanks, sorry)|E02.L21
もちろん|of course|E02.L21
{{'何'|ruby:'なん'}}{{'回'|ruby:'かい'}}も|many times|E02.L23
{{'全'|ruby:'ぜん'}}{{'部'|ruby:'ぶ'}}|all|E02.L24
{{'他'|ruby:'ほか'}}に|besides|E02.L24
もし［～たら］|if|E02.L25
{{'今'|ruby:'こん'}}{{'度'|ruby:'ど'}}|next time, another time|I01.L26
ずいぶん|very, pretty|I01.L26
{{'直'|ruby:'ちょく'}}{{'接'|ruby:'せつ'}}|directly|I01.L26
はっきり|clearly|I01.L27
{{'自'|ruby:'じ'}}{{'由'|ruby:'ゆう'}}に|freely|I01.L27

{.table .table-hover .table-sm .align-text-bottom .table-ruby-1}
