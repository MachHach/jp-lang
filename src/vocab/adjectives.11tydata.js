module.exports = {
    tableOfContent: [
        {id: 'temperature', name: 'Temperature'},
        {id: 'colors', name: 'Colors'},
        {id: 'other', name: 'Other'},
    ],
}
