---
layout: reader.html
title: 'Vocabulary: Misc'
navbar:
  vocab: 'active'
---

# Misc

## Greetings {{'挨'|ruby:'あい'}}{{'拶'|ruby:'さつ'}} {#greetings}

Sentence | Meaning | Refer
-------- | ------- | -----
おはよう［ございます］|Good morning|B00.L01
こんにちは|Hello (good afternoon)|B00.L01
こんばんは|Good evening|B00.L01
さようなら|Goodbye|B00.L01
また{{'明日'|ruby:'あした'}}|See you tomorrow|B00.L01
おやすみなさい|Good night|B00.L01
ありがとう［ございます］|Thank you|B00.L01
どういたしまして|You are welcome (formal)|B00.L01
すみません|Excuse me / Sorry|B00.L01
いってらっしゃい|See you later / So long (lit. Go and come back)|E01.L11
{{'行'|ruby:'い'}}って{{'来'|ruby:'き'}}ます|See you later / So long (lit. I'm going and coming back)|E01.L11
ただいま|I'm home|E01.L12
お{{'帰'|ruby:'かえ'}}り［なさい］|Welcome home|E01.L12
おかけさまで|Thank you (used when expressing gratitude for help received)|E02.L19
おめでとう［ございます］|Congratulations (used on birthdays, at weddings, New Year's Day, etc.)|E02.L22
もしもし|hello (used on the phone)|E02.L25

{.table .table-hover .table-sm .align-text-bottom .table-ruby-1}

## Other {#other}

Sentence | Meaning | Refer
-------- | ------- | -----
<span class="red">{{'初'|ruby:'はじ'}}め</span>まして|How do you do? (I am meeting you for the <span class="red">first time</span>)|B00.L01
～から{{'来'|ruby:'き'}}ました|I'm from ~ (country)|B00.L01
「どうぞ」よろしく「お{{'願'|ruby:'ねが'}}いします」|Please to meet you (Please be nice to me)|B00.L01
<span class="red">{{'失'|ruby:'しつ'}}{{'礼'|ruby:'れい'}}</span><span class="orange">ですが</span>|<span class="red">Excuse me</span>, <span class="orange">but</span> (used when asking someone for personal information)|B00.L01
お{{'名'|ruby:'な'}}{{'前'|ruby:'まえ'}}は？|May I have your name?|B00.L01
こちらは～さんです|This is Mr./Ms. ~|B00.L01
あのう|Er... (used to show hesitation)|B00.L02
えっ|Oh? What! (used when hearing something unexpected)|B00.L02
どうぞ|Here you are (used when offering someone something)|B00.L02
「どうも」<span class="red">ありがとう</span>「ございます」|<span class="red">Thank you</span> (very much)|B00.L02
そうですか|I see|B00.L02
{{'違'|ruby:'ちが'}} います|No, it isn't / You are wrong|B00.L02
あ|Oh! (used when becoming aware of something)|B00.L02
これからお{{'世'|ruby:'せ'}}{{'話'|ruby:'わ'}}になります|Thank you in advance for your kindness|B00.L02
<span class="red">こちらこそ</span>「どうぞ」よろしく「お{{'願'|ruby:'ねが'}}いします」|Pleased to meet you, <span class="red">too</span> (response to)|B00.L02
すみません|Excuse me|B00.L03
どうも|Thanks|B00.L03
いらっしゃいませ|Welcome / May I help you (a greeting to a customer or a guest entering a shop)|B00.L03
「～を」{{'見'|ruby:'み'}}せてください|Please show me ~|B00.L03
じゃ|well, then, in the case|B00.L03
「～を」ください|Give me ~, please|B00.L03
<span class="red">{{'大'|ruby:'たい'}}{{'変'|ruby:'へん'}}</span>ですね|That's <span class="red">tough</span>, isn't it (used when expressing sympathy)|B00.L04
そうですね|Yes, it is|B00.L05
「どうも」ありがとうございました|Thank you very much|B00.L05
どういたしまして|You're welcome / Don't mention it|B00.L05
いいですね|That's good|B00.L06
わかりました|I see|B00.L06
{{'何'|ruby:'なん'}}ですか|Yes? (lit. What is it?)|B00.L06
じゃ、また［あした］|See you (tomorrow)|B00.L06
［～、］<span class="red">すてき</span>ですね|What a <span class="red">nice</span> (~)!|B00.L07
いらっしゃい|How nice of you to come (lit. Welcome)|B00.L07
どうぞおあがりください|Do come in|B00.L07
<span class="red">{{'失'|ruby:'しつ'}}{{'礼'|ruby:'れい'}}</span>します|May I? (lit. I commit an <span class="red">incivility</span>)|B00.L07
［～は］いかがですか|Won't you have (~)? (used when offering something)|B00.L07
いただきます|Thank you / I accept (said before starting to eat or drink)|B00.L07
ごちそうさま［でした］|That was delicious (said after eating or drinking)|B00.L07
お{{'元'|ruby:'げん'}}{{'気'|ruby:'き'}}ですか|How are you?|B00.L08
そうですね|Well let me see (pausing)|B00.L08
［～、］<span class="red">もういっぱい</span>いかがですか|Won't you have <span class="red">another cup</span> of (~)?|B00.L08
［いいえ、］{{'結'|ruby:'けっ'}}{{'構'|ruby:'こう'}}です|No, thank you|B00.L08
<span class="red">もう</span>～です［ね］|It's <span class="red">already</span> ~ (, isn't it?)|B00.L08
そろそろ{{'失'|ruby:'しつ'}}{{'礼'|ruby:'れい'}}します|It's time I was going|B00.L08
いいえ|Not at all|B00.L08
<span class="red">まだ</span>いらっしゃってください。|Please come <span class="red">again</span>|B00.L08
{{'貸'|ruby:'か'}}して<span class="red">ください</span>|<span class="red">Please</span> lend (it to me)|E01.L09
いいですよ|Sure / Certainly|E01.L09
<span class="red">{{'残'|ruby:'ざん'}}{{'念'|ruby:'ねん'}}</span>です［が］|I'm sorry (, but), <span class="red">unfortunately</span>|E01.L09
ああ|oh|E01.L09
{{'一'|ruby:'いっ'}}{{'緒'|ruby:'しょ'}}にいかがですか|Won't you join me (us)?|E01.L09
［～は］ちょっと．．．|(~) is a bit difficult (an euphemism used when declining an invitation)|E01.L09
<span class="red">だめ</span>ですか|So you <span class="red">can't</span> (come)?|E01.L09
<span class="red">また</span><span class="orange">{{'今'|ruby:'こん'}}{{'度'|ruby:'ど'}}</span>お{{'願'|ruby:'ねが'}}いします|Please ask me <span class="red">again</span> <span class="orange">some other time</span> (used when refusing an invitation indirectly, considering someone's feeling)|E01.L09
［どうも］すみません|Thank you|E01.L10
かしこまりました|Certainly, (Sir/Madam)|E01.L11
いい［お］{{'天'|ruby:'てん'}}{{'気'|ruby:'き'}}ですね|Nice weather, isn't it?|E01.L11
<span class="red">お{{'出'|ruby:'で'}}かけ</span>ですか|Are you <span class="red">going out</span>?|E01.L11
ちょっと～まで|I'm just going to ~|E01.L11
お{{'願'|ruby:'ねが'}}いします|Please (lit. ask for a favour)|E01.L11
わあ、すごい{{'人'|ruby:'ひと'}}ですね|Wow! Look at all those people!|E01.L12
{{'疲'|ruby:'つか'}}れました|I'm tired|E01.L12
そうしましょう|Let's do that (used when agreeing with someone's suggestion)|E01.L13
ご{{'注'|ruby:'ちゅう'}}{{'文'|ruby:'もん'}}は？|May I take your order?|E01.L13
{{'少'|ruby:'しょう'}}{{'々'|ruby:'しょう'}}お{{'待'|ruby:'ま'}}ちください|Please wait (a moment)|E01.L13
～でございます|polite equivalent of です|E01.L13
さあ|right (used when encouraging some course of action)|E01.L14
あれ？|Oh! Eh? (in surprise or wonder)|E01.L14
{{'信'|ruby:'しん'}}{{'号'|ruby:'ごう'}}を{{'右'|ruby:'みぎ'}}へ<span class="red">{{'曲'|ruby:'ま'}}がって</span>ください|<span class="red">Turn</span> right at the traffic lights|E01.L14
これでお{{'願'|ruby:'ねが'}}いします|I'd like to pay with this|E01.L14
{{'思'|ruby:'おも'}}い{{'出'|ruby:'だ'}}します|remember, recollect|E01.L15
いらっしゃいます|be (honorific equivalent of います)|E01.L15
すごいですね|That's amazing!|E01.L16
［いいえ、］まだまだです|(No, ) I still have a long way to go|E01.L16
お{{'引'|ruby:'ひ'}}き{{'出'|ruby:'だ'}}しですか|Are you making a withdrawal?|E01.L16
どうしましたか|What's the matter?|E02.L17
［～が］{{'痛'|ruby:'いた'}}いです|(I) have a pain (in my ~)|E02.L17
それから|and, furthermore|E02.L17
お{{'大'|ruby:'だい'}}{{'事'|ruby:'じ'}}に|Take care of yourself (said to people who are ill)|E02.L17
へえ|What! Really! (used when expressing surprise)|E02.L18
それは{{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}いですね|That's interesting, isn't it?|E02.L18
{{'本'|ruby:'ほん'}}{{'当'|ruby:'とう'}}ですか|Really?|E02.L18
{{'乾'|ruby:'かん'}}{{'杯'|ruby:'ぱい'}}|Bottoms up / Cheers!|E02.L19
{{'体'|ruby:'からだ'}}にいい|good for one's health|E02.L19
お{{'腹'|ruby:'なか'}}が{{'一'|ruby:'いっ'}}{{'杯'|ruby:'ぱい'}}です|(I'm) full|E02.L20
よかっ<span class="orange">たら</span>|<span class="orange">if</span> you like|E02.L20
{{'久'|ruby:'ひさ'}}しぶりですね|It's been a long time (since we last met)|E02.L21
～でも{{'飲'|ruby:'の'}}みませんか|How about drinking ~ or something?|E02.L21
もう{{'帰'|ruby:'かえ'}}ら<span class="orange">ないと</span>．．．|I <span class="orange">have to</span> get home now...|E02.L21
えーと|well, let me see|E02.L22
お{{'探'|ruby:'さが'}}しですか|Are you looking for ~?|E02.L22
では|Well then,|E02.L22
お{{'名'|ruby:'な'}}{{'前'|ruby:'まえ'}}と{{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}がわかる{{'物'|ruby:'もの'}}|A thing that states a name and address (e.g. driving license)|E02.L23
［いろいろ］お{{'世'|ruby:'せ'}}{{'話'|ruby:'わ'}}になりました|Thank you for everything you've done for me|E02.L25
どうぞ、お{{'元'|ruby:'げん'}}{{'気'|ruby:'き'}}で|Please take care of yourself (said when expecting a long separation)|E02.L25
［ちょっと］お{{'願'|ruby:'ねが'}}いがあるんですが|I have a (small) favour to ask|I01.L28
{{'実'|ruby:'じつ'}}は|as a matter as fact|I01.L28
うーん|well, let me see, hmm...|I01.L28
お{{'先'|ruby:'さき'}}にどうぞ|After you / Go ahead please|I01.L29
{{'今'|ruby:'いま'}}の{{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}|the train which has just left|I01.L29
{{'覚'|ruby:'おぼ'}}えていません|I don't remember|I01.L29
{{'確'|ruby:'たし'}}か|I'm pretty sure|I01.L29
［ああ、］よかった|(Oh,) that's great / Thank goodness! (used to express a feeling of relief)|I01.L29

{.table .table-hover .table-sm .align-text-bottom .table-ruby-1}
