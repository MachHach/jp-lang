module.exports = {
    tableOfContent: [
        {id: 'numbering', name: 'Numbering'},
        {id: 'age', name: 'Age'},
        {id: 'room-number', name: 'Room number'},
        {id: 'phone-number', name: 'Phone number'},
        {id: 'platform', name: 'Platform'},
        {id: 'shelf', name: 'Shelf'},
    ],
}
