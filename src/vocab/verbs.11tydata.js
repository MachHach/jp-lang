module.exports = {
    tableOfContent: [
        {id: 'intransitive', name: 'Intransitive'},
        {id: 'transitive', name: 'Transitive'},
        {id: 'intransitive-transitive', name: 'Intransitive & transitive pairs'},
        {id: 'noun-based', name: 'Noun based'},
        {id: 'uncategorized', name: 'Uncategorized'},
    ],
}
