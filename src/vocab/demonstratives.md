---
layout: reader.html
title: 'Vocabulary: Demonstratives'
navbar:
  vocab: 'active'
---

# Demonstratives

Demonstratives are normally written in hiragana.

## Pronouns {#pronouns}

Formal | Informal | Kanji | Meaning | Refer
-- | -- | -- | -- | --
これ|こちら|此れ|this (thing here)|B00.L02, E02.L22
それ|そちら|其れ|that (thing near the listener)|B00.L02
あれ|あちら|彼れ|that (thing over there)|B00.L02
どれ|どちら|何れ|which one (of three of more things)|E01.L16

{.table .table-hover .table-sm}

Formal | Informal | Kanji | Meaning | Refer
-- | -- | -- | -- | --
こちら|こっち|此方|this way, this place|B00.L03, E02.L20
そちら|そっち|其方|that way, that place near the listener|B00.L03, E02.L20
あちら|あっち|彼方|that way, that place over there|B00.L03, E02.L20
どちら|どっち|何方|which way, where, which one (of two things)|B00.L03, E01.L12, E02.L20

{.table .table-hover .table-sm}

Word | Meaning | Refer
-- | -- | --
ここ|here, this place|B00.L03
そこ|there, that place near the listener|B00.L03
あそこ|that place over there|B00.L03
どこ|where, what place|B00.L03

{.table .table-hover .table-sm}

## Adjectives {#adjectives}

Word | Meaning | Refer
-- | -- | --
この～|this ~, this ~ here|B00.L02
その～|that ~, that ~ near the listener|B00.L02
あの～|that ~, that ~ over there|B00.L02
なんの～|what kind of ~|B00.L02
なん|what|B00.L02
どの～|which ~ (used for three or more)|E01.L16
こんな～|~ like this|I01.L26
そんな～|~ like that (near the listener)|I01.L26
あんな～|~ like that (far from both speaker and listener)|I01.L26

{.table .table-hover .table-sm}
