---
layout: reader.html
title: 'Vocabulary: Datetime'
navbar:
  vocab: 'active'
---

# Datetime {{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}

## Relative {#relative}

{{5|jlpt}} {{'B00.L04'|icls}}

Day | Morning | Night
--- | ------- | -----
<span class="fs-5">{{'一昨日'|ruby:'おととい'}}</span><br/>the day before yesterday|<span class="fs-5">{{'一昨日'|ruby:'おととい'}}の{{'朝日'|ruby:'あさ'}}</span><br/>the morning before last|<span class="fs-5">{{'一昨日'|ruby:'おととい'}}の{{'晩'|ruby:'ばん'}}（{{'夜'|ruby:'よる'}}）</span><br/>the night before last
<span class="fs-5">{{'昨日'|ruby:'きのう'}}</span><br/>yesterday|<span class="fs-5">{{'昨日'|ruby:'きのう'}}の{{'朝日'|ruby:'あさ'}}</span><br/>yesterday morning|<span class="fs-5">{{'昨日'|ruby:'きのう'}}の{{'晩'|ruby:'ばん'}}（{{'夜'|ruby:'よる'}}）</span><br/>last night
<span class="fs-5 red">{{'今日'|ruby:'きょう'}}</span><br/>today|<span class="fs-5 red">{{'今朝'|ruby:'けさ'}}</span><br/>this morning|<span class="fs-5"><span class="red">{{'今'|ruby:'こん'}}{{'晩'|ruby:'ばん'}}</span> / {{'今日'|ruby:'きょう'}}の{{'夜'|ruby:'よる'}}</span><br/>tonight
<span class="fs-5">{{'明日'|ruby:'あした'}}</span><br/>tomorrow|<span class="fs-5">{{'明日'|ruby:'あした'}}の{{'朝日'|ruby:'あさ'}}</span><br/>tomorrow morning|<span class="fs-5">{{'明日'|ruby:'あした'}}の{{'晩'|ruby:'ばん'}}（{{'夜'|ruby:'よる'}}）</span><br/>tomorrow night
<span class="fs-5">{{'明後日'|ruby:'あさって'}}</span><br/>the day after tomorrow|<span class="fs-5">{{'明後日'|ruby:'あさって'}}の{{'朝日'|ruby:'あさ'}}</span><br/>the morning after next|<span class="fs-5">{{'明後日'|ruby:'あさって'}}の{{'晩'|ruby:'ばん'}}（{{'夜'|ruby:'よる'}}）</span><br/>the night after next
<span class="fs-5 red">{{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}</span><br/>every day|<span class="fs-5">{{'毎'|ruby:'まい'}}{{'朝'|ruby:'あさ'}}</span><br/>every morning|<span class="fs-5">{{'毎'|ruby:'まい'}}{{'晩'|ruby:'ばん'}}</span><br/>every night

{.table .table-hover .table-sm}

Week | Month | Year
---- | ----- | ----
<span class="fs-5">{{'先'|ruby:'せん'}}{{'先'|ruby:'せん'}}{{'週'|ruby:'しゅう'}} / {{'二'|ruby:'に'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}{{'前'|ruby:'まえ'}}</span><br/>the week before last|<span class="fs-5">{{'先'|ruby:'せん'}}{{'先'|ruby:'せん'}}{{'月'|ruby:'げつ'}} / {{'二'|ruby:'に'}}か{{'月'|ruby:'げつ'}}{{'前'|ruby:'まえ'}}</span><br/>the month before last|<span class="fs-5">{{'一昨年'|ruby:'おととし'}}</span><br/>the year before last
<span class="fs-5">{{'先'|ruby:'せん'}}{{'週'|ruby:'しゅう'}}</span><br/>last week|<span class="fs-5">{{'先'|ruby:'せん'}}{{'月'|ruby:'げつ'}}</span><br/>last month|<span class="fs-5"><span class="red">{{'去'|ruby:'きょ'}}</span>{{'年'|ruby:'ねん'}}</span><br/>last year
<span class="fs-5">{{'今'|ruby:'こん'}}{{'週'|ruby:'しゅう'}}</span><br/>this week|<span class="fs-5">{{'今'|ruby:'こん'}}{{'月'|ruby:'げつ'}}</span><br/>this month|<span class="fs-5 red">{{'今'|ruby:'こ'}}{{'年'|ruby:'とし'}}</span><br/>this year
<span class="fs-5">{{'来'|ruby:'らい'}}{{'週'|ruby:'しゅう'}}</span><br/>next week|<span class="fs-5">{{'来'|ruby:'らい'}}{{'月'|ruby:'げつ'}}</span><br/>next month|<span class="fs-5">{{'来'|ruby:'らい'}}{{'年'|ruby:'ねん'}}</span><br/>next year
<span class="fs-5">{{'再'|ruby:'さ'}}{{'来'|ruby:'らい'}}{{'週'|ruby:'しゅう'}}</span><br/>the week after next|<span class="fs-5">{{'再'|ruby:'さ'}}{{'来'|ruby:'らい'}}{{'月'|ruby:'げつ'}}</span><br/>the month after next|<span class="fs-5">{{'再'|ruby:'さ'}}{{'来'|ruby:'らい'}}{{'年'|ruby:'ねん'}}</span><br/>the year after next
<span class="fs-5">{{'毎'|ruby:'まい'}}{{'週'|ruby:'しゅう'}}</span><br/>every week|<span class="fs-5">{{'毎'|ruby:'まい'}}<span class="red">{{'月'|ruby:'つき'}}</span></span><br/>every month|<span class="fs-5">{{'毎'|ruby:'まい'}}{{'年'|ruby:'とし'}}、{{'毎'|ruby:'まい'}}{{'年'|ruby:'ねん'}}</span><br/>every year

{.table .table-hover .table-sm}

Noun | Meaning | Refer
---- | ------- | -----
{{'週'|ruby:'しゅう'}}{{'末'|ruby:'まつ'}}|weekend|E01.L13
{{'平'|ruby:'へい'}}{{'日'|ruby:'じつ'}}|weekday|I01.L26
{{'日'|ruby:'ひ'}}にち|date|I01.L28

{.table .table-hover .table-sm}

## Absolute {#absolute}

{{5|jlpt}} {{'B00.L04'|icls}} {{'E01.L11'|icls}}

### Period {#period}

Period | Word
------ | ----
a.m.|{{'午'|ruby:'ご'}}{{'前'|ruby:'ぜん'}}
p.m.|{{'午'|ruby:'ご'}}{{'後'|ruby:'ご'}}
Now|{{'今'|ruby:'いま'}}
Morning|{{'朝'|ruby:'あさ'}}
Daytime, noon|{{'昼'|ruby:'ひる'}}
Night, evening|{{'晩'|ruby:'ばん'}} / {{'夜'|ruby:'よる'}}

{.table .table-hover .table-sm .table-ruby-2}

### O'clock {{'時'|ruby:'じ'}} & Hour {{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}} {#hour}

Roman | O'clock | Hour
----: | ------: | ---:
1|{{'一'|ruby:'いち'}}{{'時'|ruby:'じ'}}|{{'一'|ruby:'いち'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
2|{{'二'|ruby:'に'}}{{'時'|ruby:'じ'}}|{{'二'|ruby:'に'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
3|{{'三'|ruby:'さん'}}{{'時'|ruby:'じ'}}|{{'三'|ruby:'さん'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
4|<span class="red">{{'四'|ruby:'よ'}}</span>{{'時'|ruby:'じ'}}|<span class="red">{{'四'|ruby:'よ'}}</span>{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
5|{{'五'|ruby:'ご'}}{{'時'|ruby:'じ'}}|{{'五'|ruby:'ご'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
6|{{'六'|ruby:'ろく'}}{{'時'|ruby:'じ'}}|{{'六'|ruby:'ろく'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
7|<span class="red">{{'七'|ruby:'しち'}}</span>{{'時'|ruby:'じ'}}|{{'七'|ruby:'なな'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}、<span class="red">{{'七'|ruby:'しち'}}</span>{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
8|{{'八'|ruby:'はち'}}{{'時'|ruby:'じ'}}|{{'八'|ruby:'はち'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
9|<span class="red">{{'九'|ruby:'く'}}</span>{{'時'|ruby:'じ'}}|<span class="red">{{'九'|ruby:'く'}}</span>{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
10|{{'十'|ruby:'じゅう'}}{{'時'|ruby:'じ'}}|{{'十'|ruby:'じゅう'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
11|{{'十'|ruby:'じゅう'}}{{'一'|ruby:'いち'}}{{'時'|ruby:'じ'}}|{{'十'|ruby:'じゅう'}}{{'一'|ruby:'いち'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
12|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'時'|ruby:'じ'}}|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}
?|{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}|{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}

{.table .table-hover .table-sm .table-ruby-2 .table-ruby-3}

### Minute {{'分'|ruby:'ぷん'}} {#minute}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'いっ'}}{{'分'|ruby:'ぷん'}}</span>
2|{{'二'|ruby:'に'}}{{'分'|ruby:'ふん'}}
3|{{'三'|ruby:'さん'}}<span class="red">{{'分'|ruby:'ぷん'}}</span>
4|{{'四'|ruby:'よん'}}<span class="red">{{'分'|ruby:'ぷん'}}</span>
5|{{'五'|ruby:'ご'}}{{'分'|ruby:'ふん'}}
6|<span class="red">{{'六'|ruby:'ろっ'}}{{'分'|ruby:'ぷん'}}</span>
7|{{'七'|ruby:'なな'}}{{'分'|ruby:'ふん'}}
8|<span class="red">{{'八'|ruby:'はっ'}}{{'分'|ruby:'ぷん'}}</span>
9|{{'九'|ruby:'きゅう'}}{{'分'|ruby:'ふん'}}
10|<span class="red">{{'十'|ruby:'じゅっ'}}{{'分'|ruby:'ぷん'}}</span>、<span class="red">{{'十'|ruby:'じっ'}}{{'分'|ruby:'ぷん'}}</span>
15|{{'十'|ruby:'じゅう'}}{{'五'|ruby:'ご'}}{{'分'|ruby:'ふん'}}
20|{{'二'|ruby:'に'}}<span class="red">{{'十'|ruby:'じゅっ'}}{{'分'|ruby:'ぷん'}}</span>、{{'二'|ruby:'に'}}<span class="red">{{'十'|ruby:'じっ'}}{{'分'|ruby:'ぷん'}}</span>
30|{{'三'|ruby:'さん'}}<span class="red">{{'十'|ruby:'じゅっ'}}{{'分'|ruby:'ぷん'}}</span>、{{'三'|ruby:'さん'}}<span class="red">{{'十'|ruby:'じっ'}}{{'分'|ruby:'ぷん'}}</span>、<span class="red">{{'半'|ruby:'はん'}}</span>
40|{{'四'|ruby:'よん'}}<span class="red">{{'十'|ruby:'じゅっ'}}{{'分'|ruby:'ぷん'}}</span>、{{'四'|ruby:'よん'}}<span class="red">{{'十'|ruby:'じっ'}}{{'分'|ruby:'ぷん'}}</span>
50|{{'五'|ruby:'ご'}}<span class="red">{{'十'|ruby:'じゅっ'}}{{'分'|ruby:'ぷん'}}</span>、{{'五'|ruby:'ご'}}<span class="red">{{'十'|ruby:'じっ'}}{{'分'|ruby:'ぷん'}}</span>
?|{{'何'|ruby:'なん'}}<span class="red">{{'分'|ruby:'ぷん'}}</span>

{.table .table-hover .table-sm .table-ruby-2}

### Day of week {{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}} {#day-of-week}

Roman | Meaning | Word | Icon
----: | ------- | ---: | ----
0|Sunday|{{'日'|ruby:'にち'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|☀
1|Monday|{{'月'|ruby:'げつ'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|🌘
2|Tuesday|{{'火'|ruby:'か'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|🔥
3|Wednesday|{{'水'|ruby:'すい'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|💧
4|Thursday|{{'木'|ruby:'もく'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|🌴
5|Friday|{{'金'|ruby:'きん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|💰
6|Saturday|{{'土'|ruby:'ど'}}、{{'土'|ruby:'ど'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|🗻
?|What day|{{'何'|ruby:'なん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}|❔

{.table .table-hover .table-sm .table-ruby-3}

### Week {{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}} {#week}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'いっ'}}</span>{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
2|{{'二'|ruby:'に'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
3|{{'三'|ruby:'さん'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
4|{{'四'|ruby:'よん'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
5|{{'五'|ruby:'ご'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
6|{{'六'|ruby:'ろく'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
7|{{'七'|ruby:'なな'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
8|<span class="red">{{'八'|ruby:'はっ'}}</span>{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
9|{{'九'|ruby:'じゅう'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
10|<span class="red">{{'十'|ruby:'じゅっ'}}</span>{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}、<span class="red">{{'十'|ruby:'じっ'}}</span>{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}
?|{{'何'|ruby:'なん'}}{{'週'|ruby:'しゅう'}}{{'間'|ruby:'かん'}}

{.table .table-hover .table-sm .table-ruby-2}

### Month of year {{'月'|ruby:'がつ'}} & Month か{{'月'|ruby:'げつ'}} {#month}

Roman | Meaning (month of year) | Month of year | Months
----: | ----------------------- | ------------: | -----:
1|January|{{'一'|ruby:'いち'}}{{'月'|ruby:'がつ'}}|<span class="red">{{'一'|ruby:'いっ'}}</span>か{{'月'|ruby:'げつ'}}
2|February|{{'二'|ruby:'に'}}{{'月'|ruby:'がつ'}}|{{'二'|ruby:'に'}}か{{'月'|ruby:'げつ'}}
3|March|{{'三'|ruby:'さん'}}{{'月'|ruby:'がつ'}}|{{'三'|ruby:'さん'}}か{{'月'|ruby:'げつ'}}
4|April|<span class="red">{{'四'|ruby:'し'}}</span>{{'月'|ruby:'がつ'}}|{{'四'|ruby:'よん'}}か{{'月'|ruby:'げつ'}}
5|May|{{'五'|ruby:'ご'}}{{'月'|ruby:'がつ'}}|{{'五'|ruby:'ご'}}か{{'月'|ruby:'げつ'}}
6|June|{{'六'|ruby:'ろく'}}{{'月'|ruby:'がつ'}}|<span class="red">{{'六'|ruby:'ろっ'}}</span>か{{'月'|ruby:'げつ'}}、<span class="red">{{'半'|ruby:'はん'}}{{'年'|ruby:'とし'}}</span>
7|July|<span class="red">{{'七'|ruby:'しち'}}</span>{{'月'|ruby:'がつ'}}|{{'七'|ruby:'なな'}}か{{'月'|ruby:'げつ'}}
8|August|{{'八'|ruby:'はち'}}{{'月'|ruby:'がつ'}}|{{'八'|ruby:'はち'}}か{{'月'|ruby:'げつ'}}、{{'八'|ruby:'はっ'}}か{{'月'|ruby:'げつ'}}
9|September|<span class="red">{{'九'|ruby:'く'}}</span>{{'月'|ruby:'がつ'}}|{{'九'|ruby:'きゅう'}}か{{'月'|ruby:'げつ'}}
10|October|{{'十'|ruby:'じゅう'}}{{'月'|ruby:'がつ'}}|{{'十'|ruby:'じゅっ'}}か{{'月'|ruby:'げつ'}}、{{'十'|ruby:'じっ'}}か{{'月'|ruby:'げつ'}}
11|November|{{'十'|ruby:'じゅう'}}{{'一'|ruby:'いち'}}{{'月'|ruby:'がつ'}}|{{'十'|ruby:'じゅう'}}<span class="red">{{'一'|ruby:'いっ'}}</span>か{{'月'|ruby:'げつ'}}
12|December|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'月'|ruby:'がつ'}}|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}か{{'月'|ruby:'げつ'}}
?|What month|{{'何'|ruby:'なん'}}{{'月'|ruby:'がつ'}}|{{'何'|ruby:'なん'}}か{{'月'|ruby:'げつ'}}

{.table .table-hover .table-sm .table-ruby-3 .table-ruby-4}

### Day {{'日'|ruby:'ひ'}} {#day}

Roman | Word
----- | ---:
1|<span class="red">{{'一日'|ruby:'ついたち'}}</span> (day of week) / <span class="red">{{'一'|ruby:'いち'}}</span>{{'日'|ruby:'にち'}} (day)
2|<span class="red">{{'二'|ruby:'ふつ'}}{{'日'|ruby:'か'}}</span>
3|<span class="red">{{'三'|ruby:'みっ'}}{{'日'|ruby:'か'}}</span>
4|<span class="red">{{'四'|ruby:'よっ'}}{{'日'|ruby:'か'}}</span>
5|<span class="red">{{'五'|ruby:'いつ'}}{{'日'|ruby:'か'}}</span>
6|<span class="red">{{'六'|ruby:'むい'}}{{'日'|ruby:'か'}}</span>
7|<span class="red">{{'七'|ruby:'なの'}}{{'日'|ruby:'か'}}</span>
8|<span class="red">{{'八'|ruby:'よう'}}{{'日'|ruby:'か'}}</span>
9|<span class="red">{{'九'|ruby:'ここの'}}{{'日'|ruby:'か'}}</span>
10|<span class="red">{{'十'|ruby:'とお'}}{{'日'|ruby:'か'}}</span>
11|{{'十'|ruby:'じゅう'}}{{'一'|ruby:'いち'}}{{'日'|ruby:'にち'}}
12|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'日'|ruby:'にち'}}
13|{{'十'|ruby:'じゅう'}}{{'三'|ruby:'さん'}}{{'日'|ruby:'にち'}}
14|{{'十'|ruby:'じゅう'}}<span class="red">{{'四'|ruby:'よっ'}}{{'日'|ruby:'か'}}</span>
15|{{'十'|ruby:'じゅう'}}{{'五'|ruby:'ご'}}{{'日'|ruby:'にち'}}
16|{{'十'|ruby:'じゅう'}}{{'六'|ruby:'ろく'}}{{'日'|ruby:'にち'}}
17|{{'十'|ruby:'じゅう'}}<span class="red">{{'七'|ruby:'しち'}}</span>{{'日'|ruby:'にち'}}
18|{{'十'|ruby:'じゅう'}}{{'八'|ruby:'はち'}}{{'日'|ruby:'にち'}}
19|{{'十'|ruby:'じゅう'}}<span class="red">{{'九'|ruby:'く'}}</span>{{'日'|ruby:'にち'}}
20|<span class="red">{{'二十'|ruby:'はつ'}}{{'日'|ruby:'か'}}</span>
24|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'四'|ruby:'よっ'}}{{'日'|ruby:'か'}}</span>
27|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'七'|ruby:'しち'}}</span>{{'日'|ruby:'にち'}}
29|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'九'|ruby:'く'}}</span>{{'日'|ruby:'にち'}}
?|{{'何'|ruby:'なん'}}{{'日'|ruby:'にち'}}

{.table .table-hover .table-sm .table-ruby-2}

### Year {{'年'|ruby:'ねん'}} {#year}

Roman | Word
----- | ---:
1|{{'一'|ruby:'いち'}}{{'年'|ruby:'ねん'}}
2|{{'二'|ruby:'に'}}{{'年'|ruby:'ねん'}}
3|{{'三'|ruby:'さん'}}{{'年'|ruby:'ねん'}}
4|<span class="red">{{'四'|ruby:'よ'}}</span>{{'年'|ruby:'ねん'}}
5|{{'五'|ruby:'ご'}}{{'年'|ruby:'ねん'}}
6|{{'六'|ruby:'ろく'}}{{'年'|ruby:'ねん'}}
7|{{'七'|ruby:'なな'}}{{'年'|ruby:'ねん'}}、{{'七'|ruby:'しち'}}{{'年'|ruby:'ねん'}}
8|{{'八'|ruby:'はち'}}{{'年'|ruby:'ねん'}}
9|{{'九'|ruby:'きゅう'}}{{'年'|ruby:'ねん'}}
10|{{'十'|ruby:'じゅう'}}{{'年'|ruby:'ねん'}}
?|{{'何'|ruby:'なん'}}{{'年'|ruby:'ねん'}}

{.table .table-hover .table-sm .table-ruby-2}
