---
layout: reader.html
title: 'Vocabulary: Pronouns'
navbar:
  vocab: 'active'
---

# Pronouns

## Personal {#personal}

Word | Meaning | Type | Refer
-- | -- | -- | --
{{'私'|ruby:'わたし'}}|I|1st person|B00.L01
あなた|you|2nd person|B00.L01
あの{{'人'|ruby:'ひと'}}|that person, he, she (informal)|3rd person|B00.L01
あの{{'方'|ruby:'かた'}}|that person, he, she (formal)|3rd person|B00.L01
{{'誰'|ruby:'だれ'}}|who (informal)|Interrogative|B00.L01
どなた|who (formal)|Interrogative|B00.L01
{{'僕'|ruby:'ぼく'}}|I (informal of {{'私'|ruby:'わたし'}} for young men / boys)|1st person|E02.L20
{{'俺'|ruby:'おれ'}}|I (informal of {{'私'|ruby:'わたし'}} for men)|1st person|E02.L20
{{'君'|ruby:'きみ'}}|you (informal of あなた for people of equal/lower status, only used in anime/songs)|2nd person|E02.L20
{{'私'|ruby:'わたし'}}たち|we|1st person (plural)|E02.L22

{.table .table-hover .table-sm .table-ruby-1}

## Suffixes {#suffixes}

Word | Meaning | Refer
-- | -- | --
～さん|Mr., Ms. (added to a name for expressing politeness, not used when referring to self)|B00.L01
～ちゃん|(often added to a child's name) / Ms. (informal of ～さん for girls of equal/lower status)|B00.L01, E02.L20
～じん|(meaning 'a national of')|B00.L01
～{{'君'|ruby:'くん'}}|Mr. (informal of ～さん for boys of equal/lower status)|E02.L20
～{{'様'|ruby:'さま'}}|Mr. ~ / Ms. ~ (respectful equivalent of ～さん)|I01.L26

{.table .table-hover .table-sm}
