module.exports = {
    tableOfContent: [
        {id: 'relative', name: 'Relative'},
        {id: 'absolute', name: 'Absolute', children: [
            {id: 'period', name: 'Period'},
            {id: 'hour', name: 'Hour'},
            {id: 'minute', name: 'Minute'},
            {id: 'day-of-week', name: 'Day of week'},
            {id: 'week', name: 'Week'},
            {id: 'month', name: 'Month'},
            {id: 'day', name: 'Day'},
            {id: 'year', name: 'Year'},
        ]},
    ],
}
