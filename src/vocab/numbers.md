---
layout: reader.html
title: 'Vocabulary: Numbers'
navbar:
  vocab: 'active'
---

# Numbers {{'数'|ruby:'すう'}}{{'字'|ruby:'じ'}}

## Numbering {#numbering}

{{5|jlpt}} {{'B00.L01'|icls}}

Roman | Word
----: | ---:
0|{{'零'|ruby:'れい'}}
10|{{'十'|ruby:'じゅう'}}
100|{{'百'|ruby:'ひゃく'}}
1,000|{{'千'|ruby:'せん'}}
10,000|{{'万'|ruby:'まん'}}
100,000|{{'十'|ruby:'じゅう'}}{{'万'|ruby:'まん'}}
1,000,000|{{'百'|ruby:'ひゃく'}}{{'万'|ruby:'まん'}}
100,000,000|{{'億'|ruby:'おく'}}

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

Roman | Word
----: | ----
0|れい／ゼロ
1|<span class="red">いち</span>
2|<span class="red">に</span>
3|<span class="red">さん</span>
4|<span class="red">よん</span>／し／（よ）
5|<span class="red">ご</span>
6|<span class="red">ろく</span>
7|<span class="red">なな</span>／しち
8|<span class="red">はち</span>
9|<span class="red">きゅう</span>／く
10|じゅう
11|じゅういち
12|じゅうに
13|じゅうさん
14|じゅうよん／じゅうし
15|じゅうご
16|じゅうろく
17|じゅうなな／じゅうしち
18|じゅうはち
19|じゅうきゅう／じゅうく
20|にじゅう
30|さんじゅう
40|よんじゅう
50|ごじゅう
60|ろくじゅう
70|ななじゅう
80|はちじゅう
90|きゅうじゅう
100|ひゃく
200|にひゃく
300|さん<span class="red">びゃく</span>
400|よんひゃく
500|ごひゃく
600|<span class="red">ろっぴゃく</span>
700|ななひゃく
800|<span class="red">はっぴゃく</span>
900|きゅうひゃく
1,000|せん
2,000|にせん
3,000|さん<span class="red">ぜん</span>
4,000|よんせん
5,000|ごせん
6,000|ろくせん
7,000|ななせん
8,000|<span class="red">はっせん</span>
9,000|きゅうせん
10,000|<span class="red">いちまん</span>

{.table .table-hover .table-sm}

Roman | Word
----: | ----
17.5|{{'十'|ruby:'じゅう'}}{{'七'|ruby:'なな'}}{{'点'|ruby:'てん'}}{{'五'|ruby:'ご'}}
0.83|{{'零'|ruby:'れい'}}{{'点'|ruby:'てん'}}{{'八'|ruby:'はち'}}{{'三'|ruby:'さん'}}
1/2|{{'二'|ruby:'に'}}{{'分'|ruby:'ぶん'}}の{{'一'|ruby:'いち'}}
3/4|{{'四'|ruby:'よん'}}{{'分'|ruby:'ぶん'}}の{{'三'|ruby:'さん'}}

{.table .table-hover .table-sm .table-ruby-2}

## Age {{'年'|ruby:'ねん'}}{{'齢'|ruby:'れい'}} {#age}

{{5|jlpt}} {{'B00.L01'|icls}}

Roman | Word
----: | ---:
1|<span class="red">{{'一'|ruby:'いっ'}}</span>{{'歳'|ruby:'さい'}}
2|{{'二'|ruby:'に'}}{{'歳'|ruby:'さい'}}
3|{{'三'|ruby:'さん'}}{{'歳'|ruby:'さい'}}
4|{{'四'|ruby:'よん'}}{{'歳'|ruby:'さい'}}
5|{{'五'|ruby:'ご'}}{{'歳'|ruby:'さい'}}
6|{{'六'|ruby:'ろく'}}{{'歳'|ruby:'さい'}}
7|{{'七'|ruby:'なな'}}{{'歳'|ruby:'さい'}}
8|<span class="red">{{'八'|ruby:'はっ'}}</span>{{'歳'|ruby:'さい'}}
9|{{'九'|ruby:'きゅう'}}{{'歳'|ruby:'さい'}}
10|<span class="red">{{'十'|ruby:'じゅっ'}}</span>{{'歳'|ruby:'さい'}}
11|{{'十'|ruby:'じゅう'}}<span class="red">{{'一'|ruby:'いっ'}}</span>{{'歳'|ruby:'さい'}}
12|{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'歳'|ruby:'さい'}}
13|{{'十'|ruby:'じゅう'}}{{'三'|ruby:'さん'}}{{'歳'|ruby:'さい'}}
18|{{'十'|ruby:'じゅう'}}<span class="red">{{'八'|ruby:'はっ'}}</span>{{'歳'|ruby:'さい'}}
19|{{'十'|ruby:'じゅう'}}{{'九'|ruby:'きゅう'}}{{'歳'|ruby:'さい'}}
20|<span class="red">{{'二十歳'|ruby:'はたち'}}</span>
21|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'一'|ruby:'いっ'}}</span>{{'歳'|ruby:'さい'}}
22|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'歳'|ruby:'さい'}}
28|{{'二'|ruby:'に'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'八'|ruby:'はっ'}}</span>{{'歳'|ruby:'さい'}}
30|{{'三'|ruby:'さん'}}<span class="red">{{'十'|ruby:'じゅっ'}}</span>{{'歳'|ruby:'さい'}}
31|{{'三'|ruby:'さん'}}{{'十'|ruby:'じゅう'}}<span class="red">{{'一'|ruby:'いっ'}}</span>{{'歳'|ruby:'さい'}}
32|{{'三'|ruby:'さん'}}{{'十'|ruby:'じゅう'}}{{'二'|ruby:'に'}}{{'歳'|ruby:'さい'}}
?|{{'何'|ruby:'なん'}}{{'歳'|ruby:'さい'}}

{.table .table-hover .table-sm .table-ruby-2}

## Room number {#room-number}

{{5|jlpt}} {{'B00.L02'|icls}}

For room numbers, a zero is pronounced as __まる__ (not れい / ゼロ).

Example for a room number "408":

{{'4'|ruby:'よん'}}<span class="red">{{'0'|ruby:'まる'}}</span>{{'8'|ruby:'はち'}}
{.fs-5}

## Phone number {{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}{{'番'|ruby:'ばん'}}{{'号'|ruby:'ご'}} {#phone-number}

{{5|jlpt}} {{'B00.L04'|icls}}

For phone numbers:

- A zero is pronounced as __ゼロ__ (not れい).
- A hyphen is pronounced as __の__.

Example for a phone number "929-0078":

{{'9'|ruby:'きゅう'}}{{'2'|ruby:'に'}}{{'9'|ruby:'きゅう'}}{{'-'|ruby:'の'}}<span class="red">{{'0'|ruby:'ゼロ'}}{{'0'|ruby:'ゼロ'}}</span>{{'7'|ruby:'なな'}}{{'8'|ruby:'はち'}}
{.fs-5}

To ask for phone number, use __{{'何'|ruby:'なん'}}{{'番'|ruby:'ばん'}}__.

Example:

{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}{{'番'|ruby:'ばん'}}{{'号'|ruby:'ご'}}は<span class="red">{{'何'|ruby:'なん'}}{{'番'|ruby:'ばん'}}</span>ですか
{.fs-5}

## Platform {#platform}

{{5|jlpt}} {{'B00.L05'|icls}}

Roman | Word | Meaning
----: | ---: | -------
1|{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}{{'線'|ruby:'せん'}}|platform 1, 1st platform
2|{{'二'|ruby:'に'}}{{'番'|ruby:'ばん'}}{{'線'|ruby:'せん'}}|platform 2, 2nd platform
?|{{'何'|ruby:'なん'}}{{'番'|ruby:'ばん'}}{{'線'|ruby:'せん'}}|which platform

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## Shelf {{'棚'|ruby:'たな'}} {#shelf}

{{5|jlpt}} {{'E01.L10'|icls}}

Shelf row | Count from top | Count from bottom
--- | --- | ---
1st (top)|{{'上'|ruby:'うえ'}}から１{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}（{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}{{'上'|ruby:'うえ'}}）|{{'下'|ruby:'した'}}から４{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}
2nd|{{'上'|ruby:'うえ'}}から２{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}|{{'下'|ruby:'した'}}から３{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}
3rd|{{'上'|ruby:'うえ'}}から３{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}|{{'下'|ruby:'した'}}から２{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}
4th (bottom)|{{'上'|ruby:'うえ'}}から４{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}|{{'下'|ruby:'した'}}から１{{'段'|ruby:'だん'}}{{'目'|ruby:'め'}}（{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}{{'下'|ruby:'した'}}）

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-3}
