---
layout: reader.html
title: 'Vocabulary: Adjectives'
navbar:
  vocab: 'active'
---

# Adjective の {{'言'|ruby:'こと'}}{{'葉'|ruby:'ば'}}

## Temperature {#temperature}

Type | Adjective | Meaning | Refer
---- | --------- | ------- | -----
い|{{'暑'|ruby:'あつ'}}い|hot (temperature)|B00.L08
い|{{'熱'|ruby:'あつ'}}い|hot (touch)|B00.L08
い|{{'寒'|ruby:'さむ'}}い|cold (temperature)|B00.L08
い|{{'冷'|ruby:'つめ'}}たい|cold (touch)|B00.L08
い|{{'暖'|ruby:'あたた'}}かい|warm|E01.L12
い|{{'温'|ruby:'あたた'}}かい|warm|E01.L12
い|{{'涼'|ruby:'すず'}}しい|cool|E01.L12

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## Colors {#colors}

Type | Adjective | Meaning | Refer
---- | --------- | ------- | -----
い|{{'白'|ruby:'しろ'}}い|white|B00.L08
い|{{'黒'|ruby:'くろ'}}い|black|B00.L08
い|{{'赤'|ruby:'あか'}}い|red|B00.L08
い|{{'青'|ruby:'あお'}}い|blue|B00.L08

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2}

## Other {#other}

Type | Adjective | Meaning | Example | Refer
---- | --------- | ------- | ------- | -----
な|ハンサム|handsome|-|B00.L08
な|きれい|beautiful, clean|-|B00.L08
な|{{'静'|ruby:'しず'}}か|quiet|-|B00.L08
な|{{'賑'|ruby:'にぎ'}}やか|lively|-|B00.L08
な|{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}|famous|-|B00.L08
な|{{'親'|ruby:'しん'}}{{'切'|ruby:'せつ'}}|helpful, kind, considerate (not used about one's own family members)|-|B00.L08
な|{{'元'|ruby:'げん'}}{{'気'|ruby:'き'}}|healthy, energetic, cheerful|-|B00.L08
な|{{'暇'|ruby:'ひま'}}|free (time)|-|B00.L08
な|{{'便'|ruby:'べん'}}{{'利'|ruby:'り'}}|convenient|-|B00.L08
な|すてき|fine, nice, wonderful|-|B00.L08
な|{{'好'|ruby:'す'}}き|like|-|E01.L09
な|{{'嫌'|ruby:'きら'}}い|dislike|-|E01.L09
な|{{'上'|ruby:'じょう'}}{{'手'|ruby:'ず'}}|good at|-|E01.L09
な|{{'得'|ruby:'とく'}}{{'意'|ruby:'い'}}|good at (describe ownself)|-|E01.L09
な|{{'下'|ruby:'へ'}}{{'手'|ruby:'た'}}|poor at|-|E01.L09
な|いろいろ|various|-|E01.L10, E02.L20
な|{{'簡'|ruby:'かん'}}{{'単'|ruby:'たん'}}|easy, simple|-|E01.L12
な|{{'大'|ruby:'たい'}}{{'変'|ruby:'へん'}}|hard, tough, severe, awful|-|E01.L13
な|{{'大'|ruby:'たい'}}{{'切'|ruby:'せつ'}}|important, precious|-|E02.L17
な|{{'大'|ruby:'だい'}}{{'丈'|ruby:'じょう'}}{{'夫'|ruby:'ぶ'}}|all right|-|E02.L17
な|{{'無'|ruby:'む'}}{{'理'|ruby:'り'}}|excessive, impossible|-|E02.L19
な|むだ|wasteful|-|E02.L21
な|{{'不'|ruby:'ふ'}}{{'便'|ruby:'べん'}}|inconvenient|-|E02.L21
な|{{'心'|ruby:'しん'}}{{'配'|ruby:'ぱい'}}|worried, anxious|-|I01.L27
な|{{'大'|ruby:'だい'}}{{'好'|ruby:'す'}}き|like very much|-|I01.L27
な|{{'不'|ruby:'ふ'}}{{'思'|ruby:'し'}}{{'議'|ruby:'ぎ'}}|fantastic, mysterious|-|I01.L27
な|まじめ|serious|-|I01.L28
な|{{'熱'|ruby:'ねっ'}}{{'心'|ruby:'しん'}}|earnest|-|I01.L28
い|{{'大'|ruby:'おお'}}きい|big, large|-|B00.L08
い|{{'小'|ruby:'ちい'}}さい|small, little|-|B00.L08
い|{{'新'|ruby:'あたら'}}しい|new, fresh|-|B00.L08
い|{{'古'|ruby:'ふる'}}い|old (not used to describe a person's age)|-|B00.L08
い|いい（よい）|good|-|B00.L08
い|{{'悪'|ruby:'わる'}}い|bad|-|B00.L08
い|{{'難'|ruby:'むずか'}}しい|difficult|-|B00.L08
い|やさしい|easy|-|B00.L08
い|{{'高'|ruby:'たか'}}い|expensive, tall, high|-|B00.L08
い|{{'安'|ruby:'やす'}}い|inexpensive, cheap|-|B00.L08
い|{{'低'|ruby:'ひく'}}い|low|-|B00.L08
い|{{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}い|interesting|-|B00.L08
い|おいしい|delicious, tasty|-|B00.L08
い|{{'忙'|ruby:'いそが'}}しい|busy|-|B00.L08
い|{{'楽'|ruby:'たの'}}しい|enjoyable|-|B00.L08
い|{{'近'|ruby:'ちか'}}い|near|-|E01.L12
い|{{'遠'|ruby:'とお'}}い|far|-|E01.L12
い|{{'速'|ruby:'はや'}}い|fast|-|E01.L12
い|{{'早'|ruby:'はや'}}い|early|-|E01.L12
い|{{'遅'|ruby:'おそ'}}い|slow, late|-|E01.L12
い|{{'多'|ruby:'おお'}}い|many, much|{{'人'|ruby:'ひと'}}<span class="red">が</span>{{'多'|ruby:'おお'}}い|E01.L12
い|{{'少'|ruby:'すく'}}ない|few, a little|{{'人'|ruby:'ひと'}}<span class="red">が</span>{{'少'|ruby:'すく'}}ない|E01.L12
い|{{'甘'|ruby:'あま'}}い|sweet|-|E01.L12
い|{{'辛'|ruby:'から'}}い|hot (taste), spicy|-|E01.L12
い|{{'重'|ruby:'おも'}}い|heavy|-|E01.L12
い|{{'軽'|ruby:'かる'}}い|light|-|E01.L12
い|いい|prefer|コーヒー<span class="red">が</span>いい|E01.L12
い|{{'欲'|ruby:'ほ'}}しい|want (something)|-|E01.L13
い|{{'広'|ruby:'ひろ'}}い|wide, spacious|-|E01.L13
い|{{'狭'|ruby:'せま'}}い|narrow, small (room, etc.)|-|E01.L13
い|{{'若'|ruby:'わか'}}い|young|-|E01.L16
い|{{'長'|ruby:'なが'}}い|long|-|E01.L16
い|{{'短'|ruby:'みじか'}}い|short (length)|-|E01.L16
い|{{'明'|ruby:'あか'}}るい|bright, light|-|E01.L16
い|{{'暗'|ruby:'くら'}}い|dark|-|E01.L16
い|{{'危'|ruby:'あぶ'}}ない|dangerous|-|E02.L17
い|{{'眠'|ruby:'ねむ'}}い|sleepy|-|E02.L19
い|{{'強'|ruby:'つよ'}}い|strong|-|E02.L19
い|{{'弱'|ruby:'よわ'}}い|weak|-|E02.L19
い|すごい|awful, great (expresses astonishment/admiration)|すごい{{'人'|ruby:'ひと'}}です<br/>{{'人'|ruby:'ひと'}}<span class="red">が</span>すごいです|E02.L21
い|{{'寂'|ruby:'さび'}}しい|lonely|-|E02.L23
い|{{'気'|ruby:'き'}}{{'分'|ruby:'ぶん'}}がいい（よい）|feel well|-|I01.L26
い|{{'気'|ruby:'き'}}{{'分'|ruby:'ぶん'}}が{{'悪'|ruby:'わる'}}い|feel ill|-|I01.L26
い|{{'怖'|ruby:'こわ'}}い|be afraid of|-|I01.L26
い|すばらしい|wonderful|-|I01.L27
い|{{'偉'|ruby:'えら'}}い|great, admirable|-|I01.L28
い|ちょうどいい|proper, just right|-|I01.L28
の|{{'他'|ruby:'ほか'}}|other|{{'他'|ruby:'ほか'}}の|I01.L27
の|{{'昔'|ruby:'むかし'}}|old days, ancient times|-|I01.L27
の|{{'無'|ruby:'む'}}{{'料'|ruby:'りょう'}}|free of charge|-|I01.L28

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-4}
