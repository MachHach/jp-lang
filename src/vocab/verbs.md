---
layout: reader.html
title: 'Vocabulary: Verbs'
navbar:
  vocab: 'active'
---

# Verb の {{'言'|ruby:'こと'}}{{'葉'|ruby:'ば'}}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Describing the "type" of a verb:

- 五段 = Godan. Parenthesis indicates the euphonic change for te-form
- 一段 = Ichidan. Parenthesis indicates the sub-type: 上 = Kami (end with *-i* sound), 下 = Shimo (end with *-e* sound)
- カ行 = Ka-gyō.

All noun based verbs are of type サ行 (Sa-gyō).
</div>

## Intransitive {#intransitive}

Type | Word | Meaning | Example | Refer
---- | ---: | ------- | ------- | -----
五段 (って)|{{'行'|ruby:'い'}}きます|go|-|B00.L05
五段 (って)|{{'帰'|ruby:'かえ'}}ります|go home, return|-|B00.L05
五段 (って)|あります|have; exist, be (inanimate things); be held, take place|これ<span class="red">が</span>ある<br/>お{{'祭'|ruby:'まつ'}}り<span class="red">が</span>ある|E01.L09, E01.L10, E02.L21
五段 (って)|{{'待'|ruby:'ま'}}ちます|wait|-|E01.L14
五段 (って)|{{'座'|ruby:'すわ'}}ります|sit down|{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="red">に</span>{{'座'|ruby:'すわ'}}る|E01.L14
五段 (って)|{{'持'|ruby:'も'}}って{{'行'|ruby:'い'}}きます|take (something)|-|E02.L17
五段 (って)|{{'立'|ruby:'た'}}ちます|stand up; be useful|ここ<span class="red">に</span>{{'立'|ruby:'た'}}つ<br/>{{'役'|ruby:'やく'}}<span class="red">に</span>{{'立'|ruby:'た'}}つ|E01.L14, E02.L21
五段 (って)|{{'連'|ruby:'つ'}}れて{{'行'|ruby:'い'}}きます|take (someone)|{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}<span class="red">を</span>{{'連'|ruby:'つ'}}れて{{'行'|ruby:'い'}}く|E02.L24
五段 (んで)|{{'遊'|ruby:'あそ'}}びます|enjoy oneself, play|-|E01.L13
五段 (いて)|{{'働'|ruby:'はたら'}}きます|work|-|B00.L04
五段 (いて)|{{'歩'|ruby:'ある'}}きます|walk|{{'道'|ruby:'みち'}}<span class="red">を</span>{{'歩'|ruby:'ある'}}く|E02.L23
五段 (いで)|{{'急'|ruby:'いそ'}}ぎます|hurry|-|E01.L14
カ行|{{'来'|ruby:'き'}}ます|come|-|B00.L05
カ行|{{'持'|ruby:'も'}}って{{'来'|ruby:'き'}}ます|bring (something)|-|E02.L17
カ行|{{'連'|ruby:'つ'}}れて{{'来'|ruby:'き'}}ます|bring (someone)|{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}<span class="red">を</span>{{'連'|ruby:'つ'}}れて{{'来'|ruby:'く'}}る|E02.L24

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-4}

## Transitive {#transitive}

Type | Word | Meaning | Example | Refer
---- | ---: | ------- | ------- | -----
五段 (って)|{{'買'|ruby:'か'}}います|buy|-|B00.L06
五段 (って)|もらいます|receive|-|B00.L07
五段 (って)|{{'作'|ruby:'つく'}}ります|make, produce|-|E01.L15
五段 (って)|{{'造'|ruby:'つく'}}ります|make, produce|-|E01.L15
五段 (んで)|{{'飲'|ruby:'の'}}みます|drink; take medicine|-|B00.L06, E01.L16, E02.L17
五段 (んで)|{{'読'|ruby:'よ'}}みます|read|-|B00.L06
五段 (いて)|{{'書'|ruby:'か'}}きます|write, draw, paint|-|B00.L06
五段 (いて)|{{'拭'|ruby:'ふ'}}きます|wipe|-|I01.L29
五段 (して)|{{'話'|ruby:'はな'}}します|speak, talk|-|E01.L14
五段 (して)|{{'指'|ruby:'さ'}}します|point|-|I01.L29
一段 (下)|{{'食'|ruby:'た'}}べます|eat|-|B00.L06
一段 (下)|{{'上'|ruby:'あ'}}げます|give|-|B00.L07
一段 (下)|{{'忘'|ruby:'わす'}}れます|forget|-|E02.L17
一段 (下)|{{'間'|ruby:'ま'}}{{'違'|ruby:'ちが'}}えます|make a mistake|-|I01.L29
一段 (下)|{{'取'|ruby:'と'}}り{{'替'|ruby:'か'}}えます|change|-|I01.L29

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-4}

## Intransitive & transitive pairs {#intransitive-transitive}

<div class="table-responsive">

Intransitive | Transitive
-- | --
{% verbWord %}{{'開'|ruby:'あ'}}きます{% endverbWord %}<br/>open<br/>五段 (いて), I01.L29<br/>{% verbEg %}ドア<span class="blue">が</span>{{'開'|ruby:'あ'}}きます{% endverbEg %}|{% verbWord %}{{'開'|ruby:'あ'}}けます{% endverbWord %}<br/>open<br/>一段 (下), E01.L14<br/>{% verbEg %}ドア<span class="blue">を</span>{{'開'|ruby:'あ'}}けます{% endverbEg %}
<span class="text-1-25">{{'閉'|ruby:'し'}}まります</span><br/>close, shut<br/>五段 (って), I01.L29<br/>{% verbEg %}ドア<span class="blue">が</span>{{'閉'|ruby:'し'}}まります{% endverbEg %}|<span class="text-1-25">{{'閉'|ruby:'し'}}めます</span><br/>close, shut<br/>一段 (下), E01.L14<br/>{% verbEg %}ドア<span class="blue">を</span>{{'閉'|ruby:'し'}}めます{% endverbEg %}
<span class="text-1-25">つきます</span><br/>come on, be turned on, be lighted, go on<br/>五段 (いて), I01.L29<br/>{% verbEg %}{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="blue">が</span>つきます{% endverbEg %}|<span class="text-1-25">つけます</span><br/>turn on<br/>一段 (下), E01.L14<br/>{% verbEg %}{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="blue">を</span>つけます{% endverbEg %}
<span class="text-1-25">{{'消'|ruby:'き'}}えます</span><br/>go off<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="blue">が</span>{{'消'|ruby:'き'}}えます{% endverbEg %}|<span class="text-1-25">{{'消'|ruby:'け'}}します</span><br/>turn off, erase<br/>五段 (して), E01.L14<br/>{% verbEg %}{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="blue">を</span>{{'消'|ruby:'け'}}します{% endverbEg %}
{% verbWord %}{{'壊'|ruby:'こわ'}}れます{% endverbWord %}<br/>break, get broken<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="blue">が</span>{{'壊'|ruby:'こわ'}}れます{% endverbEg %}|{% verbWord %}{{'壊'|ruby:'こわ'}}します{% endverbWord %}<br/>break<br/>五段 (して), I01.L29<br/>{% verbEg %}{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="blue">を</span>{{'壊'|ruby:'こわ'}}します{% endverbEg %}
{% verbWord %}{{'割'|ruby:'わ'}}れます{% endverbWord %}<br/>break, smash, crack, be broken<br/>一段 (下), I01.L29<br/>{% verbEg %}コップ<span class="blue">が</span>{{'割'|ruby:'わ'}}れます{% endverbEg %}|{% verbWord %}{{'割'|ruby:'わ'}}ります{% endverbWord %}<br/>break, smash<br/>五段 (って), I01.L29<br/>{% verbEg %}コップ<span class="blue">を</span>{{'割'|ruby:'わ'}}ります{% endverbEg %}
{% verbWord %}{{'折'|ruby:'お'}}れます{% endverbWord %}<br/>break, snap, be broken<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'木'|ruby:'き'}}<span class="blue">が</span>{{'折'|ruby:'お'}}れます{% endverbEg %}|{% verbWord %}{{'折'|ruby:'お'}}ります{% endverbWord %}<br/>break, snap<br/>五段 (って), I01.L29<br/>{% verbEg %}{{'木'|ruby:'き'}}<span class="blue">を</span>{{'折'|ruby:'お'}}ります{% endverbEg %}
{% verbWord %}{{'破'|ruby:'やぶ'}}れます{% endverbWord %}<br/>tear, be torn, rip<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'紙'|ruby:'かみ'}}<span class="blue">が</span>{{'破'|ruby:'やぶ'}}れます{% endverbEg %}|{% verbWord %}{{'破'|ruby:'やぶ'}}ります{% endverbWord %}<br/>tear, break (promise)<br/>五段 (って), I01.L29<br/>{% verbEg %}{{'紙'|ruby:'かみ'}}<span class="blue">を</span>{{'破'|ruby:'やぶ'}}ります{% endverbEg %}
{% verbWord %}{{'汚'|ruby:'よご'}}れます{% endverbWord %}<br/>get dirty<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'服'|ruby:'ふく'}}<span class="blue">が</span>{{'汚'|ruby:'よご'}}れます{% endverbEg %}|{% verbWord %}{{'汚'|ruby:'よご'}}します{% endverbWord %}<br/>make dirty<br/>五段 (して), I01.L29<br/>{% verbEg %}{{'服'|ruby:'ふく'}}<span class="blue">を</span>{{'汚'|ruby:'よご'}}します{% endverbEg %}
{% verbWord %}{{'付'|ruby:'つ'}}きます{% endverbWord %}<br/>be attached<br/>五段 (いて), I01.L29<br/>{% verbEg %}ポケット<span class="blue">が</span>{{'付'|ruby:'つ'}}きます{% endverbEg %}|{% verbWord %}{{'付'|ruby:'つ'}}けます{% endverbWord %}<br/>attach, put on<br/>一段 (下), I01.L27<br/>{% verbEg %}ポケット<span class="blue">を</span>{{'付'|ruby:'つ'}}けます{% endverbEg %}
{% verbWord %}{{'外'|ruby:'はず'}}れます{% endverbWord %}<br/>be undone, come off<br/>一段 (下), I01.L29<br/>{% verbEg %}ボタン<span class="blue">が</span>{{'外'|ruby:'はず'}}れます{% endverbEg %}|{% verbWord %}{{'外'|ruby:'はず'}}します{% endverbWord %}<br/>take off, remove, undo<br/>五段 (して), I01.L29<br/>{% verbEg %}ボタン<span class="blue">を</span>{{'外'|ruby:'はず'}}します{% endverbEg %}
{% verbWord %}{{'止'|ruby:'と'}}まります{% endverbWord %}<br/>stop, park<br/>五段 (って), I01.L29<br/>{% verbEg %}{{'車'|ruby:'くるま'}}<span class="blue">が</span>{{'止'|ruby:'と'}}まります{% endverbEg %}|{% verbWord %}{{'止'|ruby:'と'}}めます{% endverbWord %}<br/>stop, park<br/>一段 (下), E01.L14<br/>{% verbEg %}{{'車'|ruby:'くるま'}}<span class="blue">を</span>{{'止'|ruby:'と'}}めます{% endverbEg %}
{% verbWord %}{{'落'|ruby:'お'}}ちます{% endverbWord %}<br/>fall<br/>一段 (上), I01.L29<br/>{% verbEg %}ペン<span class="blue">が</span>{{'落'|ruby:'お'}}ちます{% endverbEg %}|{% verbWord %}{{'落'|ruby:'お'}}とします{% endverbWord %}<br/>drop, lose<br/>五段 (して), I01.L29<br/>{% verbEg %}ペン<span class="blue">を</span>{{'落'|ruby:'お'}}とします{% endverbEg %}
{% verbWord %}{{'掛'|ruby:'か'}}かります{% endverbWord %}<br/>be locked<br/>五段 (って), I01.L29<br/>{% verbEg %}{{'鍵'|ruby:'かぎ'}}<span class="blue">が</span>{{'掛'|ruby:'か'}}かります{% endverbEg %}|{% verbWord %}{{'掛'|ruby:'か'}}けます{% endverbWord %}<br/>lock, secure<br/>一段 (下)<br/>{% verbEg %}{{'鍵'|ruby:'かぎ'}}<span class="blue">を</span>{{'掛'|ruby:'か'}}けます{% endverbEg %}
{% verbWord %}{{'片'|ruby:'かた'}}{{'付'|ruby:'ず'}}きます{% endverbWord %}<br/>be put in order, tidy up<br/>五段 (いて), I01.L26<br/>{% verbEg %}{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}<span class="blue">が</span>{{'片'|ruby:'かた'}}{{'付'|ruby:'ず'}}きます{% endverbEg %}|{% verbWord %}{{'片'|ruby:'かた'}}{{'付'|ruby:'ず'}}けます{% endverbWord %}<br/>put (things) in order, tidy up<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}<span class="blue">を</span>{{'片'|ruby:'かた'}}{{'付'|ruby:'ず'}}けます{% endverbEg %}
{% verbWord %}{{'倒'|ruby:'たお'}}れます{% endverbWord %}<br/>fall down<br/>一段 (下), I01.L29<br/>{% verbEg %}{{'木'|ruby:'き'}}<span class="blue">が</span>{{'倒'|ruby:'たお'}}れます{% endverbEg %}|{% verbWord %}{{'倒'|ruby:'たお'}}します{% endverbWord %}<br/>push down, knock down<br/>五段 (して), I01.L29<br/>{% verbEg %}{{'木'|ruby:'き'}}<span class="blue">を</span>{{'倒'|ruby:'たお'}}します{% endverbEg %}
{% verbWord %}{{'燃'|ruby:'も'}}えます{% endverbWord %}<br/>burn<br/>一段 (下), I01.L29<br/>{% verbEg %}ごみ<span class="blue">が</span>{{'燃'|ruby:'も'}}えます{% endverbEg %}|{% verbWord %}{{'燃'|ruby:'も'}}やします{% endverbWord %}<br/>burn<br/>五段 (して), I01.L29<br/>{% verbEg %}ごみ<span class="blue">を</span>{{'燃'|ruby:'も'}}やします{% endverbEg %}

{.table .table-hover .table-sm .align-text-bottom}

</div>

## Noun based {#noun-based}

Word | Meaning | Example | Refer
---: | ------- | ------- | -----
{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}します|study|-|B00.L04
します|do, play; put on (tie)|テニス<span class="red">を</span>します<br/>ネクタイ<span class="red">を</span>します|B00.L06, E02.L22
{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}します|marry, get married|-|E01.L13
{{'買'|ruby:'か'}}い{{'物'|ruby:'もの'}}します|do shopping|-|E01.L13
{{'食'|ruby:'しょく'}}{{'事'|ruby:'じ'}}します|have a meal, dine|-|E01.L13
{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}します|take a walk|{{'公'|ruby:'こう'}}{{'園'|ruby:'えん'}}<span class="red">を</span>{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}します|E01.L13
コピーします|copy|-|E01.L14
{{'研'|ruby:'けん'}}{{'究'|ruby:'きゅう'}}します|do research|-|E01.L15
{{'見'|ruby:'けん'}}{{'学'|ruby:'がく'}}します|tour, visit a place to study it|-|E01.L16
{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}します|phone|-|E01.L16
{{'心'|ruby:'しん'}}{{'配'|ruby:'ぱい'}}します|worry|-|E02.L17
{{'残'|ruby:'ざん'}}{{'業'|ruby:'ぎょう'}}します|work overtime|-|E02.L17
{{'出'|ruby:'しゅっ'}}{{'張'|ruby:'ちょう'}}します|go on a business trip|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">へ</span>{{'出'|ruby:'しゅっ'}}{{'張'|ruby:'ちょう'}}します|E02.L17
{{'運'|ruby:'うん'}}{{'転'|ruby:'てん'}}します|drive|-|E02.L18
{{'予'|ruby:'よ'}}{{'約'|ruby:'やく'}}します|reserve, book|-|E02.L18
{{'掃'|ruby:'そう'}}{{'除'|ruby:'じ'}}します|clean (a room)|-|E02.L19
{{'洗'|ruby:'せん'}}{{'濯'|ruby:'たく'}}します|wash (clothes)|-|E02.L19
{{'修'|ruby:'しゅう'}}{{'理'|ruby:'り'}}します|repair|-|E02.L20
{{'留'|ruby:'りゅう'}}{{'学'|ruby:'がく'}}します|study abroad|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">に</span>{{'留'|ruby:'りゅう'}}{{'学'|ruby:'がく'}}します|E02.L21
{{'紹'|ruby:'しょう'}}{{'介'|ruby:'かい'}}します|introduce|{{'人'|ruby:'ひと'}}<span class="red">に</span>{{'物'|ruby:'もの'}}<span class="red">を</span>{{'紹'|ruby:'しょう'}}{{'介'|ruby:'かい'}}します|E02.L24
{{'案'|ruby:'あん'}}{{'内'|ruby:'ない'}}します|show around, show the way|{{'人'|ruby:'ひと'}}<span class="red">に</span>{{'物'|ruby:'もの'}}<span class="red">を</span>{{'案'|ruby:'あん'}}{{'内'|ruby:'ない'}}します|E02.L24
{{'説'|ruby:'せつ'}}{{'明'|ruby:'めい'}}します|explain|{{'人'|ruby:'ひと'}}<span class="red">に</span>{{'物'|ruby:'もの'}}<span class="red">を</span>{{'説'|ruby:'せつ'}}{{'明'|ruby:'めい'}}します|E02.L24
{{'連'|ruby:'れん'}}{{'絡'|ruby:'らく'}}します|contact, get in touch with|-|I01.L26
メモします|take a memo|-|I01.L28
{{'参'|ruby:'さん'}}{{'加'|ruby:'か'}}します|participate, join, attend|-|I01.L28

{.table .table-hover .table-sm .align-text-bottom .table-ruby-1 .table-ruby-3}

## Uncategorized {#uncategorized}

<div class="table-responsive">

Type | Formal | Informal | Meaning | Example | Refer
---- | -----: | -------: | ------- | ------- | -----
五段 (って)<br/>intransitive|{{'終'|ruby:'お'}}わります|{{'終'|ruby:'お'}}わる|finish|-|B00.L04
五段 (って)<br/>transitive|{{'吸'|ruby:'す'}}います|{{'吸'|ruby:'す'}}う|smoke|たばこ<span class="red">を</span>{{'吸'|ruby:'す'}}う|B00.L06
五段 (って)<br/>transitive|{{'撮'|ruby:'と'}}ります|{{'撮'|ruby:'と'}}る|take (photograph)|{{'写'|ruby:'しゃ'}}{{'真'|ruby:'しん'}}<span class="red">を</span>{{'撮'|ruby:'と'}}る|B00.L06
五段 (って)<br/>intransitive|{{'会'|ruby:'あ'}}います|{{'会'|ruby:'あ'}}う|meet|{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}<span class="red">に</span>{{'会'|ruby:'あ'}}う|B00.L06
五段 (って)<br/>transitive|{{'切'|ruby:'き'}}ります|{{'切'|ruby:'き'}}る|cut, slice|-|B00.L07
五段 (って)<br/>transitive|{{'送'|ruby:'おく'}}ります|{{'送'|ruby:'おく'}}る|send; escort, go with|{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}<span class="red">を</span>{{'送'|ruby:'おく'}}る<br/>{{'人'|ruby:'ひと'}}<span class="red">を</span>{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<span class="red">へ</span>{{'送'|ruby:'おく'}}る|B00.L07, E02.L24
五段 (って)<br/>transitive|{{'習'|ruby:'なら'}}います|{{'習'|ruby:'なら'}}う|learn|-|B00.L07
五段 (って)<br/>intransitive|わかります|わかる|understand|これ<span class="red">が</span>わかる|E01.L09
五段 (って)<br/>intransitive|かかります|かかる|take, cost (time or money)|-|E01.L11
五段 (って)<br/>transitive|{{'持'|ruby:'も'}}ちます|{{'持'|ruby:'も'}}つ|hold|-|E01.L14
五段 (って)<br/>transitive|{{'取'|ruby:'と'}}ります|{{'取'|ruby:'と'}}る|take, pass; grow old|{{'物'|ruby:'もの'}}<span class="red">を</span>{{'取'|ruby:'と'}}る<br/>{{'年'|ruby:'とし'}}<span class="red">を</span>{{'取'|ruby:'と'}}る|E01.L14、E02.L25
五段 (って)<br/>transitive|{{'手'|ruby:'て'}}{{'伝'|ruby:'つだ'}}います|{{'手'|ruby:'て'}}{{'伝'|ruby:'つだ'}}う|help (with a task)|-|E01.L14
五段 (って)<br/>transitive|{{'使'|ruby:'つか'}}います|{{'使'|ruby:'つか'}}う|use|-|E01.L14
五段 (って)<br/>intransitive|{{'入'|ruby:'はい'}}ります|{{'入'|ruby:'はい'}}る|enter|{{'喫'|ruby:'きっ'}}{{'茶'|ruby:'さ'}}{{'店'|ruby:'てん'}}<span class="red">に</span>{{'入'|ruby:'はい'}}る<br/>{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}<span class="red">に</span>{{'入'|ruby:'はい'}}る<br/>お{{'風'|ruby:'ふ'}}{{'呂'|ruby:'ろ'}}<span class="red">に</span>{{'入'|ruby:'はい'}}る|E01.L14, E01.L16, E02.L17
五段 (って)<br/>intransitive|{{'降'|ruby:'ふ'}}ります|{{'降'|ruby:'ふ'}}る|rain|{{'雨'|ruby:'あめ'}}<span class="red">が</span>{{'降'|ruby:'ふ'}}る|E01.L14
五段 (って)<br/>transitive|{{'売'|ruby:'う'}}ります|{{'売'|ruby:'う'}}る|sell|-|E01.L15
五段 (って)<br/>transitive|{{'知'|ruby:'し'}}ります|{{'知'|ruby:'し'}}る|get to know|-|E01.L15
五段 (って)<br/>intransitive|{{'乗'|ruby:'の'}}ります|{{'乗'|ruby:'の'}}る|ride, get on|{{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}<span class="red">に</span>{{'乗'|ruby:'の'}}る|E01.L16
五段 (って)<br/>transitive|{{'払'|ruby:'はら'}}います|{{'払'|ruby:'はら'}}う|pay|お{{'金'|ruby:'かね'}}<span class="red">を</span>{{'払'|ruby:'はら'}}う|E02.L17
五段 (って)<br/>transitive|{{'洗'|ruby:'あら'}}います|{{'洗'|ruby:'あら'}}う|wash|-|E02.L18
五段 (って)<br/>transitive|{{'歌'|ruby:'うた'}}います|{{'歌'|ruby:'うた'}}う|sing|-|E02.L18
五段 (って)<br/>intransitive|{{'登'|ruby:'のぼ'}}ります|{{'登'|ruby:'のぼ'}}る|climb, go up|{{'山'|ruby:'やま'}}<span class="red">に</span>{{'登'|ruby:'のぼ'}}る|E02.L19
五段 (って)<br/>intransitive|{{'泊'|ruby:'と'}}まります|{{'泊'|ruby:'と'}}まる|stay|ホテル<span class="red">に</span>{{'泊'|ruby:'と'}}まる|E02.L19
五段 (って)<br/>intransitive|なります|なる|become|-|E02.L19
五段 (って)<br/>intransitive|{{'要'|ruby:'い'}}ります|{{'要'|ruby:'い'}}る|need, require|ビザ<span class="red">が</span>{{'要'|ruby:'い'}}る|E02.L20
五段 (って)<br/>transitive|{{'思'|ruby:'おも'}}います|{{'思'|ruby:'おも'}}う|think|-|E02.L21
五段 (って)|{{'言'|ruby:'い'}}います|{{'言'|ruby:'い'}}う|say|-|E02.L21
五段 (って)<br/>intransitive|{{'勝'|ruby:'か'}}ちます|{{'勝'|ruby:'か'}}つ|win|-|E02.L21
五段 (って)<br/>transitive|かぶります|かぶる|put on (a hat, etc.)|-|E02.L22
五段 (って)<br/>intransitive|{{'触'|ruby:'さわ'}}ります|{{'触'|ruby:'さわ'}}る|touch|ドア<span class="red">に</span>{{'触'|ruby:'さわ'}}る|E02.L23
五段 (って)<br/>intransitive|{{'渡'|ruby:'わた'}}ります|{{'渡'|ruby:'わた'}}る|cross|{{'橋'|ruby:'はし'}}<span class="red">を</span>{{'渡'|ruby:'わた'}}る|E02.L23
五段 (って)<br/>intransitive|{{'曲'|ruby:'ま'}}がります|{{'曲'|ruby:'ま'}}がる|turn|{{'右'|ruby:'みぎ'}}<span class="red">へ</span>{{'曲'|ruby:'ま'}}がる|E02.L23
五段 (って)<br/>intransitive|{{'頑'|ruby:'がん'}}{{'張'|ruby:'ば'}}ます|{{'頑'|ruby:'がん'}}{{'張'|ruby:'ば'}}る|do one's best|-|E02.L25
五段 (って)<br/>intransitive|{{'間'|ruby:'ま'}}に{{'合'|ruby:'あ'}}います|{{'間'|ruby:'ま'}}に{{'合'|ruby:'あ'}}う|be in time|{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}<span class="red">に</span>{{'間'|ruby:'ま'}}に{{'合'|ruby:'あ'}}う|I01.L26
五段 (って)<br/>transitive|やります|やる|do|-|I01.L26
五段 (って)<br/>transitive|{{'拾'|ruby:'ひろ'}}います|{{'拾'|ruby:'ひろ'}}う|pick up|-|I01.L26
五段 (って)<br/>intransitive|{{'違'|ruby:'ちが'}}います|{{'違'|ruby:'ちが'}}う|be different|-|I01.L26
五段 (って)<br/>transitive|{{'飼'|ruby:'か'}}います|{{'飼'|ruby:'か'}}う|keep (a pet), raise (an animal)|{{'犬'|ruby:'いぬ'}}<span class="red">を</span>{{'飼'|ruby:'か'}}う|I01.L27
五段 (って)<br/>intransitive|{{'走'|ruby:'はし'}}ります|{{'走'|ruby:'はし'}}る|run, drive|{{'道'|ruby:'みち'}}<span class="red">を</span>{{'走'|ruby:'はし'}}る|I01.L27
五段 (って)<br/>intransitive|{{'踊'|ruby:'おど'}}ります|{{'踊'|ruby:'おど'}}る|dance|-|I01.L28
五段 (って)<br/>intransitive|{{'通'|ruby:'かよ'}}います|{{'通'|ruby:'かよ'}}う|go to and from|{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}<span class="red">に</span>{{'通'|ruby:'かよ'}}う|I01.L28
五段 (って)<br/>transitive|{{'誘'|ruby:'さそ'}}います|{{'誘'|ruby:'さそ'}}う|invite, ask someone to join|-|I01.L28
五段 (んで)<br/>intransitive|{{'休'|ruby:'やす'}}みます|{{'休'|ruby:'やす'}}む|take a rest, holiday|-|B00.L04
五段 (んで)<br/>intransitive|{{'休'|ruby:'やす'}}みます|{{'休'|ruby:'やす'}}む|take a day off|{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}<span class="red">を</span>{{'休'|ruby:'やす'}}む|E01.L11
五段 (んで)<br/>transitive|{{'呼'|ruby:'よ'}}びます|{{'呼'|ruby:'よ'}}ぶ|call|タクシー<span class="red">を</span>{{'呼'|ruby:'よ'}}ぶ|E01.L14
五段 (んで)<br/>intransitive|{{'住'|ruby:'す'}}みます|{{'住'|ruby:'す'}}む|be going to live|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">に</span>{{'住'|ruby:'す'}}む|E01.L15
五段 (んで)<br/>intransitive|{{'飛'|ruby:'と'}}びます|{{'飛'|ruby:'と'}}ぶ|fly|-|I01.L27
五段 (んで)<br/>transitive|かみます|かむ|chew, bite|-|I01.L28
五段 (んで)<br/>transitive|{{'選'|ruby:'えら'}}びます|{{'選'|ruby:'えら'}}ぶ|choose|-|I01.L28
五段 (いて)<br/>transitive|{{'聞'|ruby:'き'}}きます|{{'聞'|ruby:'き'}}く|hear, listen|-|B00.L06
五段 (いて)<br/>intransitive|{{'渇'|ruby:'かわ'}}きます|{{'渇'|ruby:'かわ'}}く|get thirsty|{{'喉'|ruby:'のど'}}<span class="red">が</span>{{'渇'|ruby:'かわ'}}く|E01.L13
五段 (いて)<br/>intransitive|{{'空'|ruby:'す'}}きます|{{'空'|ruby:'す'}}く|get hungry|お{{'腹'|ruby:'なか'}}<span class="red">が</span>{{'空'|ruby:'す'}}く|E01.L13
五段 (いて)<br/>transitive|{{'置'|ruby:'お'}}きます|{{'置'|ruby:'お'}}く|put|-|E01.L15
五段 (いて)<br/>transitive|{{'弾'|ruby:'ひ'}}きます|{{'弾'|ruby:'ひ'}}く|play (stringed instrument, piano, etc.)|-|E02.L18
五段 (いて)<br/>intransitive|{{'動'|ruby:'うご'}}きます|{{'動'|ruby:'うご'}}く|move, work|-|E02.L21
五段 (いて)<br/>transitive|{{'履'|ruby:'は'}}きます|{{'履'|ruby:'は'}}く|put on (shoes, trousers, etc.)|-|E02.L22
五段 (いて)<br/>transitive|{{'聞'|ruby:'き'}}きます|{{'聞'|ruby:'き'}}く|ask|{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}<span class="red">に</span>{{'聞'|ruby:'き'}}く|E02.L23
五段 (いて)<br/>transitive|{{'引'|ruby:'ひ'}}きます|{{'引'|ruby:'ひ'}}く|pull|-|E02.L23
五段 (いて)<br/>intransitive|{{'着'|ruby:'つ'}}きます|{{'着'|ruby:'つ'}}く|arrive|{{'駅'|ruby:'えき'}}<span class="red">に</span>{{'着'|ruby:'つ'}}く|E02.L25
五段 (いて)<br/>transitive|{{'開'|ruby:'ひら'}}きます|{{'開'|ruby:'ひら'}}く|set up, open, hold|{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}<span class="red">を</span>{{'開'|ruby:'ひら'}}く|I01.L27
五段 (いで)<br/>intransitive|{{'泳'|ruby:'およ'}}ぎます|{{'泳'|ruby:'およ'}}ぐ|swim|-|E01.L13
五段 (いで)<br/>transitive|{{'脱'|ruby:'ぬ'}}ぎます|{{'脱'|ruby:'ぬ'}}ぐ|take off (clothes, shoes, etc.)|{{'靴'|ruby:'くつ'}}<span class="red">を</span>{{'脱'|ruby:'ぬ'}}ぐ|E02.L17
五段 (して)<br/>transitive|{{'貸'|ruby:'か'}}します|{{'貸'|ruby:'か'}}す|lend|-|B00.L07
五段 (して)<br/>transitive|{{'出'|ruby:'だ'}}します|{{'出'|ruby:'だ'}}す|take out, hand in, send; put out|ここ<span class="red">から</span>{{'出'|ruby:'だ'}}す<br/>ごみ<span class="red">を</span>{{'出'|ruby:'だ'}}す|E01.L16, I01.L26
五段 (して)<br/>transitive|{{'下'|ruby:'お'}}ろします|{{'下'|ruby:'お'}}ろす|withdraw|お{{'金'|ruby:'かね'}}<span class="red">を</span>{{'下'|ruby:'お'}}ろす|E01.L16
五段 (して)<br/>transitive|{{'押'|ruby:'お'}}します|{{'押'|ruby:'お'}}す|push, press|-|E01.L16
五段 (して)<br/>transitive|なくします|なくす|lose|-|E02.L17
五段 (して)<br/>transitive|{{'返'|ruby:'かえ'}}します|{{'返'|ruby:'かえ'}}す|give back, return|{{'女'|ruby:'おんな'}}の{{'人'|ruby:'ひと'}}<span class="red">に</span>{{'傘'|ruby:'かさ'}}<span class="red">を</span>{{'返'|ruby:'かえ'}}す|E02.L17
五段 (して)<br/>transitive|{{'回'|ruby:'まわ'}}します|{{'回'|ruby:'まわ'}}す|turn|-|E02.L23
五段 (して)<br/>transitive|{{'直'|ruby:'なお'}}します|{{'直'|ruby:'なお'}}す|repair, correct|{{'宿'|ruby:'しゅく'}}{{'題'|ruby:'だい'}}の{{'答'|ruby:'こた'}}え<span class="red">を</span>{{'直'|ruby:'なお'}}す|E02.L24
五段 (して)<br/>transitive|{{'探'|ruby:'さが'}}します|{{'探'|ruby:'さが'}}す|look for, search|-|I01.L26
一段 (上)<br/>intransitive|{{'起'|ruby:'お'}}きます|{{'起'|ruby:'お'}}きる|get up, wake up|-|B00.L04
一段 (上)<br/>transitive|{{'見'|ruby:'み'}}ます|{{'見'|ruby:'み'}}る|see, look at, watch; check, take a look at|-|B00.L06, I01.L26
一段 (上)<br/>transitive|{{'借'|ruby:'か'}}ります|{{'借'|ruby:'か'}}りる|borrow|-|B00.L07
一段 (上)<br/>intransitive|います|いる|exist, be (animate things)|-|E01.L10
一段 (上)<br/>intransitive|います|いる|have|{{'子'|ruby:'こ'}}{{'供'|ruby:'ども'}}<span class="red">が</span>いる|E01.L11
一段 (上)<br/>intransitive|います|いる|stay, be (at location)|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">に</span>いる|E01.L11
一段 (上)<br/>intransitive|{{'降'|ruby:'お'}}ります|{{'降'|ruby:'お'}}りる|get off|{{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}<span class="red">を</span>{{'降'|ruby:'お'}}りる|E01.L16
一段 (上)<br/>transitive|{{'浴'|ruby:'あ'}}びます|{{'浴'|ruby:'あ'}}びる|take (a shower)|シャワー<span class="red">を</span>{{'浴'|ruby:'あ'}}びる|E01.L16
一段 (上)<br/>intransitive|できます|できる|be able to, can; be made, be completed, come into existence|ピアノ<span class="red">が</span>できる<br/>{{'道'|ruby:'みち'}}<span class="red">が</span>できる|E02.L18, I01.L27
一段 (上)<br/>intransitive|{{'足'|ruby:'た'}}ります|{{'足'|ruby:'た'}}りる|be enough, be sufficient|-|E02.L25
一段 (上)<br/>transitive|{{'着'|ruby:'き'}}ます|{{'着'|ruby:'き'}}る|put on (a shirt, etc.)|-|E02.L22
一段 (下)<br/>intransitive|{{'寝'|ruby:'ね'}}ます|{{'寝'|ruby:'ね'}}る|sleep, go to bed|-|B00.L04
一段 (下)<br/>transitive|{{'教'|ruby:'おし'}}えます|{{'教'|ruby:'おし'}}える|teach; tell|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}<span class="red">を</span>{{'教'|ruby:'おし'}}える<br/>{{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}<span class="red">を</span>{{'教'|ruby:'おし'}}える|B00.L07, E01.L14
一段 (下)<br/>transitive|かけます|かける|make (a telephone call)|-|B00.L07
一段 (下)<br/>transitive|{{'迎'|ruby:'むか'}}えます|{{'迎'|ruby:'むか'}}える|go to meet, welcome|-|E01.L13
一段 (下)<br/>intransitive|{{'疲'|ruby:'つか'}}れます|{{'疲'|ruby:'つか'}}れる|get tired|-|E01.L13
一段 (下)<br/>transitive|{{'見'|ruby:'み'}}せます|{{'見'|ruby:'み'}}せる|show|-|E01.L14
一段 (下)<br/>transitive|{{'乗'|ruby:'の'}}り{{'換'|ruby:'か'}}えます|{{'乗'|ruby:'の'}}り{{'換'|ruby:'か'}}える|change (between same type of train, etc.)|-|E01.L16
一段 (下)<br/>transitive|{{'入'|ruby:'い'}}れます|{{'入'|ruby:'い'}}れる|put in, insert|ポスト<span class="red">に</span>{{'入'|ruby:'い'}}れる|E01.16
一段 (下)<br/>intransitive|{{'出'|ruby:'で'}}ます|{{'出'|ruby:'で'}}る|go out; graduate from; come out|{{'喫'|ruby:'きっ'}}{{'茶'|ruby:'さ'}}{{'店'|ruby:'てん'}}<span class="red">を</span>{{'出'|ruby:'で'}}る<br/>{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}<span class="red">を</span>{{'出'|ruby:'で'}}る<br/>お{{'釣'|ruby:'つ'}}り<span class="red">が</span>{{'出'|ruby:'で'}}る|E01.L14, E01.L16, E02.L23
一段 (下)<br/>transitive|{{'始'|ruby:'はじ'}}めます|{{'始'|ruby:'はじ'}}める|start, begin|-|E01.L16
一段 (下)<br/>transitive|{{'覚'|ruby:'おぼ'}}えます|{{'覚'|ruby:'おぼ'}}える|memorise|-|E02.L17
一段 (下)<br/>intransitive|{{'出'|ruby:'で'}}かけます|{{'出'|ruby:'で'}}かける|go out|-|E02.L17
一段 (下)<br/>transitive|{{'集'|ruby:'あつ'}}めます|{{'集'|ruby:'あつ'}}める|collect, gather|-|E02.L18
一段 (下)<br/>transitive|{{'捨'|ruby:'す'}}てます|{{'捨'|ruby:'す'}}てる|throw away|-|E02.L18
一段 (下)<br/>transitive|{{'換'|ruby:'か'}}えます|{{'換'|ruby:'か'}}える|exchange, change|-|E02.L18
一段 (下)<br/>transitive|{{'調'|ruby:'しら'}}べます|{{'調'|ruby:'しら'}}べる|check, investigate|-|E02.L20
一段 (下)<br/>intransitive|{{'負'|ruby:'ま'}}けます|{{'負'|ruby:'ま'}}ける|lose, be beaten|-|E02.L21
一段 (下)<br/>transitive|やめます|やめる|quit/retire from, stop, give up|{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}<span class="red">を</span>やめる|E02.L21
一段 (下)<br/>transitive|{{'気'|ruby:'き'}}をつけます|{{'気'|ruby:'き'}}をつける|pay attention, take care|{{'車'|ruby:'くるま'}}<span class="red">に</span>{{'気'|ruby:'き'}}<span class="red">を</span>つける|E02.L21
一段 (下)<br/>transitive|{{'着'|ruby:'つ'}}けます|{{'着'|ruby:'つ'}}ける|put on (watch)|-|E02.L22
一段 (下)<br/>transitive|かけます|かける|put on (glasses)|{{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}<span class="red">を</span>かける|E02.L22
一段 (下)<br/>intransitive|{{'生'|ruby:'う'}}まれます|{{'生'|ruby:'う'}}まれる|be born|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">に</span>{{'赤'|ruby:'あか'}}ちゃん<span class="red">が</span>{{'生'|ruby:'う'}}まれる|E02.L22
一段 (下)<br/>transitive|{{'変'|ruby:'か'}}えます|{{'変'|ruby:'か'}}える|change|-|E02.L23
一段 (下)<br/>transitive|くれます|くれる|give (me)|-|E02.L24
一段 (下)<br/>transitive|{{'考'|ruby:'かんが'}}えます|{{'考'|ruby:'かんが'}}える|think, consider|-|E02.L25
一段 (下)<br/>intransitive|{{'遅'|ruby:'おく'}}れます|{{'遅'|ruby:'おく'}}れる|be late|{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}<span class="red">に</span>{{'遅'|ruby:'おく'}}れる<br/>{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}<span class="red">に</span>{{'遅'|ruby:'おく'}}れる<br/>バス<span class="red">が</span>{{'遅'|ruby:'おく'}}れる|I01.L26
一段 (下)<br/>intransitive|{{'見'|ruby:'み'}}えます|{{'見'|ruby:'み'}}える|can be seen|{{'山'|ruby:'やま'}}<span class="red">が</span>{{'見'|ruby:'み'}}える|I01.L27
一段 (下)<br/>intransitive|{{'聞'|ruby:'き'}}こえます|{{'聞'|ruby:'き'}}こえる|can be heard|{{'音'|ruby:'おと'}}<span class="red">が</span>{{'聞'|ruby:'き'}}こえる|I01.L27
一段 (下)<br/>transitive|{{'建'|ruby:'た'}}てます|{{'建'|ruby:'た'}}てる|build|-|I01.L27
一段 (下)<br/>intransitive|{{'売'|ruby:'う'}}れます|{{'売'|ruby:'う'}}れる|sell, be sold|パン<span class="red">が</span>{{'売'|ruby:'う'}}れる|I01.L28

{.table .table-hover .table-sm .align-text-bottom .table-ruby-2 .table-ruby-3 .table-ruby-5}

</div>
