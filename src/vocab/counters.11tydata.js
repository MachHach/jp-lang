module.exports = {
    tableOfContent: [
        {id: 'building-floors', name: 'Building floors'},
        {id: 'things', name: 'Things'},
        {id: 'people', name: 'People'},
        {id: 'frequency', name: 'Frequency'},
    ],
}
