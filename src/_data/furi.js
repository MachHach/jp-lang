const Kuroshiro = require("kuroshiro").default;
const KuromojiAnalyzer = require("kuroshiro-analyzer-kuromoji");

const kuroshiro = new Kuroshiro();
const jpOptsFuriHira = {mode: "furigana", to: "hiragana"};

const convertKanji = async (attrname, value, replaceables = []) => {
    let result = await kuroshiro.convert(value, jpOptsFuriHira);
    for (const [replaceFrom, replaceTo] of replaceables) {
        result = result.replace(replaceFrom, replaceTo);
    }
    return [attrname, result];
};

const getData = async () => {
    await kuroshiro.init(new KuromojiAnalyzer());

    let result = {};
    const inputData = [
        ['irrealisForm', '未然形', [['がた', 'けい']]],
        ['continuativeForm', '連用形', [['がた', 'けい']]],
        ['terminalForm', '終止形', [['がた', 'けい']]],
        ['attributiveForm', '連体形', [['がた', 'けい']]],
        ['hypotheticalForm', '仮定形', [['がた', 'けい']]],
        ['imperativeForm', '命令形', [['がた', 'けい']]],
        ['dictionaryForm', '辞書形', [['がた', 'けい']]],
        ['formalForm', '丁寧形', [['がた', 'けい']]],
        ['informalForm', '普通形', [['がた', 'けい']]],
        ['perfectiveForm', '完了形', [['がた', 'けい']]],
        ['teForm', 'て形', [['かたち', 'けい']]],
        ['conditionalForm', '已然形', [['がた', 'けい']]],
        ['volitionalForm', '意志形', [['がた', 'けい']]],
        ['passiveForm', '受身形', [['がた', 'けい']]],
        ['causativeForm', '使役形', [['がた', 'けい']]],
        ['potentialForm', '可能形', [['がた', 'けい']]],
    ];
    const assignments = await Promise.all(inputData.map((i) => {
        const [attrname, kanji, replaceables] = i;
        return convertKanji(attrname, kanji, replaceables);
    }));
    for (const [attrname, value] of assignments) {
        result[attrname] = value;
    }
    return result;
};

module.exports = getData;
