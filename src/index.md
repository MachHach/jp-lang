---
layout: simple.html
title: Home
---

<div class="home-container d-flex flex-column justify-content-center align-items-center">

{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}} {.home-title}

Welcome! This is a platform to learn the Japanese standard language
([標準語](https://ja.wikipedia.org/wiki/%E6%A8%99%E6%BA%96%E8%AA%9E)). {.lead}

To use this platform, it is required to be able to read [Hiragana](https://en.wikipedia.org/wiki/Hiragana)
first, so that content can be presented concisely.
<br/>Knowledge of [Katakana](https://en.wikipedia.org/wiki/Katakana) may also be needed, but not strictly required to proceed. {.text-center .small .gray}

</div>
