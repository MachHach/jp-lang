---
layout: reader.html
title: 'Grammar: Adjectives'
navbar:
  grammar: 'active'
---

# Adjectives {{'形'|ruby:'けい'}}{{'容'|ruby:'よう'}}{{'詞'|ruby:'し'}}

## Categories {#categories}

There are mainly 2 categories of adjectives.
Each category has its own rules for conjugation.

### i-adjective {#i-adjective}

I-adjective (「い」形容詞), formally known as *adjectival verb* ({{'形'|ruby:'けい'}}{{'容'|ruby:'よう'}}{{'詞'|ruby:'し'}}, lit. adjective).

- A type of verb whose terminal form ends with 〜い. For example:

<blockquote class="blockquote-qna">
Terminal form / attributive form:
<div class="text-1-25">
<span class="red">{{'安'|ruby:'やす'}}い</span>
</div>
(cheap)
</blockquote>

<blockquote class="blockquote-qna">
Terminal form sentence (if formal: append です):
<div class="text-1-25">
{{'鞄'|ruby:'かばん'}}は<span class="red">{{'安'|ruby:'やす'}}い</span>［です］。
</div>
(Bag is cheap.)
</blockquote>

<blockquote class="blockquote-qna">
Attributive form sentence:
<div class="text-1-25">
<span class="red">{{'安'|ruby:'やす'}}い</span>{{'鞄'|ruby:'かばん'}}［です］。
</div>
(Cheap bag.)
</blockquote>

- The ending 〜い may change when conjugating to other forms. For example:

<blockquote class="blockquote-qna">
Present negative (based on continuative form), i.e. （い➝く）＋ない
<div class="text-1-25">
<span class="red">{{'安'|ruby:'やす'}}くない</span>［です］
</div>
(is not cheap)
</blockquote>

<blockquote class="blockquote-qna">
Past positive (based on continuative form), i.e. （い➝く）-く＋かった
<div class="text-1-25">
<span class="red">{{'安'|ruby:'やす'}}かった</span>［です］
</div>
(was cheap)
</blockquote>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Exception:

The adjective "いい" is treated as "よい" when it is conjugated to other forms. For example:
<blockquote class="blockquote-qna">
<div class="text-1-25">
<span class="red">いい</span>
</div>
(good)
<div class="text-1-25">
<span class="red">よくない</span>
</div>
(not good)
</blockquote>
</div>

### na-adjective {#na-adjective}

Na-adjective (「な」形容詞), formally known as *adjectival noun* ({{'形'|ruby:'けい'}}{{'容'|ruby:'よう'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}, lit. adjective verb).

- A type of noun that can become a verb by attaching to a form of the copula (e.g. だ/です). For example:

<blockquote class="blockquote-qna">
Root:
<div class="text-1-25">
<span class="red">{{'静'|ruby:'しず'}}か</span>
</div>
(quiet)
</blockquote>

<blockquote class="blockquote-qna">
Terminal form, e.g. ＋だ (informal) / ＋です (formal):
<div class="text-1-25">
<span class="red">{{'静'|ruby:'しず'}}かだ</span>
</div>
(quiet)
<div class="text-1-25">
<span class="red">{{'静'|ruby:'しず'}}かです</span>
</div>
(quiet)
</blockquote>

<blockquote class="blockquote-qna">
Terminal form sentence:
<div class="text-1-25">
{{'町'|ruby:'まち'}}は<span class="red">{{'静'|ruby:'しず'}}かです</span>。
</div>
(Town is quiet.)
</blockquote>

- In the adjective's attributive form, it attaches to 〜な instead. For example:

<blockquote class="blockquote-qna">
Attributive form, i.e. ＋な
<div class="text-1-25">
<span class="red">{{'静'|ruby:'しず'}}かな</span>
</div>
(quiet)
</blockquote>

<blockquote class="blockquote-qna">
Attributive form sentence:
<div class="text-1-25">
<span class="red">{{'静'|ruby:'しず'}}かな</span>{{'町'|ruby:'まち'}}［です］。
</div>
(Quiet town.)
</blockquote>

## Forms {#forms}

### Stems {#stems}

There are 6 stem forms for adjectives, similarly for verbs.
To form an adjective's stem, merge its root with a suffix corresponding to the form needed.

Refer to the following table (adapted from [here](https://en.wikipedia.org/wiki/Japanese_grammar#Adjectival_verbs_and_nouns)):

Category/form | い | な
-- | -- | --
Root|安・(yasu.)|静か-(shizuka-)
{{furi.irrealisForm}}<br/>(Irrealis)|安かろ(yasu.karo)|静かだろ(shizuka-daro)
{{furi.continuativeForm}}<br/>(Continuative)|安く(yasu.ku)|静かで(shizuka-de)
{{furi.terminalForm}}<br/>(Terminal)|安い(yasu.i)|静かだ(shizuka-da)
{{furi.attributiveForm}}<br/>(Attributive)|安い(yasu.i)|静かな(shizuka-na)/静かなる(shizuka-naru)
{{furi.hypotheticalForm}}<br/>(Hypothetical)|安けれ(yasu.kere)|静かなら(shizuka-nara)
{{furi.imperativeForm}}<br/>(Imperative)|安かれ(yasu.kare)|静かなれ(shizuka-nare)

{.table .table-hover .table-sm}

### Conjugations {#conjugations}

To fully conjugate an adjective, merge its stem with a suffix corresponding to the form needed.

Usually, an adjective has both formal ({{furi.formalForm}}) and informal ({{furi.informalForm}}) variants.

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Unlike verbs, adjectives do not have passive, causative, or potential forms.
</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Appearing in some of the conjugations, じゃ is equivalent to では.
</div>

#### Imperfective {#imperfective}

Present tense.

「い」 | Formal | Informal
-- | -- | --
Positive|Terminal (終止形) + *です*<br/>安い<span class="red">です</span>|Terminal (終止形)<br/>安い
Negative|Continuative (連用形) + *ない* + *です*<br/>安く<span class="red">ないです</span>|Continuative (連用形) + *ない*<br/>安く<span class="red">ない</span>

{.table .table-hover .table-sm}

「な」 | Formal | Informal
-- | -- | --
Positive|Root + *です*<br/>静か<span class="red">です</span>|Root + *だ*<br/>静か<span class="red">だ</span>|
Negative|Continuative (連用形) - *で* + *じゃありません*<br/>静か<span class="red">じゃありません</span>|Continuative (連用形) - *で* + *じゃない*<br/>静か<span class="red">じゃない</span>

{.table .table-hover .table-sm}

#### Perfective {#perfective}

Past/prefect tense.

「い」 | Formal | Informal
-- | -- | --
Positive|Continuative (連用形) - *く* + *かった* + *です*<br/>安<span class="red">かったです</span>|Continuative (連用形) - *く* + *かった*<br/>安<span class="red">かった</span>
Negative|Continuative (連用形) + *なかった* + *です*<br/>安く<span class="red">なかったです</span>|Continuative (連用形) + *なかった*<br/>安く<span class="red">なかった</span>

{.table .table-hover .table-sm}

「な」 | Formal | Informal
-- | -- | --
Positive|Root + *でした*<br/>静か<span class="red">でした</span>|Continuative (連用形) - *で* + *だった*<br/>静か<span class="red">だった</span>
Negative|Continuative (連用形) - *で* + *じゃありません* + *でした*<br/>静か<span class="red">じゃありませんでした</span>|Continuative (連用形) - *で* + *じゃなかった*<br/>静か<span class="red">じゃなかった</span>

{.table .table-hover .table-sm}

#### Gerundive {#gerundive}

More commonly known as te-form ({{furi.teForm}}).

Type | Rule
-- | --
「い」|Continuative (連用形) + *て*<br/>安く<span class="red">て</span>
「な」|Continuative (連用形)<br/>静かで

{.table .table-hover .table-sm}

#### Conditional {#conditional}

Conditional form ({{furi.conditionalForm}}), to describe "if/when" uses.

Type | 「い」 | 「な」
-- | -- | --
Provisional|Hypothetical (仮定形) + *ば*<br/>安けれ<span class="red">ば</span>|Hypothetical (仮定形) (+ *ば*)<br/>静かなら<span class="red">(ば)</span>
Past|Continuative (連用形) - *く* + *かった* + *ら*<br/>安<span class="red">かったら</span>|Continuative (連用形) - *で* + *だった* + *ら*<br/>静か<span class="red">だったら</span>

{.table .table-hover .table-sm}

#### Volitional {#volitional}

Volitional form ({{furi.volitionalForm}}), to express will/intention/invitation/guess.

Type | Rule
-- | --
「い」|Irrealis (未然形) + *う*<br/>安かろ<span class="red">う</span>
「な」|Irrealis (未然形) + *う*<br/>静かだろ<span class="red">う</span>

{.table .table-hover .table-sm}

### Adverbial {#adverbial}

Type | Rule
-- | --
「い」|Continuative (連用形)<br/>安く
「な」|Root + *に*<br/>静か<span class="red">に</span>

{.table .table-hover .table-sm}

### Degree-ness {#degree-ness}

Type | Rule
-- | --
「い」|Root + *さ*<br/>安<span class="red">さ</span>
「な」|Root + *さ*<br/>静か<span class="red">さ</span>

{.table .table-hover .table-sm}
