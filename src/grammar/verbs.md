---
layout: reader.html
title: 'Grammar: Verbs'
navbar:
  grammar: 'active'
---

# Verbs {{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}

## Groups {#groups}

{{5|jlpt}} {{'E01.L14'|icls}}

Verbs are categorized into 3 groups.
Each group has its own rules for conjugation.

### Godan {#godan}

Godan verbs ({{'五'|ruby:'ご'}}{{'段'|ruby:'だん'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}), sometimes known as "u" verbs / Group 1 verbs.

- When forming a stem, it uses different kana as suffix depending on its root corresponding to which one of the 5 rows of the gojūon table.<br/>For example, to determine the terminal form of Godan verb root {{'書'|ruby:'か'}} (kak.), append the kana corresponding to the *k-* column (requirement for this specific verb) and *-u* row (requirement for terminal form), which in this case the result is {{'書'|ruby:'か'}}<span class="red">く</span>
- Conjugating ~て/た/たら forms requires euphonic changes ({{'音'|ruby:'おん'}}{{'便'|ruby:'びん'}})
to the continuative stem ({{furi.continuativeForm}}).
For example, use the following rules to conjugate ~て form:

連用形 ends with | Change to | Example
-- | -- | --
い、ち、り | って | {{'買'|ruby:'か'}}<span class="red">い</span>ます -> {{'買'|ruby:'か'}}<span class="green">って</span><br/>{{'待'|ruby:'ま'}}<span class="red">ち</span>ます -> {{'待'|ruby:'ま'}}<span class="green">って</span><br/>{{'帰'|ruby:'かえ'}}<span class="red">り</span>ます -> {{'帰'|ruby:'かえ'}}<span class="green">って</span>
み、び | んで | {{'飲'|ruby:'の'}}<span class="red">み</span>ます -> {{'飲'|ruby:'の'}}<span class="green">んで</span><br/>{{'遊'|ruby:'あそ'}}<span class="red">び</span>ます -> {{'遊'|ruby:'あそ'}}<span class="green">んで</span>
き | いて | {{'書'|ruby:'か'}}<span class="red">き</span>ます -> {{'書'|ruby:'か'}}<span class="green">いて</span><br/>{{'行'|ruby:'い'}}<span class="red">き</span>ます -> {{'行'|ruby:'い'}}<span class="orange">って</span> (exception)
ぎ | いで | {{'泳'|ruby:'およ'}}<span class="red">ぎ</span>ます -> {{'泳'|ruby:'およ'}}<span class="green">いで</span>
し | して | {{'話'|ruby:'はな'}}<span class="red">し</span>ます -> {{'話'|ruby:'はな'}}<span class="green">して</span>

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The table above uses continuative stem as example so that it is more relatable to beginners, who usually learn the polite (continuative + <i>-masu</i>) form first.
</div>

### Ichidan {#ichidan}

Ichidan verbs ({{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}), sometimes known as "ru" verbs / Group 2 verbs.

- When forming a stem, it uses the same kana as suffix.<br/>For example, to determine the terminal form of Ichidan verb root {{'食'|ruby:'た'}}べ, always append the kana る, which in this case the result is {{'食'|ruby:'た'}}べ<span class="red">る</span>
- There are 2 types of Ichidan verbs:
  - *Kami-Ichidan* ({{'上'|ruby:'かみ'}}{{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}, lit. upper-1-row): Verbs ending with *-i* before the る of terminal form (e.g. {{'起'|ruby:'お'}}<span class="red">き</span>る)
  - *Shimo-Ichidan* ({{'下'|ruby:'しも'}}{{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}, lit. lower-1-row): Verbs ending with *-e* before the る of terminal form (e.g. {{'食'|ruby:'た'}}<span class="red">べ</span>る)

### Irregular {#irregular}

Sometimes known as Group 3 verbs.

- When forming a stem, it uses its own rules very specific to combination of verb and stem needed.
- There are 2 types of irregular verbs:
  - *Ka-gyō* (カ{{'行'|ruby:'ぎょう'}}{{'変'|ruby:'へん'}}{{'格'|ruby:'かく'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}, lit. ka-column): Verbs derived from the verb {{'来'|ruby:'く'}}る
  - *Sa-gyō* (サ{{'行'|ruby:'ぎょう'}}{{'変'|ruby:'へん'}}{{'格'|ruby:'かく'}}{{'活'|ruby:'かつ'}}{{'用'|ruby:'よう'}}, lit. sa-column): Verbs derived from the verb する

## Forms {#forms}

### Stems {#stems}

There are 6 stem forms for verbs. Each of these forms the base to conjugate other more complex forms.

To form a verb's stem, merge its root with a suffix corresponding to the form needed.

Refer to the following table (adapted from [here](https://en.wikipedia.org/wiki/Japanese_grammar#Verbs)):

<div class="table-responsive">

Group/form | 五段 | 五段 | 上一段 | 下一段 | カ行 | サ行
-- | -- | -- | -- | -- | -- | --
Root|使・(tsuka(w).)|書・(kak.)|見・(mi.)|食べ・(tabe.)|-|-
{{furi.irrealisForm}}<br/>(Irrealis)|使わ(tsukaw.a)/使お(tsuka.o)|書か(kak.a)/書こ(kak.o)|見(mi.)|食べ(tabe.)|さ(sa)/し(shi)/せ(se)|来(ko)
{{furi.continuativeForm}}<br/>(Continuative)|使い(tsuka.i)|書き(kak.i)|見(mi.)|食べ(tabe.)|し(shi)|来(ki)
{{furi.terminalForm}}<br/>(Terminal)|使う(tsuka.u)|書く(kak.u)|見る(mi.ru)|食べる(tabe.ru)|する(suru)|来る(kuru)
{{furi.hypotheticalForm}}<br/>(Hypothetical)|使え(tsuka.e)|書け(kak.e)|見れ(mi.re)|食べれ(tabe.re)|すれ(sure)|来れ (kure)
{{furi.imperativeForm}}<br/>(Imperative)|使え(tsuka.e)|書け(kak.e)|見ろ(mi.ro)/見よ(mi.yo)|食べろ(tabe.ro)/食べよ(tabe.yo)|しろ(shiro)/せよ(seyo)|せい(sei)/来い(koi)

{.table .table-hover .table-sm}

</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Attributive form ({{furi.attributiveForm}}) is one of the 6 stem forms.
It is not shown above, as for a verb its attributive form is the <i>same as its terminal form</i>.
</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb's terminal form is infinitive, hence it may also be known as <i>dictionary form</i> ({{furi.dictionaryForm}}).
</div>

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
An honorific verb's continuative form uses a different formation rule (<span class="red">remove る</span>, <span class="green">add い</span>). These verbs include:
<ul>
<li>くださ<span class="red">る</span> (to give) -> くださ<span class="green">い</span></li>
<li>ござ<span class="red">る</span> (to be) -> ござ<span class="green">い</span></li>
<li>いらっしゃ<span class="red">る</span> (to come/go) -> いらっしゃ<span class="green">い</span></li>
<li>なさ<span class="red">る</span> (to do) -> なさ<span class="green">い</span></li>
</ul>
</div>

### Conjugations {#conjugations}

To fully conjugate a verb, merge its specific stem with a suffix corresponding to the form needed.

Usually, a verb has both formal ({{furi.formalForm}}) and informal ({{furi.informalForm}}) variants,
which both may not share the same conjugation rules.

#### Imperfective {#imperfective}

Present tense.

Type | Formal | Informal
-- | -- | --
Positive|Continuative (連用形) <span class="green">+ *ます*</span><br/><small class="gray">(masu-form (ます形))</small>|Terminal (終止形)<br/>
Negative|Continuative (連用形) <span class="green">+ *ません*</span>|Irrealis (未然形) <span class="green">+ *ない*</span><br/><small class="gray">(nai-form (ない形))</small>

{.table .table-hover .table-sm}

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
Exception for informal negative imperfective form:
<ul>
<li>ある (to exist) -> ない (not exist)</li>
</ul>
</div>

#### Perfective {#perfective}

Past/perfect tense ({{furi.perfectiveForm}}).

Type | Formal | Informal
-- | -- | --
Positive|Continuative (連用形) <span class="green">+ *ました*</span>|Continuative-euphonic (連用形+音便) <span class="green">+ *た/だ*</span><br/><small class="gray">(ta-form (た形))</small>
Negative|Continuative (連用形) <span class="green">+ *ませんでした*</span>|Irrealis (未然形) <span class="green">+ *なかった*</span><br/><small class="gray">(nakatta-form (なかった形))</small>

{.table .table-hover .table-sm}

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
Exceptions for informal positive perfective form (due to exceptions in euphonic change,
so these also apply to ~て form, ~たら form, etc):
<ul>
<li>行く (to go) -> 行った (went)</li>
<li>問う (to ask/blame) -> 問うた (asked/blamed)</li>
<li>請う (to beg) -> 請うた (begged)</li>
</ul>
</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A ~た form verb can be turned into:
<ul>
<li>~たり form, to describe a non-exhaustive list of actions,
for example: {{'飲'|ruby:'の'}}んだ<span class="red">り</span>、{{'食'|ruby:'た'}}べた<span class="red">り</span>する</li>
<li>~たら form, becoming a past-<a href="#conditional">conditional</a> verb.</li>
</ul>
</div>

#### Gerundive {#gerundive}

More commonly known as te-form ({{furi.teForm}}).

Type | Rule
-- | --
Normal|Continuative-euphonic (連用形+音便) <span class="green">+ *て/で*</span>
Nai-form|Nai-form (ない形) <span class="green">+ *で*</span>

{.table .table-hover .table-sm}

Te-form has various uses, including but not limited to:

Type | Rule | Example
-- | -- | --
Permission|Te-form (て形) <span class="green">+ *もいい*</span>|ここで{{'食'|ruby:'た'}}べて<span class="green">もいい</span> (can eat here)
Prohibition|Te-form (て形) <span class="green">+ *はいけない*</span>|ここで{{'食'|ruby:'た'}}べて<span class="green">はいけない</span> (must not eat here)
Request|Te-form (て形) <span class="green">+ *くれ*</span>|{{'食'|ruby:'た'}}べて<span class="green">くれ？</span> (informal: will you eat?)
Conjunction|Te-form (て形) <span class="green">+ another verb</span>|レストランへ{{'行'|ruby:'い'}}って<span class="green">{{'食'|ruby:'た'}}べる</span> (go to restaurant and eat)

{.table .table-hover .table-sm .align-text-bottom}

Te-form can attach a helper auxiliary verb ({{'補'|ruby:'ほ'}}{{'助'|ruby:'じょ'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}) to change its meaning:

Auxiliary | Restrictions | Meaning | Example
-- | -- | -- | --
ある|transitive|state modification|{{'開'|ruby:'ひら'}}いて<span class="green">ある</span>(opened and is still open)
いる|transitive|progressive aspect / continuous action|{{'寝'|ruby:'ね'}}て<span class="green">いる</span>(is sleeping)
いる|intransitive|state modification|{{'閉'|ruby:'し'}}まって<span class="green">いる</span>(is closed)
{{'置'|ruby:'お'}}く|-|do something in advance|{{'食'|ruby:'た'}}べて<span class="green">おく</span>(eat in advance)
{{'置'|ruby:'お'}}く|-|keep state|{{'開'|ruby:'あ'}}けて<span class="green">おく</span>(keep it open)

{.table .table-hover .table-sm .align-text-bottom}

#### Continuative {#continuative}

Continuative form ({{furi.continuativeForm}}) can be conjuncted with other words for various uses, including but not limited to:

Type | Rule | Example
-- | -- | --
Desire|Continuative (連用形) <span class="green">+ *たい*</span>|{{'飲'|ruby:'の'}}み<span class="green">たい</span> (want to drink)
Purpose|Continuative (連用形) <span class="green">+ *に* particle</span>|{{'飲'|ruby:'の'}}み<span class="green">に</span>いく (go for drinking)
Compound noun|Continuative (連用形) <span class="green">+ suffix</span>|{{'飲'|ruby:'の'}}み<span class="green">もの</span> (beverage)

{.table .table-hover .table-sm .align-text-bottom}

#### Conditional {#conditional}

Conditional form ({{furi.conditionalForm}}), to describe "if/when" uses.

- Provisional conditional verbs emphasize more on the condition than the result.

Type | Rule | Example
-- | -- | --
Provisional|Hypothetical (仮定形) <span class="green">+ *ば*</span>|{{'飲'|ruby:'の'}}め<span class="green">ば</span><br/>{{'食'|ruby:'た'}}べれ<span class="green">ば</span>
Past|Continuative-euphonic (連用形+音便) <span class="green">+ *たら*</span>|{{'飲'|ruby:'の'}}ん<span class="green">だら</span><br/>{{'食'|ruby:'た'}}べ<span class="green">たら</span>

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The negative conditional (なければ) can be negated with ならない, literally meaning "to avoid that, will not happen".

For example, {{'食'|ruby:'た'}}べなけれ<span class="green">ば</span>ならない (must eat)
</div>

#### Volitional {#volitional}

Volitional form ({{furi.volitionalForm}}), to express will/intention/invitation/guess.

- Irrealis stem of a Godan verb used for this form ends with *-o* instead of *-a*.

Verb | Rule | Example
-- | -- | --
Godan|Irrealis-o (未然形) <span class="green">+ *う*</span>|{{'飲'|ruby:'の'}}も<span class="green">う</span>
Ichidan/Ka/Sa|Irrealis (未然形) <span class="green">+ *よう*</span>|{{'食'|ruby:'た'}}べ<span class="green">よう</span>
Formal|Masu-form (ます形) <span class="red">- *す*</span> <span class="green">+ *しょう*</span>|{{'食'|ruby:'た'}}べま<span class="green">しょう</span>
Honorific/ある|Terminal (終止形) <span class="red">- *る*</span> <span class="green">+ *ろう*</span>|くださ<span class="green">ろう</span>

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
There are verbs that do not have volitional form, for example: わかる (to understand), できる (to be able)
</div>

#### Passive {#passive}

Passive form ({{furi.passiveForm}}), to emphasize on the verb instead of the subject.

Verb | Rule | Example
-- | -- | --
Godan/Sa|Irrealis (未然形) <span class="green">+ *れる*</span>|{{'飲'|ruby:'の'}}ま<span class="green">れる</span>
Ichidan/Ka|Irrealis (未然形) <span class="green">+ *られる*</span>|{{'食'|ruby:'た'}}べ<span class="green">られる</span>
Honorific|Terminal (終止形) <span class="red">- *る*</span> <span class="green">+ *られる*</span>|くださ<span class="green">られる</span>
ある|-|ある

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in passive form becomes a Shimo-Ichidan verb (下一段).
</div>

#### Causative {#causative}

Causative form ({{furi.causativeForm}}), to indicate that the subject is forced/allowed to do something.

Verb | Rule | Example
-- | -- | --
Godan/Sa|Irrealis (未然形) <span class="green">+ *せる*</span>|{{'飲'|ruby:'の'}}ま<span class="green">せる</span>
Ichidan/Ka|Irrealis (未然形) <span class="green">+ *させる*</span>|{{'食'|ruby:'た'}}べ<span class="green">させる</span>
Honorific|Terminal (終止形) <span class="red">- *る*</span> <span class="green">+ *らせる*</span>|くださ<span class="green">らせる</span>
ある|-|ある

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in causative form becomes a Shimo-Ichidan verb (下一段).
</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A causative verb can be conjugated to passive form, becoming a causative-passive verb.

For example, {{'食'|ruby:'た'}}べる -> {{'食'|ruby:'た'}}べ<span class="red">させる</span> -> {{'食'|ruby:'た'}}べ<span class="red">させ</span>られる.
</div>

#### Potential {#potential}

Potential form ({{furi.potentialForm}}), to describe capability / ability to do something / asking a favor.

Verb | Rule | Example
-- | -- | --
Godan|Hypothetical (仮定形) <span class="green">+ *る*</span>|{{'飲'|ruby:'の'}}め<span class="green">る</span>
Ichidan/Ka|Irrealis (未然形) <span class="green">+ *られる*</span>|{{'食'|ruby:'た'}}べ<span class="green">られる</span>
Sa|できる|<span class="green">できる</span>
ある|-|ある

{.table .table-hover .table-sm}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in potential form becomes a Shimo-Ichidan verb (下一段).
</div>

## Auxiliary {#auxiliary}

Auxiliary verbs attach to a verbal or adjectival stem form and conjugate as verbs.

### Pure {#auxiliary-pure}

Pure auxiliary verbs ({{'助'|ruby:'じょ'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}) are verb endings and do not exist independently.

A pure auxiliary verb ending that is well known is ~ます, which turns a verb into polite form.

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in ~ます form has different stems and conjugations on it has different rules,
which are a bit exceptional / advanced and will not be discussed here.
</div>

Other examples include those for passive and/or potential forms (~れる, ~られる), and causative form (~せる, ~させる).

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in either potential, causative, or passive forms is a Shimo-Ichidan verb (下一段), hence in these forms it has different conjugation rules.
</div>

### Helper {#auxiliary-helper}

Helper auxiliary verbs ({{'補'|ruby:'ほ'}}{{'助'|ruby:'じょ'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}) attach to another verb to change its meaning.

Usually they attach to te-form (て形) or continuative form (連用形).

## Transitive & Intransitive {#transitive-intransitive}

{{4|jlpt}} {{'I01.L29'|icls}}

A verb is either a *transitive* verb ({{'他'|ruby:'た'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}})
or *intransitive* verb ({{'自'|ruby:'じ'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}).

- *Transitive*: Takes a direct object, which is marked with the を particle.
- *Intransitive*: Do not take a direct object.

Some verbs are transitive only, while some are intransitive only.
Finally, some others have both transitive and intransitive variants, for example:

<blockquote class="blockquote-qna">
Transitive:
<div class="text-1-25">
{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}がクラス<span class="blue">を</span><span class="red">{{'始'|ruby:'はじ'}}める</span>。
</div>
(Teacher starts the class.)
</blockquote>

<blockquote class="blockquote-qna">
Intransitive:
<div class="text-1-25">
クラス<span class="blue">が</span><span class="red">{{'始'|ruby:'はじ'}}まる</span>。
</div>
(The class starts.)
</blockquote>

## Will & Involuntary {#will-involuntary}

In Japanese language, verbs may have the semantics of *will*, the intention of doing something.

- *Will ({{'意'|ruby:'い'}}{{'志'|ruby:'し'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}})*: A verb that is used when the subject has an intent of doing so.
- *Involuntary ({{'無'|ruby:'む'}}{{'意'|ruby:'い'}}{{'志'|ruby:'し'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}})*: A verb that is used when the subject has no intent of doing so.

This is well explained in [this article](https://www.miraiju.jp/gengo05.html)
and explored in [this article](https://jn1et.com/verb-has-ones-will/) (both in Japanese).
