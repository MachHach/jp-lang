---
layout: reader.html
title: 'Grammar: Misc'
navbar:
  grammar: 'active'
---

# Misc

## Who {#who}

{{5|jlpt}} {{'B00.L01'|icls}}

{{'私'|ruby:'わたし'}}は<span class="red">ブラジルエアー</span>の<span class="orange">{{'社'|ruby:'しゃ'}}{{'員'|ruby:'いん'}}</span>です。
(I am <span class="red">Brazil Air</span>'s <span class="orange">employee</span>.)

<blockquote class="blockquote-qna">

Q: Ahmadさんはマレーシア{{'人'|ruby:'じん'}}ですか。
(Is Ahmad Malaysian?)

A1: はい、マレーシア{{'人'|ruby:'じん'}}<span class="red">です</span>。
(Yes, <span class="red">(is)</span> Malaysian.)

A2: いいえ、マレーシア{{'人'|ruby:'じん'}}<span class="red">じゃありません</span>。
(No, <span class="red">(is) not</span> Malaysian.)

</blockquote>

<blockquote class="blockquote-qna">

{{'私'|ruby:'わたし'}}は{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'人'|ruby:'じん'}}です。
(I am Japanese.)

{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">も</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'人'|ruby:'じん'}}です。
(Yamada is <span class="red">also</span> Japanese.)

Kumarさん<span class="red">は</span>マレーシア{{'人'|ruby:'じん'}}です。
(Kumar is Malaysian.)

</blockquote>

<span class="red">あの{{'人'|ruby:'ひと'}}</span>はBeckhamさんです。
(<span class="red">That person</span> is Beckham.)

<span class="red">あの{{'方'|ruby:'かた'}}</span>はObamaさんです。
(<span class="red">That (respected) person</span> is Obama.)

<blockquote class="blockquote-qna">

Q: あの{{'人'|ruby:'ひと'}}は<span class="red">{{'誰'|ruby:'だれ'}}</span>ですか。
(<span class="red">Who</span> is that person?)

A: Beckhamさんです。
(Beckham.)

</blockquote>

<blockquote class="blockquote-qna">

Q: あの{{'方'|ruby:'かた'}}は<span class="red">どなた</span>ですか。
(<span class="red">Who</span> is that (respected) person?)

A: Obamaさんです。
(Obama.)

</blockquote>

## How old {#how-old}

{{5|jlpt}} {{'B00.L01'|icls}}

{{'鈴'|ruby:'すず'}}{{'木'|ruby:'き'}}さんは<span class="red">３０{{'歳'|ruby:'さい'}}</span>です。
(Suzuki is <span class="red">30 years old</span>.)

<blockquote class="blockquote-qna">

Q: {{'鈴'|ruby:'すず'}}{{'木'|ruby:'き'}}さんは<span class="red">{{'何'|ruby:'なん'}}{{'歳'|ruby:'さい'}}</span>ですか。
(<span class="red">How old</span> is Suzuki?)

A1: ３０さいです。
(30 years old.)

A2: <span class="red">{{'秘'|ruby:'ひ'}}{{'密'|ruby:'みつ'}}</span>です。
(It's a <span class="red">secret</span>.)

</blockquote>

<blockquote class="blockquote-qna">

Q: あの{{'方'|ruby:'かた'}}は<span class="red">おいくつ</span>ですか。
(<span class="red">How old</span> is that (respected) person?)

A: ８０さいです。
(80 years old.)

</blockquote>

## What {#what}

{{5|jlpt}} {{'B00.L02'|icls}}

<span class="red">これ</span>はボールペンです。
(<span class="red">This</span> is a ballpoint pen.)

<blockquote class="blockquote-qna">

Q: これは{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}ですか。
(Is this a clock?)

A1: はい、{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}<span class="red">です</span>。
(Yes, <span class="red">(is)</span> a clock.)

A2: いええ、{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}<span class="red">じゃありません</span>。これは{{'本'|ruby:'ほん'}}です。
(No, <span class="red">(is) not</span> a clock. It is a book.)

</blockquote>

<blockquote class="blockquote-qna">

Q: これは<span class="red">{{'何'|ruby:'なん'}}</span>ですか。
(<span class="red">What</span> is this?)

A: {{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}です。
(A clock.)

</blockquote>

<blockquote class="blockquote-qna">

Q: これは「い」ですか。「り」ですか。
(Is this "i"? Or "ri"?)

A: 「い」です。
("i".)

</blockquote>

これはサッカー<span class="red">の</span>{{'雑'|ruby:'ざっ'}}{{'誌'|ruby:'し'}}です。
(This is a soccer<span class="red">'s</span> magazine.)

<blockquote class="blockquote-qna">

Q: これは<span class="red">{{'何'|ruby:'なん'}}の</span>{{'本'|ruby:'ほん'}}ですか。
(<span class="red">What (kind of)</span> book is this?)

A: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}<span class="red">の</span>{{'本'|ruby:'ほん'}}です。
(Japanese language<span class="red">'s</span> book.)

</blockquote>

## Whose {#whose}

{{5|jlpt}} {{'B00.L02'|icls}}

これは<span class="red">{{'私'|ruby:'わたし'}}の</span>{{'本'|ruby:'ほん'}}です。
(This is <span class="red">my</span> book.)

<blockquote class="blockquote-qna">

Q: これは<span class="red">{{'誰'|ruby:'だれ'}}の</span>ノートですか。
(<span class="red">Whose</span> notebook is this?)

A: {{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">の</span>ノートです。
(Yamada<span class="red">'s</span> notebook.)

</blockquote>

<blockquote class="blockquote-qna">

Q: これはIsmailさんのカメラですか。
(Is this Ismail's camera?)

A1: はい、Ismailさん<span class="orange">の</span><span class="red">です</span>。
(Yes, <span class="red">(is)</span> Ismail<span class="orange">'s</span>.)

A2: いいえ、Ismailさん<span class="orange">の</span><span class="red">じゃありません</span>。
(No, <span class="red">(is) not</span> Ismail<span class="orange">'s</span>.)

</blockquote>

これは<span class="red">{{'私'|ruby:'わたし'}}の</span>{{'本'|ruby:'ほん'}}です。
(This is <span class="red">my</span> book.)

この{{'本'|ruby:'ほん'}}は<span class="red">{{'私'|ruby:'わたし'}}の</span>です。
(This book is <span class="red">mine</span>.)

<blockquote class="blockquote-qna">

Q: この{{'本'|ruby:'ほん'}}は<span class="red">{{'誰'|ruby:'だれ'}}の</span>ですか。
(<span class="red">Whose</span> is this book?)

A: この{{'本'|ruby:'ほん'}}は{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}<span class="red">の</span>です。
(This book is teacher<span class="red">'s</span>.)

</blockquote>

## How much {#how-much}

{{5|jlpt}} {{'B00.L03'|icls}}

この{{'本'|ruby:'ほん'}}は２５００<span class="red">{{'円'|ruby:'えん'}}</span>です。
(This book is (worth) 2500 <span class="red">yen</span>.)

<blockquote class="blockquote-qna">

Q: この{{'本'|ruby:'ほん'}}は<span class="red">いくら</span>ですか。
(<span class="red">How much (worth)</span> is this book?)

A: ２５００{{'円'|ruby:'えん'}}です。
(2500 yen.)

</blockquote>

## Where {#where}

{{5|jlpt}} {{'B00.L03'|icls}}

<span class="red">ここ</span>は{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}です。
(<span class="red">Here</span> is classroom.)

<blockquote class="blockquote-qna">

Q: ここは{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}ですか。
(Is here a classroom?)

A1: はい、そうです。
(Yes.)

A2: いいえ、{{'違'|ruby:'ちが'}}います。
(No.)

</blockquote>

<blockquote class="blockquote-qna">

Q: トイレは<span class="red">どこ</span>ですか。
(<span class="red">Where</span> is the toilet?)

A: <span class="red">あそこ</span>です。
(<span class="red">There</span>.)

</blockquote>

<blockquote class="blockquote-qna">

Q1: テレビは<span class="red">どこ</span>ですか。
(<span class="red">Where</span> is the TV?)

Q2: テレビは<span class="red">{{'何'|ruby:'なん'}}{{'階'|ruby:'がい'}}</span>ですか。
(<span class="red">Which floor</span> is the TV (at)?)

A: <span class="red">４{{'階'|ruby:'かい'}}</span>です。
(<span class="red">4th floor</span>.)

</blockquote>

{{'私'|ruby:'わたし'}}の{{'国'|ruby:'くに'}}はの{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}です。
(My country is Japan.)

<blockquote class="blockquote-qna">

Q: あなたの<span class="orange">{{'国'|ruby:'くに'}}</span>は<span class="red">どこ</span>ですか。
(<span class="red">Where</span> is your <span class="orange">country</span>?)

A: マレーシアです。
(Malaysia.)

</blockquote>

<blockquote class="blockquote-qna">

Q: あなたの<span class="orange">お{{'国'|ruby:'くに'}}</span>は<span class="red">どちら</span>ですか。
(<span class="red">Where (respected)</span> is your <span class="orange">country</span>?)

A: マレーシアです。
(Malaysia.)

</blockquote>

{{'私'|ruby:'わたし'}}の{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}はパワー{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}です。
(My company is Power Electric.)

<blockquote class="blockquote-qna">

Q: あなたの{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}は<span class="red">どこ</span>ですか。
(<span class="red">Where</span> is your company.)

A: パワー{{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}です。
(Power Electric.)

</blockquote>

これはドイツの{{'車'|ruby:'くるま'}}です。
(This is a German car.)

これはトヨタの{{'車'|ruby:'くるま'}}です。
(This is a Toyota car.)

<blockquote class="blockquote-qna">

Q: それは<span class="red">どこの</span>{'車'|ruby:'くるま'}}ですか。
(<span class="red">What (from where)</span> is this car?)

A1: ドイツの{{'車'|ruby:'くるま'}}です。
(German car.)

A2: トヨタの{{'車'|ruby:'くるま'}}です。
(Toyota car.)

</blockquote>

## When {#when}

{{5|jlpt}} {{'B00.L04'|icls}}

<span class="red">{{'今'|ruby:'いま'}}</span>{{'午'|ruby:'ご'}}{{'後'|ruby:'ご'}}７{{'時'|ruby:'じ'}}４５{{'分'|ruby:'ふん'}}です。
(<span class="red">Now</span> is 7:45 PM.)

<span class="red">{{'今日'|ruby:'きょう'}}</span>は{{'火'|ruby:'か'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}です。
(<span class="red">Today</span> is Tuesday.)

<blockquote class="blockquote-qna">

Q: {{'今日'|ruby:'きょう'}}は<span class="red">{{'何'|ruby:'なん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}</span>ですか。
(<span class="red">What day of week</span> is today?)

A: {{'火'|ruby:'か'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}です。
(Tuesday.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}のクラスは<span class="red">{{'何'|ruby:'なん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}</span>ですか。
(<span class="red">What day(s) of week</span> is Japanese language class?)

A: {{'火'|ruby:'か'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}<span class="red">と</span>{{'金'|ruby:'きん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}です。
(Tuesday <span class="red">and</span> Friday.)

</blockquote>

{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}は{{'金'|ruby:'きん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}です。
(Meeting is at Friday.)

{{'休'|ruby:'やす'}}みは{{'日'|ruby:'にち'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}と{{'土'|ruby:'ど'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}です。
(Rest days are Sunday and Saturday.)

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}のクラスは<span class="orange">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}</span><span class="red">から</span>、<span class="orange">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}</span><span class="red">まで</span>ですか。
(<span class="red">From</span> <span class="orange">what time</span> <span class="red">to</span> <span class="orange">what time</span> is Japanese language class?)

A: ７{{'時'|ruby:'じ'}}{{'半'|ruby:'はん'}}<span class="red">から</span>、９{{'時'|ruby:'じ'}}<span class="red">まで</span>です。
(<span class="red">From</span> 7:30 <span class="red">to</span> 9:00.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}は<span class="orange">{{'何'|ruby:'なん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}</span><span class="red">から</span>、<span class="orange">{{'何'|ruby:'なん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}</span><span class="red">まで</span>ですか。
(<span class="red">From</span> <span class="orange">which day of week</span> <span class="red">to</span> <span class="orange">which day of week</span> are company work days?)

A: {{'月'|ruby:'げつ'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}<span class="red">から</span>、{{'金'|ruby:'きん'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}<span class="red">まで</span>です。
(<span class="red">From</span> Monday <span class="red">to</span> Friday.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="orange">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}</span><span class="red">から</span><span class="orange">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}</span><span class="red">まで</span>{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}しますか。
(<span class="red">From</span> <span class="orange">what time</span> <span class="red">to</span> <span class="orange">what time</span> is study?)

A: ７{{'時'|ruby:'じ'}}{{'半'|ruby:'はん'}}<span class="red">から</span>９{{'時'|ruby:'じ'}}<span class="red">まで</span>{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}します。
(<span class="red">From</span> 7:30 <span class="red">to</span> 9:00.)

</blockquote>

{{'毎'|ruby:'まい'}}{{'朝'|ruby:'あさ'}}７{{'時'|ruby:'じ'}}<span class="red">に</span>{{'起'|ruby:'お'}}きます。
(Every morning I wake up <span class="red">at</span> 7:00.)

<blockquote class="blockquote-qna">

Q: <span class="orange">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}</span><span class="red">に</span>{{'起'|ruby:'お'}}きますか。
(<span class="red">At</span> <span class="orange">what time</span> do you wake up?)

A: ９じ<span class="red">に</span>{{'起'|ruby:'お'}}きます。
(I wake up <span class="red">at</span> 9:00.)

</blockquote>

Time | Example | Use 「に」?
---- | ------- | ---
Number ({{'数'|ruby:'すう'}}{{'字'|ruby:'じ'}})|6:00 (６{{'時'|ruby:'じ'}}), 12:00 (１２{{'時'|ruby:'じ'}})|<span class="green">Yes</span> (<span class="red">No if exists the ごろ suffix</span>)
Special day|Christmas (クリスマス)|<span class="green">Yes</span>
Long vacation|Summer vacation ({{'夏'|ruby:'なつ'}}{{'休'|ruby:'やす'}}み), winter vacation ({{'冬'|ruby:'ふゆ'}}{{'休'|ruby:'やす'}}み)|<span class="green">Yes</span>
Non-number|Night ({{'夜'|ruby:'よる'}})|<span class="red">No</span>
Recurrence|Every day ({{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}})|<span class="red">No</span>

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q1: {{'次'|ruby:'じ'}}{{'郎'|ruby:'ろう'}}ちゃんの{{'誕'|ruby:'たん'}}{{'生'|ruby:'じょう'}}{{'日'|ruby:'び'}}は<span class="red">{{'何'|ruby:'なん'}}{{'月'|ruby:'がつ'}}{{'何'|ruby:'なん'}}{{'日'|ruby:'にち'}}</span>ですか。
(<span class="red">What month (and) what day</span> is Jiro's birthday?)

Q2: {{'次'|ruby:'じ'}}{{'郎'|ruby:'ろう'}}ちゃんの{{'誕'|ruby:'たん'}}{{'生'|ruby:'じょう'}}{{'日'|ruby:'び'}}は<span class="red">いつ</span>ですか。
(<span class="red">When</span> is Jiro's birthday?)

A: ３{{'月'|ruby:'がつ'}}１０{{'日'|ruby:'か'}}です。
(March 10th.)

</blockquote>

## What number {#what-number}

{{5|jlpt}} {{'B00.L04'|icls}}

<blockquote class="blockquote-qna">

Q: {{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}{{'番'|ruby:'ばん'}}{{'号'|ruby:'ごう'}}は<span class="red">{{'何'|ruby:'なん'}}{{'番'|ruby:'ばん'}}</span>ですか。
(<span class="red">What number</span> is your phone number?)

A: {{'５'|ruby:'ご'}}{{'２'|ruby:'に'}}{{'７'|ruby:'なな'}}{{'５'|ruby:'ご'}}の{{'２'|ruby:'に'}}{{'７'|ruby:'なな'}}{{'２'|ruby:'に'}}{{'５'|ruby:'ご'}}です。
(5275-2725.)

</blockquote>

## Present & past tense {#present-past-tense}

{{5|jlpt}} {{'B00.L04'|icls}}

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ます</span>か。
(<span class="red">Do</span> you study tomorrow?)

A1: はい、{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ます</span>。
(Yes, I <span class="red">(do)</span> study.)

A2: いいえ、{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ません</span>。
(No, I <span class="red">don't</span> study.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'昨日'|ruby:'きのう'}}{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ました</span>か。
(<span class="red">Did</span> you study yesterday?)

A1: はい、{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ました</span>。
(Yes, I <span class="red">(did)</span> studied.)

A2: いいえ、{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}し<span class="red">ませんでした</span>。
(No, I <span class="red">didn't</span> study.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'昨日'|ruby:'きのう'}}{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}<span class="orange">に</span>{{'起'|ruby:'お'}}き<span class="red">ました</span>か。
(Yesterday what time <span class="red">did</span> you wake up <span class="orange">at</span>?)

A: ６{{'時'|ruby:'じ'}}<span class="orange">に</span>{{'起'|ruby:'お'}}き<span class="red">ました</span>。
(I <span class="red">(did)</span> woke up <span class="orange">at</span> 6:00.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'昨日'|ruby:'きのう'}}{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}<span class="orange">から</span>{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}<span class="orange">まで</span>{{'働'|ruby:'はたら'}}き<span class="red">ました</span>か。
(Yesterday <span class="orange">from</span> what time <span class="orange">to</span> what time <span class="red">did</span> you work?)

A: ８{{'時'|ruby:'じ'}}<span class="orange">から</span>５{{'時'|ruby:'じ'}}<span class="orange">まで</span>{{'働'|ruby:'はたら'}}き<span class="red">ました</span>。
(I <span class="red">(did)</span> worked <span class="orange">from</span> 8:00 <span class="orange">to</span> 5:00.)

</blockquote>

## Going places {#going-places}

{{5|jlpt}} {{'B00.L05'|icls}}

{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}<span class="red">へ</span>{{'行'|ruby:'い'}}きます。
(Go <span class="red">to</span> school.)

{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}<span class="red">へ</span>{{'来'|ruby:'き'}}ます。
(Come <span class="red">to</span> school.)

{{'家'|ruby:'うち'}}<span class="red">へ</span>{{'帰'|ruby:'かえ'}}ります。
(Return <span class="red">to</span> (my) home.)

{{'国'|ruby:'くに'}}<span class="red">へ</span>{{'帰'|ruby:'かえ'}}ります。
(Return <span class="red">to</span> (my) country.)

<blockquote class="blockquote-qna">

Q: {{'毎'|ruby:'まい'}}{{'朝'|ruby:'あさ'}}<span class="red">どこ</span><span class="orange">へ</span>{{'行'|ruby:'い'}}きますか。
(Every morning <span class="red">where</span> do you go <span class="orange">to</span>?)

A: {{'駅'|ruby:'えき'}}<span class="orange">へ</span>{{'行'|ruby:'い'}}きます。
(Go <span class="orange">to</span> station.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'昨日'|ruby:'きのう'}}<span class="red">どこ</span><span class="orange">へ</span>{{'行'|ruby:'い'}}きましたか。
(Yesterday <span class="red">where</span> did you go <span class="orange">to</span>?)

A: {{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}<span class="orange">へ</span>{{'行'|ruby:'い'}}きました。
(Went <span class="orange">to</span> school.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}<span class="red">どこ</span>へ{{'行'|ruby:'い'}}きますか。
(Tomorrow <span class="red">where</span> do you go to?)

A: <span class="red">どこも</span>{{'行'|ruby:'い'}}き<span class="orange">ません</span>。
(<span class="orange">Don't</span> go (to) <span class="red">anywhere</span>.)

</blockquote>

{{'自'|ruby:'じ'}}{{'転'|ruby:'てん'}}{{'車'|ruby:'しゃ'}}<span class="red">で</span>{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'行'|ruby:'い'}}きます。
(Go to school <span class="red">by</span> bicycle.)

<span class="red">{{'歩'|ruby:'ある'}}いて</span>{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'行'|ruby:'い'}}きます。
(Go to school <span class="red">by foot</span>.)

<blockquote class="blockquote-qna">

Q: <span class="red">{{'何'|ruby:'なん'}}</span><span class="orange">で</span>{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'行'|ruby:'い'}}きますか。
(<span class="orange">By</span> <span class="red">what (transportation)</span> do you go to school?)

A: {{'自'|ruby:'じ'}}{{'転'|ruby:'てん'}}{{'車'|ruby:'しゃ'}}<span class="orange">で</span>{{'行'|ruby:'い'}}きます。
(Go <span class="orange">by</span> bicycle.)

</blockquote>

{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}<span class="red">と</span>デパートへ{{'行'|ruby:'い'}}きます。
(Go to department store <span class="red">with</span> friend.)

<span class="red">{{'一人'|ruby:'ひとり'}}で</span>デパートへ{{'行'|ruby:'い'}}きます。
(Go to department store <span class="red">alone</span>.)

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}<span class="red">{{'誰'|ruby:'だれ'}}</span><span class="orange">と</span>デパートへ{{'行'|ruby:'い'}}きますか。
(Tomorrow <span class="orange">with</span> <span class="red">who</span> do you go to department store?)

A: {{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}<span class="orange">と</span>{{'行'|ruby:'い'}}きます。
(Go <span class="orange">with</span> friend.)

</blockquote>

１１{{'月'|ruby:'がつ'}}１５{{'日'|ruby:'にち'}}<span class="red">に</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'行'|ruby:'い'}}きます。
(Go to Japan <span class="red">on</span> November 15th.)

１０{{'月'|ruby:'がつ'}}<span class="red">に</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'行'|ruby:'い'}}きました。
(Went to Japan <span class="red">on</span> October.)

{{'先'|ruby:'せん'}}{{'月'|ruby:'げつ'}}<span class="text-decoration-line-through orange">に</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'行'|ruby:'い'}}きました。
(Went to Japan <span class="text-decoration-line-through orange">on</span> last month)

{{'明日'|ruby:'あした'}}２{{'時'|ruby:'じ'}}<span class="red">に</span>デパートへ{{'行'|ruby:'い'}}きます。
(Go to department store <span class="red">at</span> 2 o' clock tomorrow.)

{{'先'|ruby:'せん'}}{{'週'|ruby:'しゅう'}}<span class="text-decoration-line-through orange">に</span>デパートへ{{'行'|ruby:'い'}}きました。
(Went to department store <span class="text-decoration-line-through orange">on</span> last week.)

{{'水'|ruby:'すい'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}<span class="red">［に］</span>{{'行'|ruby:'い'}}きました。
(Went <span class="red">on</span> Wednesday.)

<blockquote class="blockquote-qna">

Q: <span class="red">いつ</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'行'|ruby:'い'}}きますか。
(<span class="red">When</span> do you go to Japan?)

A: １１{{'月'|ruby:'がつ'}}１５{{'日'|ruby:'にち'}}<span class="red">に</span>{{'行'|ruby:'い'}}きます。
(Go on <span class="red">on</span> November 15th.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">いつ</span>デパートへ{{'行'|ruby:'い'}}きましたか。
(<span class="red">When</span> did you go to department store?)

A: {{'先'|ruby:'せん'}}{{'週'|ruby:'しゅう'}}<span class="text-decoration-line-through orange">に</span>{{'行'|ruby:'い'}}きました。
(Went <span class="text-decoration-line-through orange">on</span> last week.)

</blockquote>

Use | Meaning | Question
--: | ------- | ---
{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<span class="orange">へ</span>|<span class="orange">to</span> place|どこ<span class="orange">へ</span>
{{'乗'|ruby:'のり'}}{{'物'|ruby:'もの'}}<span class="red">で</span>{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<span class="orange">へ</span>|<span class="orange">to</span> place <span class="red">by</span> transportation|{{'何'|ruby:'なん'}}<span class="red">で</span>
{{'人'|ruby:'ひと'}}<span class="red">と</span>{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<span class="orange">へ</span>|<span class="orange">to</span> place <span class="red">with</span> person|{{'誰'|ruby:'だれ'}}<span class="red">と</span>
いつ<span class="red">［に］</span>{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<span class="orange">へ</span>|<span class="orange">to</span> place <span class="red">at</span> when|いつ

{.table .table-hover .table-sm .align-text-bottom}

## Actions {#actions}

{{5|jlpt}} {{'B00.L06'|icls}}

パン<span class="red">を</span>{{'食'|ruby:'た'}}べます。
(Eat <span class="red">(acting upon)</span> bread.)

{{'音'|ruby:'おん'}}{{'楽'|ruby:'がく'}}<span class="red">を</span>{{'聞'|ruby:'き'}}きます。
(Listen (to) music.)

<blockquote class="blockquote-qna">

Q: {{'新'|ruby:'しん'}}{{'聞'|ruby:'ぶん'}}<span class="red">を</span>{{'読'|ruby:'よ'}}みますか。
(Do you read newspaper?)

A1: はい、{{'読'|ruby:'よ'}}みます。
(Yes, I read.)

A2: いいえ、{{'読'|ruby:'よ'}}みません。
(No, I don't read.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">{{'何'|ruby:'なに'}}</span><span class="orange">を</span>{{'食'|ruby:'た'}}べますか。
(<span class="red">What</span> do you eat?)

A: パン<span class="orange">を</span>{{'食'|ruby:'た'}}べます。
(Eat bread.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">{{'誰'|ruby:'だれ'}}</span><span class="orange">を</span>{{'撮'|ruby:'と'}}りますか。
(<span class="red">Who</span> are you taking photos of?)

A: {{'赤'|ruby:'あか'}}ちゃん<span class="orange">を</span>{{'撮'|ruby:'と'}}ります。
(Take photo of the baby.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}、{{'何'|ruby:'なに'}}を<span class="red">します</span>か。
(What do you <span class="red">do</span> tomorrow?)

A: デパートへ{{'行'|ruby:'い'}}きます。レポートを{{'書'|ruby:'か'}}きます。
(Go to department store. Write report.)

</blockquote>

{{'宿'|ruby:'しゅく'}}{{'題'|ruby:'だい'}}をします。<span class="red">それから</span>、サッカーをします。
(Do homework. <span class="red">Then</span>, play soccer.)

スーパー<span class="red">で</span>{{'魚'|ruby:'さかな'}}を{{'買'|ruby:'か'}}います。
(Buy fish <span class="red">at</span> the supermarket.)

Particle | Use | Example
-------- | --- | -------
「へ」|The place is directly related to the action|{{'行'|ruby:'い'}}きます、{{'来'|ruby:'き'}}ます、{{'帰'|ruby:'かえ'}}ります
「で」|The place is not the focus of the action|-

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: <span class="red">どこ</span><span class="orange">で</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}しますか。
(<span class="red">Where</span> do you study Japanese language <span class="orange">at</span>?)

A: ICLS<span class="orange">で</span>{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}します。
(Study <span class="orange">at</span> ICLS.)

</blockquote>

## Invitation {#invitation}

{{5|jlpt}} {{'B00.L06'|icls}}

{{'始'|ruby:'はじ'}}め<span class="red">ましょう</span>。
(<span class="red">Let's</span> begin.)

カフェへ{{'行'|ruby:'い'}}き<span class="red">ましょう</span>。
(<span class="red">Let's</span> go to cafe.)

あそこでビールを{{'飲'|ruby:'の'}}み<span class="red">ましょう</span>。
(<span class="red">Let's</span> drink beer there.)

<blockquote class="blockquote-qna">

Q: <span class="orange">{{'一'|ruby:'いっ'}}{{'緒'|ruby:'しょ'}}に</span>{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'見'|ruby:'み'}}<span class="red">ません</span>か。
(<span class="red">Don't</span> you wanna watch movie <span class="orange">together</span>?)

A1: ええ、{{'見'|ruby:'み'}}<span class="red">ましょう</span>。
(Yes, <span class="red">let's</span> watch.)

A2: ええ、いいですね。
(Yes, that's good.)

A3: すみません、ちょっと．．．。
(Excuse me, that's a little bit... (inconvenient/reluctant))

</blockquote>

## で particle {#de-particle}

{{5|jlpt}} {{'B00.L07'|icls}}

Use | Example | Question
--- | ------- | --------
{{'乗'|ruby:'のり'}}{{'物'|ruby:'もの'}}<br/>(vehicle)|{{'車'|ruby:'くるま'}}<span class="red">で</span>{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'行'|ruby:'い'}}きます。<br/>(Go to school <span class="red">by</span> car.)|{{'何'|ruby:'なん'}}<br/>(what)
{{'場'|ruby:'ば'}}{{'所'|ruby:'しょ'}}<br/>(place)|{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}<span class="red">で</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}します。<br/>(Study Japanese language <span class="red">at</span> school.)|どこ<br/>(where)
{{'道'|ruby:'どう'}}{{'具'|ruby:'ぐ'}}<br/>(tool)|フォーク<span class="orange">と</span>ナイフ<span class="red">で</span>ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べます。<br/>(Eat the meal <span class="red">with</span> fork <span class="orange">and</span> knife.)|{{'何'|ruby:'なん'}}<br/>(what)
{{'言'|ruby:'げん'}}{{'語'|ruby:'ご'}}<br/>(language)|{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}<span class="red">で</span>{{'手'|ruby:'て'}}{{'紙'|ruby:'がみ'}}を{{'書'|ruby:'か'}}きます。<br/>(Write the letter <span class="red">with</span> Japanese language.)|{{'何'|ruby:'なん'}}<br/>(what)

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: <span class="red">{{'何'|ruby:'なん'}}</span><span class="orange">で</span>ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べますか。
(<span class="red">What</span> do you eat the meal <span class="orange">with</span>?)

A: {{'箸'|ruby:'はし'}}<span class="orange">で</span>{{'食'|ruby:'た'}}べます。
(Eat <span class="orange">with</span> chopsticks.)

</blockquote>

ありがとう<span class="orange">は</span>{{'英'|ruby:'えい'}}{{'語'|ruby:'ご'}}<span class="red">で</span>"_Thank you_"です。
(<span class="orange">As for</span> "Thank you", <span class="red">(with)</span> its English (language) is "_Thank you_".)

<blockquote class="blockquote-qna">

Q: ありがとうは{{'英'|ruby:'えい'}}{{'語'|ruby:'ご'}}で<span class="red">{{'何'|ruby:'なん'}}</span>ですか。
(As for "Thank you", <span class="red">what</span> is its English (language)?)

A: "_Thank you_"です。
(It's "_Thank you_".)

</blockquote>

## Give & receive {#give-receive}

{{5|jlpt}} {{'B00.L07'|icls}}

Giving end | Receiving end
---------: | ------------:
{{'上'|ruby:'あ'}}げます<br/>(give)|もらいます<br/>(receive)
{{'貸'|ruby:'か'}}します<br/>(lend)|{{'借'|ruby:'か'}}ります<br/>(borrow)
{{'教'|ruby:'おし'}}えます<br/>(teach)|{{'習'|ruby:'なら'}}います<br/>(learn)
かけます<br/>(phone)|もらいます<br/>(receive)
{{'書'|ruby:'か'}}きます<br/>(write)|もらいます<br/>(receive)

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

{{'私'|ruby:'わたし'}}は<span class="blue">サントスさん</span><span class="red">に</span>プレゼントを{{'上'|ruby:'あ'}}げます。
(I give present <span class="red">to</span> <span class="blue">Santos</span>.)

{{'私'|ruby:'わたし'}}は<span class="blue">{{'佐'|ruby:'さ'}}{{'藤'|ruby:'とう'}}さん</span><span class="red">に</span>プレゼントを<span class="orange">もらいました</span>。
(I <span class="orange">received</span> present <span class="red">from</span> <span class="blue">Sato</span>.)

("receiving a present/gift" is always in past tense!)

</blockquote>

<blockquote class="blockquote-qna">

{{'佐'|ruby:'さ'}}{{'藤'|ruby:'とう'}}さん<span class="red">に</span>{{'消'|ruby:'け'}}しゴムを{{'貸'|ruby:'か'}}します。
(Lend eraser <span class="red">to</span> Sato.)

{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">に</span>{{'消'|ruby:'け'}}しゴムを{{'借'|ruby:'か'}}ります。
(Borrow eraser <span class="red">from</span> Yamada.)

</blockquote>

<blockquote class="blockquote-qna">

{{'学'|ruby:'がく'}}{{'生'|ruby:'せい'}}<span class="red">に</span>{{'英'|ruby:'えい'}}{{'語'|ruby:'ご'}}を{{'教'|ruby:'おし'}}えます。
(Teach English language <span class="red">to</span> students.)

{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}<span class="red">に</span>{{'英'|ruby:'えい'}}{{'語'|ruby:'ご'}}を{{'習'|ruby:'なら'}}います。
(Learn English language <span class="red">from</span> teacher.)

</blockquote>

<blockquote class="blockquote-qna">

{{'母'|ruby:'はは'}}<span class="red">に</span>{{'手'|ruby:'て'}}{{'紙'|ruby:'がみ'}}を{{'書'|ruby:'か'}}きます。
(Write letter <span class="red">to</span> (my) mother.)

{{'娘'|ruby:'むすめ'}}<span class="red">に</span>{{'手'|ruby:'て'}}{{'紙'|ruby:'がみ'}}をもらいます。
(Receive letter <span class="red">from</span> daughter.)

</blockquote>

<blockquote class="blockquote-qna">

{{'彼'|ruby:'かれ'}}<span class="red">に</span>{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}をかけます。
(Make a phone call <span class="red">to</span> him.)

{{'彼'|ruby:'かの'}}{{'女'|ruby:'じょ'}}<span class="red">に</span>{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}をもらいます。
(Receive a phone call <span class="red">from</span> her.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">{{'誰'|ruby:'だれ'}}</span><span class="orange">に</span>プレゼントを{{'上'|ruby:'あ'}}げますか。
(<span class="red">Who</span> do you give the present <span class="orange">to</span>?)

A: サントスさん<span class="orange">に</span>{{'上'|ruby:'あ'}}げます。
(Give <span class="orange">to</span> Santos.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'佐'|ruby:'さ'}}{{'藤'|ruby:'とう'}}さん<span class="orange">に</span><span class="red">{{'何'|ruby:'なに'}}</span>をもらいましたか。
(<span class="red">What</span> did you receive <span class="orange">from</span> Sato?)

A1: プレゼントをもらいました。
(Received (a) present.)

A2: <span class="red">{{'何'|ruby:'なに'}}も</span>もらい<span class="orange">ませんでした</span>。
(<span class="orange">Didn't</span> receive <span class="red">anything</span>.)

</blockquote>

## Already {#already}

{{5|jlpt}} {{'B00.L07'|icls}}

<blockquote class="blockquote-qna">

Q: <span class="red">もう</span>{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}を{{'送'|ruby:'おく'}}り<span class="orange">ました</span>か。
(<span class="orange">Had</span> you send the parcel <span class="red">already</span>?)

A1: はい、<span class="red">もう</span>{{'送'|ruby:'おく'}}り<span class="orange">ました</span>。
(Yes, <span class="red">already</span> <span class="orange">(had)</span> sent.)

A2: いいえ、<span class="red">まだ</span>です。<span class="orange">これから</span>{{'送'|ruby:'おく'}}ります。
(No, <span class="red">not yet</span>. To send <span class="orange">after this</span>.)

</blockquote>

## Adjectives {#adjectives}

{{5|jlpt}} {{'B00.L08'|icls}}

{{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}{{'山'|ruby:'さん'}}は{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}<span class="orange">です</span>。
(Mt. Fuji <span class="orange">is</span> famous. (な))

{{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}{{'山'|ruby:'さん'}}は{{'高'|ruby:'たか'}}<span class="orange">い</span>です。
(Mt. Fuji <span class="orange">is</span> tall. (い))

{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さんは{{'元'|ruby:'げん'}}{{'気'|ruby:'き'}}<span class="orange">じゃありません</span>。
(Yamada <span class="orange">is not</span> well. (な))

{{'彼'|ruby:'かれ'}}のご{{'飯'|ruby:'はん'}}はおいし<span class="text-decoration-line-through red">い</span><span class="orange">くない</span>です。
(His meal <span class="orange">is not</span> delicious. (い))

<blockquote class="blockquote-qna">

Q: マラッカはきれいですか。
(Is Malacca beautiful?)

A1: はい、きれい<span class="orange">です</span>。
(Yes, it <span class="orange">is</span> beautiful. (な))

A2: いいえ、きれい<span class="orange">じゃありません</span>。
(No, it <span class="orange">is not</span> beautiful. (な))

</blockquote>

Positivity level | Adverb | Example
---------------- | ------ | -------
<span class="green">+2.0</span>|とても (very)|<span class="blue">とても</span>{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}です
<span class="green">+1.0</span>|-|{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}です
<span class="red">-0.5</span>|あまり (not so)|<span class="blue">あまり</span>{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}<span class="red">じゃありません</span>
<span class="red">-1.0</span>|-|{{'有'|ruby:'ゆう'}}{{'名'|ruby:'めい'}}<span class="red">じゃありません</span>

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}は<span class="red">どう</span>ですか。
(<span class="red">How</span> is Japanese language?)

A1: {{'楽'|ruby:'たの'}}しいです。
(Enjoyable.)

A2: {{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}いです<span class="red">が</span>、{{'難'|ruby:'むずか'}}しいです。
(Interesting <span class="red">but</span> difficult.)

(Rule: adjective <span class="red">が</span> reverse-adjective. Speaker emphasizes on the 2nd adjective.)

A3: {{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}いです。<span class="red">そして</span>、{{'楽'|ruby:'たの'}}しいです。
(Interesting <span class="red">and (also)</span> enjoyable.)

(Rule: adjective <span class="red">そして</span> similar-adjective.)

</blockquote>

{{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}{{'山'|ruby:'さん'}}はきれい<span class="red">な</span>{{'山'|ruby:'やま'}}です。
(Mt. Fuji is a beautiful mountain. (な))

{{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}{{'山'|ruby:'さん'}}は{{'高'|ruby:'たか'}}い{{'山'|ruby:'やま'}}です。
(Mt. Fuji is a tall mountain. (い))

<blockquote class="blockquote-qna">

Q: {{'富'|ruby:'ふ'}}{{'士'|ruby:'じ'}}{{'山'|ruby:'さん'}}は<span class="red">どんな</span>{{'山'|ruby:'やま'}}ですか。
(<span class="red">What kind of</span> mountain is Mt. Fuji?)

A1: きれい<span class="red">な</span>{{'山'|ruby:'やま'}}です。
(Beautiful mountain. (な))

A2: {{'高'|ruby:'たか'}}い{{'山'|ruby:'やま'}}です。
(Tall mountain. (い))

</blockquote>

すてき<span class="red">な</span>{{'鞄'|ruby:'かばん'}}を{{'買'|ruby:'か'}}いました。
(Bought (a) nice bag. (な))

{{'青'|ruby:'あお'}}い{{'鞄'|ruby:'かばん'}}を{{'買'|ruby:'か'}}いました。
(Bought (a) blue bag. (い))

ハンサム<span class="red">な</span>{{'人'|ruby:'ひと'}}に{{'会'|ruby:'あ'}}いました。
(Met (a) handsome person. (な))

## Adverbs {#adverbs}

{{5|jlpt}} {{'E01.L09'|icls}}

{{'私'|ruby:'わたし'}}はダンス<span class="red">が</span>{{'好'|ruby:'す'}}きです。
(I like to dance.)

{{'彼'|ruby:'かれ'}}はダンス<span class="red">が</span>{{'上'|ruby:'じょう'}}{{'手'|ruby:'ず'}}です。
(He is good at dance.)

{{'私'|ruby:'わたし'}}はダンス<span class="red">が</span>{{'得'|ruby:'とく'}}{{'意'|ruby:'い'}}です。
(I am good at dance.)

Positivity level | Adverb | Example
---------------- | ------ | -------
<span class="green">+2.0</span>|とても (very)|<span class="blue">とても</span>{{'好'|ruby:'す'}}きです
<span class="green">+1.0</span>|-|{{'好'|ruby:'す'}}きです
<span class="green">+0.5</span>|{{'少'|ruby:'すこ'}}し (a little)|<span class="blue">{{'少'|ruby:'すこ'}}し</span>{{'好'|ruby:'す'}}きです
<span class="red">-0.5</span>|あまり (not so)|<span class="blue">あまり</span>{{'好'|ruby:'す'}}き<span class="red">じゃありません</span>
<span class="red">-1.0</span>|-|{{'好'|ruby:'す'}}き<span class="red">じゃありません</span>
<span class="red">-2.0</span>|{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}} (not at all)|<span class="blue">{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}}</span>{{'好'|ruby:'す'}}き<span class="red">じゃありません</span>

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: ダンス<span class="red">が</span>{{'好'|ruby:'す'}}きですか。
(Do you like to dance?)

A1: はい、<span class="blue">とても</span>{{'好'|ruby:'す'}}きです。
(Yes, I like (it) <span class="blue">very much</span>.)

A2: いいえ、<span class="blue">あまり</span>{{'好'|ruby:'す'}}き<span class="red">じゃありません</span>。
(No, I <span class="blue">slightly</span> <span class="red">do not</span> like (it).)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どんな</span>{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}<span class="orange">が</span>{{'好'|ruby:'す'}}きですか。
(<span class="red">What kind of</span> movie do you like?)

A: {{'恋'|ruby:'れん'}}{{'愛'|ruby:'あい'}}{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}<span class="orange">が</span>{{'好'|ruby:'す'}}きです。
(I like romantic (genre) movie.)

</blockquote>

{{'私'|ruby:'わたし'}}はひらがな<span class="red">が</span>わかります。
(I understand hiragana.)

Positivity level | Adverb | Example
---------------- | ------ | -------
<span class="green">+2.0</span>|よく (well)|<span class="blue">よく</span>わかります
<span class="green">+1.0</span>|-|わかります
<span class="green">+0.8</span>|だいたい (roughly)|<span class="blue">だいたい</span>わかります
<span class="green">+0.5</span>|{{'少'|ruby:'すこ'}}し (a little)|<span class="blue">{{'少'|ruby:'すこ'}}し</span>わかります
<span class="red">-0.5</span>|あまり (not so)|<span class="blue">あまり</span>わかり<span class="red">ません</span>
<span class="red">-1.0</span>|-|わかり<span class="red">ません</span>
<span class="red">-2.0</span>|{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}} (not at all)|<span class="blue">{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}}</span>わかり<span class="red">ません</span>

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: ひらがな<span class="red">が</span>わかりますか。
(Do you understand hiragana?)

A1: はい、<span class="blue">よく</span>わかります。
(Yes, I understand (it) <span class="blue">well</span>.)

A2: いいえ、<span class="blue">あまり</span>わかり<span class="red">ません</span>。
(No, I <span class="blue">slightly</span> <span class="red">do not</span> understand (it).)

</blockquote>

{{'車'|ruby:'くるま'}}<span class="red">が</span>あります。
(Exists a car.)

Positivity level | Adverb | Example
---------------- | ------ | -------
<span class="green">+2.0</span>|たくさん (many)|<span class="blue">たくさん</span>あります
<span class="green">+1.0</span>|-|あります
<span class="green">+0.5</span>|{{'少'|ruby:'すこ'}}し (a little)|<span class="blue">{{'少'|ruby:'すこ'}}し</span>あります
<span class="red">-0.5</span>|あまり (not so)|<span class="blue">あまり</span>あり<span class="red">ません</span>
<span class="red">-1.0</span>|-|あり<span class="red">ません</span>
<span class="red">-2.0</span>|{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}} (not at all)|<span class="blue">{{'全'|ruby:'ぜん'}}{{'然'|ruby:'ぜん'}}</span>あり<span class="red">ません</span>

{.table .table-hover .table-sm .align-text-bottom}

## Reason {#reason}

{{5|jlpt}} {{'E01.L09'|icls}}

<blockquote class="blockquote-qna">

ひらがながわかります。
(I understand hiragana.)

Q: <span class="red">どうして</span>？
(<span class="red">Why</span>?)

A: {{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}しました<span class="red">から</span>、ひらがながわかります。
(<span class="red">Because</span> I studied, I understand hiragana.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どうして</span>{{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}コーヒーを{{'飲'|ruby:'の'}}みますか。
(<span class="red">Why</span> do you drink coffee every day?)

A: {{'好'|ruby:'す'}}きです<span class="red">から</span>。
(<span class="red">Because</span> I like (it).)

</blockquote>

## Locating {#locating}

{{5|jlpt}} {{'E01.L10'|icls}}

{{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}<span class="red">に</span>{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="orange">が</span>あります。
(<span class="red">At</span> the room exists a chair.)

(Rule: place <span class="red">に</span> thing <span class="orange">が</span>あります)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
<span class="red">に</span> is used for a place where the topic (～は) / subject (～が) is located, while <span class="blue">で</span> is used for a place where the action happens.
</div>

ここ<span class="red">に</span>パン{{'屋'|ruby:'や'}}<span class="orange">が</span>あります。
(<span class="red">At</span> here exists a bread shop.)

<blockquote class="blockquote-qna">

Q: {{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}に<span class="red">{{'何'|ruby:'なに'}}</span>がありますか。
(<span class="red">What (thing)</span> exist in the room?)

A: {{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="orange">が</span>あります。
(Exists a chair.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'冷'|ruby:'れい'}}{{'蔵'|ruby:'ぞう'}}{{'庫'|ruby:'こ'}}に<span class="red">{{'何'|ruby:'なに'}}</span>がありますか。
(<span class="red">What (thing)</span> exist in the refrigerator?)

A: <span class="red">{{'何'|ruby:'なに'}}も</span>あり<span class="orange">ません</span>。
(<span class="orange">Doesn't</span> exist <span class="red">anything</span>.)

</blockquote>

{{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}<span class="red">に</span>{{'男'|ruby:'おとこ'}}の{{'子'|ruby:'こ'}}<span class="orange">が</span>います。
(<span class="red">At</span> the room exists a boy.)

(Rule: place <span class="red">に</span> person <span class="orange">が</span>います)

{{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}<span class="red">に</span>{{'猫'|ruby:'ねこ'}}<span class="orange">が</span>います。
(<span class="red">At</span> the room exists a cat.)

(Rule: place <span class="red">に</span> animal <span class="orange">が</span>います)

<blockquote class="blockquote-qna">

Q: {{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}に<span class="red">{{'誰'|ruby:'だれ'}}</span>がいますか。
(<span class="red">Who (person)</span> exist in the room?)

A1: {{'男'|ruby:'おとこ'}}の{{'子'|ruby:'こ'}}<span class="orange">が</span>います。
(Exists a boy.)

A2: <span class="red">{{'誰'|ruby:'だれ'}}も</span>い<span class="orange">ません</span>。
(<span class="orange">Doesn't</span> exist <span class="red">anyone</span>.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'部'|ruby:'へ'}}{{'屋'|ruby:'や'}}に<span class="red">{{'何'|ruby:'なに'}}</span>がいますか。
(<span class="red">What (animal)</span> exist in the room?)

A: {{'猫'|ruby:'ねこ'}}<span class="orange">が</span>います。
(Exists a cat.)

</blockquote>

{{'椅'|ruby:'い'}}{{'子'|ruby:'す'}}<span class="orange">の</span>{{'上'|ruby:'うえ'}}<span class="red">に</span>{{'猫'|ruby:'ねこ'}}がいます。
(Lit: <span class="red">At</span> the chair<span class="orange">'s</span> above exists a cat.)
(There is a cat <span class="red">on</span> top <span class="orange">of</span> the chair.)

<blockquote class="blockquote-qna">

Q: テーブルの{{'上'|ruby:'うえ'}}<span class="orange">に</span><span class="red">{{'何'|ruby:'なに'}}</span><span class="blue">が</span>ありますか。
(<span class="red">What (thing)</span> exist <span class="orange">at</span> the top of the table?)

A: コーヒー<span class="blue">が</span>あります。
(Exists a coffee.)

</blockquote>

{{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}<span class="orange">は</span>{{'頭'|ruby:'あたま'}}の{{'上'|ruby:'うえ'}}<span class="red">に</span>あります。
(Eyeglasses exist <span class="red">at</span> the top of the head (of a person).)

(Rule: thing <span class="orange">は</span> place <span class="red">に</span>あります)

<blockquote class="blockquote-qna">

Q: {{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}は<span class="red">どこ</span><span class="orange">に</span>ありますか。
(<span class="orange">(At)</span> <span class="red">Where</span> exist the eyeglasses?)

A: {{'頭'|ruby:'あたま'}}の{{'上'|ruby:'うえ'}}<span class="orange">に</span>あります。
(Exists <span class="orange">at</span> the top of the head (of a person).)

</blockquote>

{{'犬'|ruby:'いぬ'}}<span class="orange">は</span>ベッドの{{'下'|ruby:'した'}}<span class="red">に</span>います。
(A dog exist <span class="red">at</span> below the bed.)

(Rule: person/animal <span class="orange">は</span> place <span class="red">に</span>います)

<blockquote class="blockquote-qna">

Q: {{'犬'|ruby:'いぬ'}}は<span class="red">どこ</span><span class="orange">に</span>いますか。
(<span class="orange">(At)</span> <span class="red">Where</span> exist the dog?)

A: ベッドの{{'下'|ruby:'した'}}<span class="orange">に</span>います。
(Exists <span class="orange">at</span> below the bed.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'棚'|ruby:'たな'}}に{{'何'|ruby:'なに'}}がありますか。
(What (thing) exist at the shelf?)

A: {{'本'|ruby:'ほん'}}<span class="red">や</span>{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}<span class="orange">など</span>があります。
(Exists a book, a clock, <span class="orange">and so on</span>.)

</blockquote>

## Counting {#counting}

{{5|jlpt}} {{'E01.L11'|icls}}

スーパー<span class="blue">に</span>りんご<span class="orange">が</span><span class="red">{{'一'|ruby:'ひと'}}つ</span>あります。
(<span class="blue">At</span> the supermarket exists <span class="red">1</span> apple.)

<blockquote class="blockquote-qna">

Q: りんごが<span class="red">いくつ</span>ありますか。
(<span class="red">How many</span> apples exist?)

A: <span class="red">{{'一'|ruby:'ひと'}}つ</span>あります。
(Exists <span class="red">1</span>.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'車'|ruby:'くるま'}}が<span class="red">{{'何'|ruby:'なん'}}{{'台'|ruby:'だい'}}</span>ありますか。
(<span class="red">How many</span> cars exist?)

A: <span class="red">{{'一'|ruby:'ひと'}}{{'台'|ruby:'だい'}}</span>あります。
(Exists <span class="red">1</span>.)

</blockquote>

りんご<span class="orange">を</span>{{'一'|ruby:'ひと'}}つ{{'買'|ruby:'か'}}います。
(Buy 1 apple.)

<blockquote class="blockquote-qna">

Q: りんごを<span class="red">いくつ</span>{{'買'|ruby:'か'}}いますか。
(<span class="red">How many</span> apples to buy?)

A: <span class="red">{{'七'|ruby:'なな'}}つ</span>{{'買'|ruby:'か'}}います。
(Buy <span class="red">7</span>.)

</blockquote>

{{'果'|ruby:'くだ'}}{{'物'|ruby:'もの'}}は<span class="red">{{'全'|ruby:'ぜん'}}{{'部'|ruby:'ぶ'}}で</span>{{'八'|ruby:'やっ'}}つです。
(The fruits are 8 <span class="red">in total</span>.)

{{'果'|ruby:'くだ'}}{{'物'|ruby:'もの'}}は<span class="red">{{'全'|ruby:'ぜん'}}{{'部'|ruby:'ぶ'}}で</span>５５０{{'円'|ruby:'えん'}}です。
(The fruits are (priced) 550 yen <span class="red">in total</span>.)

{{'私'|ruby:'わたし'}}は{{'一'|ruby:'ひと'}}つ<span class="red">だけ</span>{{'買'|ruby:'か'}}いました。
(I bought <span class="red">only</span> 1.)

{{'私'|ruby:'わたし'}}は{{'黒'|ruby:'くろ'}}い{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}<span class="red">だけ</span>{{'買'|ruby:'か'}}いました。
(I bought <span class="red">only</span> the black watch.)

{{'私'|ruby:'わたし'}}は{{'日'|ruby:'にち'}}{{'曜'|ruby:'よう'}}{{'日'|ruby:'び'}}<span class="red">だけ</span>{{'休'|ruby:'やす'}}みです。
(I rest <span class="red">only</span> on Sunday.)

(Rule: noun <span class="red">だけ</span>)

{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}に{{'学'|ruby:'がく'}}{{'生'|ruby:'せい'}}が{{'十'|ruby:'じゅう'}}{{'人'|ruby:'にん'}}<span class="orange">います</span>。
(At the classroom <span class="orange">exists</span> 10 students.)

<blockquote class="blockquote-qna">

Q: {{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}の{{'人'|ruby:'ひと'}}が<span class="red">{{'何'|ruby:'なん'}}{{'人'|ruby:'にん'}}</span><span class="orange">います</span>か。
(<span class="red">How many (people)</span> people with eyeglasses <span class="orange">exist</span>?)

A: <span class="red">{{'四'|ruby:'よ'}}{{'人'|ruby:'にん'}}</span><span class="orange">います</span>。
(<span class="orange">Exists</span> <span class="red">4</span> (such) people.)

</blockquote>

{{'私'|ruby:'わたし'}}の{{'家'|ruby:'か'}}{{'族'|ruby:'ぞく'}}は{{'五'|ruby:'ご'}}{{'人'|ruby:'にん'}}です。
{{'両'|ruby:'りょう'}}{{'親'|ruby:'しん'}}<span class="red">と</span>
{{'弟'|ruby:'おとうと'}}<span class="orange">が</span>{{'一'|ruby:'ひと'}}{{'人'|ruby:'り'}}<span class="red">と</span>、
{{'妹'|ruby:'いもうと'}}<span class="orange">が</span>{{'一'|ruby:'ひと'}}{{'人'|ruby:'り'}}います。
(My family has 5 people: Parents <span class="red">(and)</span>, 1 brother <span class="red">(and)</span>, 1 sister (exist).)

{{'一'|ruby:'いち'}}{{'日'|ruby:'にち'}}<span class="red">に</span><span class="orange">{{'三'|ruby:'さん'}}{{'回'|ruby:'かい'}}</span>ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べます。
(<span class="red">Within</span> 1 day, I eat meal <span class="orange">3 times</span>.)

(Rule: duration <span class="red">に</span> <span class="orange">frequency</span> action)

<blockquote class="blockquote-qna">

Q: {{'一'|ruby:'いっ'}}か{{'月'|ruby:'げつ'}}に<span class="red">{{'何'|ruby:'なん'}}{{'回'|ruby:'かい'}}</span>{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'見'|ruby:'み'}}ますか。
(Within 1 month, <span class="red">how many times</span> do you watch movie?)

A: <span class="red">{{'二'|ruby:'に'}}{{'回'|ruby:'かい'}}</span>{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'見'|ruby:'み'}}ます。
((Within that duration) I watch movie <span class="red">2 times</span>.)

</blockquote>

{{'一'|ruby:'いっ'}}か{{'月'|ruby:'げつ'}}に{{'三'|ruby:'さん'}}{{'回'|ruby:'かい'}}<span class="red">ぐらい</span>デパートへ{{'行'|ruby:'い'}}きます。
(Within 1 month, I go to the department store <span class="red">about</span> 3 times.)

<span class="red">{{'一'|ruby:'いっ'}}{{'回'|ruby:'かい'}}も</span>{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}をし<span class="red">ません</span>でした。
(Lit: I did trips <span class="red">not even once</span>.)
(I <span class="red">never</span> went to trips.)

<blockquote class="blockquote-qna">

Q: {{'一'|ruby:'いっ'}}か{{'月'|ruby:'げつ'}}に{{'何'|ruby:'なん'}}{{'回'|ruby:'かい'}}{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}とご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べますか。
(Within 1 month, how many times do you eat meal with friend?)

A: {{'四'|ruby:'よん'}}{{'回'|ruby:'かい'}}<span class="orange">ぐらい</span>{{'食'|ruby:'た'}}べます。
(Eat <span class="orange">about</span> 4 times.)

</blockquote>

## Duration {#duration}

{{5|jlpt}} {{'E01.L11'|icls}}

{{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}<span class="orange">７{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}</span>{{'寝'|ruby:'ね'}}ます。
(Every day I sleep for <span class="orange">7 hours</span>.)

<span class="orange">{{'三'|ruby:'みっ'}}{{'日'|ruby:'か'}}</span>{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}を{{'休'|ruby:'やす'}}みます。
(I rest from (my work) company for <span class="orange">3 days</span>.)

<span class="orange">{{'四'|ruby:'よん'}}か{{'月'|ruby:'げつ'}}</span>{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}しました。
(I studied Japanese language for <span class="orange">4 months</span>.)

<blockquote class="blockquote-qna">

Q1: {{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}<span class="red">{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}</span>{{'寝'|ruby:'ね'}}ますか。
(<span class="red">How many hours</span> do you sleep?)

Q2: {{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}<span class="red">どのくらい</span>{{'寝'|ruby:'ね'}}ますか。
(<span class="red">How long (duration)</span> do you sleep?)

A: <span class="red">６{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}</span>{{'寝'|ruby:'ね'}}ます。
(Sleep for <span class="red">6 hours</span>.)

</blockquote>

{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="orange">から</span>タイ<span class="orange">まで</span>６{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}かかります。
(It takes 6 hours to go <span class="orange">from</span> Japan <span class="orange">to</span> Thailand.)

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="orange">から</span>タイ<span class="orange">まで</span><span class="red">どのくらい</span>かかりますか。
<span class="red">How long (duration)</span> does it take to go <span class="orange">from</span> Japan <span class="orange">to</span> Thailand?)

A: ６{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}ぐらいかかります。
(It takes about 6 hours.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'家'|ruby:'うち'}}<span class="orange">から</span>{{'会'|ruby:'かい'}}{{'社'|ruby:'しゃ'}}<span class="orange">まで</span>{{'車'|ruby:'くるま'}}<span class="red">で</span>どのくらいかかりますか。
(How long (duration) does it take to go <span class="orange">from</span> (your) house <span class="orange">to</span> (your) company <span class="red">by</span> car?)

A: {{'十'|ruby:'じゅう'}}{{'五'|ruby:'ご'}}{{'分'|ruby:'ふん'}}かかります。
(It takes 15 minutes.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="orange">から</span>タイ<span class="orange">まで</span><span class="red">いくら</span>かかりますか。
<span class="red">How much (worth)</span> does it cost to go <span class="orange">from</span> Japan <span class="orange">to</span> Thailand?)

A: ６００００{{'円'|ruby:'えん'}}ぐらいかかります。
(It costs about 60,000 yen.)

</blockquote>

## Adjective past tense {#adjective-past-tense}

{{5|jlpt}} {{'E01.L12'|icls}}

Type | Rule | Present | Past
-- | -- | -- | --
Positive, noun|でした|{{'雨'|ruby:'あめ'}}<span class="blue">です</span>|{{'雨'|ruby:'あめ'}}<span class="red">でした</span>
Positive, 「な」adjective|でした|{{'暇'|ruby:'ひま'}}<span class="blue">です</span>|{{'暇'|ruby:'ひま'}}<span class="red">でした</span>
Positive, 「い」adjective|かったです|{{'暑'|ruby:'あつ'}}<span class="blue">い</span>です|{{'暑'|ruby:'あつ'}}<span class="red">かった</span>です
Positive, いい（{{'良'|ruby:'よ'}}い）|よかったです|<span class="blue">いい</span>です|<span class="blue">よかった</span>です
Negative, noun|じゃありませんでした|{{'雨'|ruby:'あめ'}}じゃありません|{{'雨'|ruby:'あめ'}}じゃありません<span class="red">でした</span>
Negative, 「な」adjective|じゃありませんでした|{{'暇'|ruby:'ひま'}}じゃありません|{{'暇'|ruby:'ひま'}}じゃありません<span class="red">でした</span>
Negative, 「い」adjective|くなかったです|{{'暑'|ruby:'あつ'}}<span class="blue">くない</span>です|{{'暑'|ruby:'あつ'}}<span class="red">くなかった</span>です
Negative, いい（{{'良'|ruby:'よ'}}い）|よくなかったです|よ<span class="blue">くない</span>です|よ<span class="red">くなかった</span>です

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: ビギナーは<span class="red">どうでした</span>か。
(<span class="red">How was</span> Beginner (class)?)

A: {{'楽'|ruby:'たの'}}し<span class="red">かった</span>です、そして{{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}<span class="red">かった</span>です。
(It <span class="red">was</span> enjoyable, and it <span class="red">was</span> interesting.)

</blockquote>

## Compared to {#compared-to}

{{5|jlpt}} {{'E01.L12'|icls}}

マレーシアはシンガポール<span class="red">より</span>{{'大'|ruby:'おお'}}きいです。
(Malaysia is big <span class="red">(in fact) compared to</span> Singapore.)

シンガポールはマレーシア<span class="red">より</span>{{'小'|ruby:'ちい'}}さいです。
(Singapore is small <span class="red">(in fact) compared to</span> Malaysia.)

(Rule: A は B <span class="red">より</span> adjective)

{{'中'|ruby:'ちゅう'}}{{'国'|ruby:'ごく'}}は{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}<span class="red">より</span><span class="orange">ずっと</span>{{'大'|ruby:'おお'}}きいです。
(China is <span class="orange">by far (much)</span> big <span class="red">(in fact) compared to</span> to Japan.)

<blockquote class="blockquote-qna">

Q: ［あなたは］サッカー<span class="orange">と</span>テニス<span class="orange">と</span><span class="red">どちらが</span>{{'好'|ruby:'す'}}きですか。
(<span class="orange">Between</span> soccer <span class="orange">and</span> tennis, <span class="red">which</span> do you like?)

A1: ［{{'私'|ruby:'わたし'}}は］テニス<span class="red">のほうが</span>{{'好'|ruby:'す'}}きです。
(I like <span class="red">(the side of)</span> tennis.)

A2: <span class="red">どちらも</span>{{'好'|ruby:'す'}}きです。
(I like <span class="red">both</span>.)

</blockquote>

Heat level | Adjective | Season
-- | -- | --
+2|{{'暑'|ruby:'あつ'}}い (hot)|{{'夏'|ruby:'なつ'}} (summer)
+1|{{'暖'|ruby:'あたた'}}かい (warm)|{{'春'|ruby:'はる'}} (spring)
-1|{{'涼'|ruby:'すず'}}しい (cool)|{{'秋'|ruby:'あき'}} (autumn)
-2|{{'寒'|ruby:'さむ'}}い (cold)|{{'冬'|ruby:'ふゆ'}} (winter)

{.table .table-hover .table-sm .align-text-bottom}

{{'一'|ruby:'いち'}}{{'年'|ruby:'ねん'}}<span class="red">で</span>{{'八'|ruby:'はち'}}{{'月'|ruby:'がつ'}}<span class="blue">が</span><span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'暑'|ruby:'あつ'}}いです。
(<span class="red">Within</span> 1 year, August is <span class="orange">the most</span> hot.)

(Rule: category <span class="red">で</span> ~ <span class="blue">が</span><span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span> adjective)

<blockquote class="blockquote-qna">

Q: {{'一'|ruby:'いち'}}{{'年'|ruby:'ねん'}}で<span class="red">いつ</span>が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'寒'|ruby:'さむ'}}いですか。
(Within 1 year, <span class="red">when</span> is <span class="orange">the most</span> cold?)

A: {{'一'|ruby:'いち'}}{{'月'|ruby:'がつ'}}が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'寒'|ruby:'さむ'}}いです。
(January is <span class="orange">the most</span> cold.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'料'|ruby:'りょう'}}{{'理'|ruby:'り'}}で<span class="red">{{'何'|ruby:'なに'}}</span>が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'好'|ruby:'す'}}きですか。
(Within Japanese cuisine, <span class="red">what</span> do you like <span class="orange">the most</span>?)

A: {{'刺'|ruby:'さし'}}{{'身'|ruby:'み'}}が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'好'|ruby:'す'}}きです。
(I like sashimi <span class="orange">the most</span>.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}で<span class="red">どこ</span>が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'大'|ruby:'おお'}}きいですか。
(Within Japan, <span class="red">where</span> is <span class="orange">the most</span> big?)

A: {{'北'|ruby:'ほっ'}}{{'海'|ruby:'かい'}}{{'道'|ruby:'どう'}}が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'大'|ruby:'おお'}}きいです。
(Hokkaido is <span class="orange">the most</span> big.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'兄'|ruby:'きょう'}}{{'弟'|ruby:'だい'}}で<span class="red">{{'誰'|ruby:'だれ'}}</span>が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'背'|ruby:'せ'}}が{{'高'|ruby:'たか'}}いですか。
(Within siblings, <span class="red">who</span> has <span class="orange">the most</span> height?)

A: {{'私'|ruby:'わたし'}}が<span class="orange">{{'一'|ruby:'いち'}}{{'番'|ruby:'ばん'}}</span>{{'背'|ruby:'せ'}}が{{'高'|ruby:'たか'}}いです。
(I have <span class="orange">the most</span> height.)

</blockquote>

## Desire {#desire}

{{5|jlpt}} {{'E01.L13'|icls}}

お{{'金'|ruby:'かね'}}が<span class="red">{{'欲'|ruby:'ほ'}}しい</span>です。
(I <span class="red">want</span> money.)

(Rule: noun が<span class="red">{{'欲'|ruby:'ほ'}}しい</span>)

{{'忙'|ruby:'いそが'}}しいですから、{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}が<span class="red">{{'欲'|ruby:'ほ'}}しい</span>です。
(Because (I am) busy, I <span class="red">want</span> (more) time.)

<blockquote class="blockquote-qna">

Q: <span class="red">{{'何'|ruby:'なに'}}</span>が{{'欲'|ruby:'ほ'}}しいですか。
(<span class="red">What</span> do you want?)

A: {{'新'|ruby:'あたら'}}しい{{'靴'|ruby:'くつ'}}が{'欲'|ruby:'ほ'}}しいです。
(I want new shoes.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どんな</span>{{'猫'|ruby:'ねこ'}}が{{'欲'|ruby:'ほ'}}しいですか。
(<span class="red">What kind of</span> cat do you want?)

A: {{'白'|ruby:'しろ'}}い{{'猫'|ruby:'ねこ'}}が{{'欲'|ruby:'ほ'}}しいです。
(I want white cat.)

</blockquote>

テニスをし<span class="blue">ます</span>。(I play tennis.)

テニスをし<span class="red">たいです</span>。
(I <span class="red">want to</span> play tennis.)

テニスをし<span class="red">たくないです</span>。
(I <span class="red">don't want to</span> play tennis.)

<blockquote class="blockquote-qna">

Q: <span class="orange">{{'何'|ruby:'なに'}}</span>を{{'食'|ruby:'た'}}べたいですか。
(<span class="orange">What</span> do you want to eat?)

A: カレーを{{'食'|ruby:'た'}}べたいです。
(I want to eat curry.)

</blockquote>

## Purpose {{'目'|ruby:'もく'}}{{'的'|ruby:'てき'}} {#purpose}

{{5|jlpt}} {{'E01.L13'|icls}}

デパート<span class="orange">へ{{'行'|ruby:'い'}}きます</span>。{{'靴'|ruby:'くつ'}}を{{'買'|ruby:'か'}}い<span class="blue">ます</span>。
(I <span class="orange">go to</span> the department store. I buy shoes.)

デパート<span class="orange">へ</span>{{'靴'|ruby:'くつ'}}を{{'買'|ruby:'か'}}い<span class="red">に</span><span class="orange">{{'行'|ruby:'い'}}きます</span>。
(I <span class="orange">go to</span> the department store <span class="red">to</span> buy shoes.)

(Rule: location へ purpose <span class="red">に</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
To use a verb as the "purpose", use its polite form and remove the trailing <span class="blue">ます</span>.
</div>

{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}<span class="blue">し</span>に{{'行'|ruby:'い'}}きます。
(I go to school to <span class="blue">do</span> study. (purpose is a verb))

{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}に{{'行'|ruby:'い'}}きます。
(I go to school to study. (purpose is a noun))

<blockquote class="blockquote-qna">

Q: <span class="red">どこ</span>へ{{'靴'|ruby:'くつ'}}を{{'買'|ruby:'か'}}いに{{'行'|ruby:'い'}}きますか。
(<span class="red">Where</span> do you go to buy shoes?)

A: デパートへ{{'買'|ruby:'か'}}いに{{'行'|ruby:'い'}}きます。
(I go to the department store to buy.)

</blockquote>

<blockquote class="blockquote-qna">

Q: デパートへ<span class="red">{{'何'|ruby:'なに'}}</span>を{{'買'|ruby:'か'}}いに{{'行'|ruby:'い'}}きますか。
(<span class="red">What</span> do you go to the department store to buy?)

A: {{'靴'|ruby:'くつ'}}を{{'買'|ruby:'か'}}いに{{'行'|ruby:'い'}}きます。
(I go to buy shoes.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">いつ</span>{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}の{{'家'|ruby:'うち'}}へゲームに{{'行'|ruby:'い'}}きますか。
(<span class="red">When</span> do you go to friend's house to play games?)

A: {{'明日'|ruby:'あした'}}ゲームに{{'行'|ruby:'い'}}きます。
(I go to play games tomorrow.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">{{'誰'|ruby:'だれ'}}</span>とデパートへご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べに{{'行'|ruby:'い'}}きますか。
(<span class="red">Who</span> do you go to the department store to eat meal with?)

A: {{'家'|ruby:'か'}}{{'族'|ruby:'ぞく'}}と{{'食'|ruby:'た'}}べに{{'行'|ruby:'い'}}きます。
(I go to eat with family.)

</blockquote>

## Please {#please}

{{5|jlpt}} {{'E01.L14'|icls}}

Group | Rule | ます形 (masu-form) | て形 (te-form)
-- | -- | -- | --
1 (五段)|います➝って|{{'吸'|ruby:'す'}}<span class="blue">います</span>|{{'吸'|ruby:'す'}}<span class="red">って</span>
1 (五段)|ちます➝って|{{'待'|ruby:'ま'}}<span class="blue">ちます</span>|{{'待'|ruby:'ま'}}<span class="red">って</span>
1 (五段)|ります➝って|{{'帰'|ruby:'かえ'}}<span class="blue">ります</span>|{{'帰'|ruby:'かえ'}}<span class="red">って</span>
1 (五段)|みます➝んで|{{'飲'|ruby:'の'}}<span class="blue">みます</span>|{{'飲'|ruby:'の'}}<span class="red">んで</span>
1 (五段)|びます➝んで|{{'呼'|ruby:'よ'}}<span class="blue">びます</span>|{{'呼'|ruby:'よ'}}<span class="red">んで</span>
1 (五段)|きます➝いて|{{'書'|ruby:'か'}}<span class="blue">きます</span>|{{'書'|ruby:'か'}}<span class="red">いて</span>
1 (五段)|ぎます➝いで|{{'急'|ruby:'いそ'}}<span class="blue">ぎます</span>|{{'急'|ruby:'いそ'}}<span class="red">いで</span>
1 (五段)|します➝して|{{'話'|ruby:'はな'}}<span class="blue">します</span>|{{'話'|ruby:'はな'}}<span class="red">して</span>
1 (五段)|-|{{'行'|ruby:'い'}}<span class="blue">きます</span>|{{'行'|ruby:'い'}}<span class="red">って</span>
2 (一段)|ます➝て|{{'食'|ruby:'た'}}べ<span class="blue">ます</span>|{{'食'|ruby:'た'}}べ<span class="red">て</span>
2 (一段)|ます➝て|{{'寝'|ruby:'ね'}}<span class="blue">ます</span>|{{'寝'|ruby:'ね'}}<span class="red">て</span>
2 (一段)|ます➝て|{{'見'|ruby:'み'}}<span class="blue">ます</span>|{{'見'|ruby:'み'}}<span class="red">て</span>
2 (一段)|ます➝て|{{'借'|ruby:'か'}}り<span class="blue">ます</span>|{{'借'|ruby:'か'}}り<span class="red">て</span>
3 (カ)|{{'来'|ruby:'き'}}ます➝{{'来'|ruby:'き'}}て|{{'来'|ruby:'き'}}<span class="blue">ます</span>|{{'来'|ruby:'き'}}<span class="red">て</span>
3 (サ)|します➝して|し<span class="blue">ます</span>|し<span class="red">て</span>
3 (サ)|します➝して|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="blue">ます</span>|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="red">て</span>

{.table .table-hover .table-sm .align-text-bottom}

{{'来'|ruby:'き'}}ます。お{{'願'|ruby:'ねが'}}いします。
(Please come.)

{{'来'|ruby:'き'}}て<span class="red">ください</span>。
(<span class="red">Please</span> come.)

(Rule: verb て形 <span class="red">ください</span>)

<span class="blue">すみませんが</span>、［ちょっと］<span class="orange">{{'道'|ruby:'みち'}}</span>を{{'教'|ruby:'おし'}}えて<span class="red">ください</span>。
(<span class="blue">Excuse me but</span>, (could you) <span class="red">please</span> tell me <span class="orange">the way</span>.)

<span class="blue">どうぞ</span>、{{'座'|ruby:'すわ'}}って<span class="red">ください</span>。
(<span class="red">Please</span> <span class="blue">(feel free to)</span> sit down.)

{{'荷'|ruby:'に'}}{{'物'|ruby:'もつ'}}を{{'持'|ruby:'も'}}ち<span class="red">ましょうか</span>。
(<span class="red">Do you want (me)</span> to hold the luggage?)

(Rule: Verb ます形 <span class="text-decoration-line-through blue">ます</span>➝<span class="red">ましょうか</span>)

<blockquote class="blockquote-qna">

Q: {{'窓'|ruby:'まど'}}を{{'閉'|ruby:'し'}}め<span class="red">ましょうか</span>。
(<span class="red">Do you want (me)</span> to close the window?)

A1: ええ、お{{'願'|ruby:'ねが'}}いします。
(Yes, please.)

A2: いいえ、{{'結'|ruby:'けっ'}}{{'構'|ruby:'こう'}}です。
(No, I'm fine.)

A3: いいえ、{{'大'|ruby:'だい'}}{{'丈'|ruby:'じょう'}}{{'夫'|ruby:'ぶ'}}です。
(No, I'm good. (more polite))

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'手'|ruby:'て'}}{{'伝'|ruby:'つだ'}}い<span class="red">ましょうか</span>。
(<span class="red">Do you want (me)</span> to help?)

A: <span class="orange">ありがとうございます</span>。［ちょっと、］これを{{'持'|ruby:'も'}}って<span class="red">ください</span>。
(<span class="orange">Thank you</span>. (Could you) <span class="red">Please</span> hold this.)

</blockquote>

<blockquote class="blockquote-qna">

Q: エアコンを{{'消'|ruby:'け'}}し<span class="red">ましょうか</span>。
(<span class="red">Do you want (me)</span> to turn off the air conditioner?)

A: <span class="orange">そうですね</span>。{{'一'|ruby:'いち'}}{{'度'|ruby:'ど'}}{{'消'|ruby:'け'}}して<span class="red">ください</span>。
(<span class="orange">Is that so (I feel the same too)</span>. <span class="red">Please</span> turn it off at once.)

</blockquote>

## Doing {#doing}

{{5|jlpt}} {{'E01.L14'|icls}}

［{{'今'|ruby:'いま'}}、］カレーを{{'食'|ruby:'た'}}べて<span class="red">います</span>。
((Now, ) I eat<span class="red">ing</span> curry.)

(Rule: ［{{'今'|ruby:'いま'}}］ verb て形 <span class="red">います</span>)

<blockquote class="blockquote-qna">

Q: おばあちゃんは<span class="red">{{'何'|ruby:'なに'}}</span>をして<span class="orange">います</span>か。
(<span class="red">What</span> is your grandmother do<span class="orange">ing</span>?)

A: {{'絵'|ruby:'え'}}を{{'書'|ruby:'か'}}いて<span class="orange">います</span>。
(Draw<span class="orange">ing</span> a picture.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'佐'|ruby:'さ'}}{{'久'|ruby:'く'}}{{'間'|ruby:'ま'}}さんは<span class="red">{{'誰'|ruby:'だれ'}}</span>と{{'話'|ruby:'はな'}}し<span class="orange">います</span>か。
(<span class="red">Who</span> is Sakuma talk<span class="orange">ing</span> to?)

A: {{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんと{{'話'|ruby:'はな'}}し<span class="orange">います</span>。
(Talk<span class="orange">ing</span> to Tanaka.)

</blockquote>

## Permission {#permission}

{{5|jlpt}} {{'E01.L15'|icls}}

{{'窓'|ruby:'まど'}}を{{'開'|ruby:'あ'}}けます。いいですか。
(Is it ok to open the window?)

{{'窓'|ruby:'まど'}}を{{'開'|ruby:'あ'}}けて<span class="red">も</span>いいですか。
(Is it ok to open the window?)

(Rule: verb て形 <span class="red">も</span> いいですか)

ここに{{'座'|ruby:'すわ'}}って<span class="red">も</span>いいですか。
(Is it ok to sit here?)

ここで{{'写'|ruby:'しゃ'}}{{'真'|ruby:'しん'}}を{{'撮'|ruby:'と'}}って<span class="red">も</span>いいですか。
(Is it ok to take photo here?)

{{'家'|ruby:'うち'}}へ{{'帰'|ruby:'かえ'}}って<span class="red">も</span>いいですか。
(Is it ok to return home?)

<blockquote class="blockquote-qna">

Q: ．．．<span class="red">も</span>いいですか。
(It is ok to ...?)

A1: いい、いいですよ。どうぞ。
(Yes, it's ok. Go ahead.)

A2: すみません。ちょっと．．．。
(Sorry, that's a little bit... (rejection))

A3: だめです。
(Cannot. (used to enforce a social/legal rule ({{'社'|ruby:'しゃ'}}{{'会'|ruby:'かい'}}のルール)))

A4: いけません。
(Must not. (used to enforce a social/legal rule ({{'社'|ruby:'しゃ'}}{{'会'|ruby:'かい'}}のルール)))

</blockquote>

たばこを{{'吸'|ruby:'す'}}います。だめです。
(Cannot smoke. (social rule))

たばこを{{'吸'|ruby:'す'}}って<span class="red">は</span>いけません。
(Cannot smoke. (social rule))

(Rule: verb て形 <span class="red">は</span> いけません)

ここで{{'泳'|ruby:'およ'}}いで<span class="red">は</span>いけません。
(Cannot swim here.)

{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'撮'|ruby:'と'}}って<span class="red">は</span>いけません。
(Cannot take photo of the movie.)

{{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}で{{'食'|ruby:'た'}}べ{{'物'|ruby:'もの'}}を{{'食'|ruby:'た'}}べて<span class="red">は</span>いけません。
(Cannot eat food at the train.)

## Present perfect tense {#present-perfect-tense}

{{5|jlpt}} {{'E01.L15'|icls}}

{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんは{{'去'|ruby:'きょ'}}{{'年'|ruby:'ねん'}}の{{'八'|ruby:'はち'}}{{'月'|ruby:'がつ'}}に{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}しました。
(Tanaka is married at August last year.)

［ずっと］{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんは{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}して<span class="red">います</span>。
(Lit: (All the while) Tanaka is continu<span class="red">ing</span> to marry.)
(Tanaka has married.)

(Rule: ［ずっと］ verb て形 <span class="red">います</span>)

<blockquote class="blockquote-qna">

Q: {{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}{{'一'|ruby:'いち'}}{{'郎'|ruby:'ろう'}}さんは{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}して<span class="red">います</span>か。
(Lit: Is Yamada Ichiro continu<span class="red">ing</span> to marry?)
(Has Yamada Ichiro married?)

A1: はい、{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}して<span class="red">います</span>。
(Yes, he has married.)

A2: いいえ、{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}して<span class="red">いません</span>。
(No, he has not married.)

A3: いいえ、{{'独'|ruby:'どく'}}{{'身'|ruby:'しん'}}です。
(No, he is single.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="orange">どこ</span>に{{'住'|ruby:'す'}}んで<span class="red">います</span>か。
(<span class="orange">Where</span> do you live at?)

A: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}に{{'住'|ruby:'す'}}んで<span class="red">います</span>。
(I has lived in Japan.)

</blockquote>

{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんはお{{'金'|ruby:'かね'}}が<span class="blue">あります</span>。
(Tanaka <span class="blue">has</span> money.)

{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんはお{{'金'|ruby:'かね'}}を<span class="red">{{'持'|ruby:'も'}}っています</span>。
(Tanaka <span class="red">has</span> money.)

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
<span class="red">{{'持'|ruby:'も'}}っています</span> cannot be used for the following:

- {{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}} (time)
- {{'仕'|ruby:'し'}}{{'事'|ruby:'ごと'}} (work, business)
- {{'約'|ruby:'やく'}}{{'束'|ruby:'そく'}} (appointment, promise)
- {{'用'|ruby:'よう'}}{{'事'|ruby:'じ'}} (errand)
- アルバイト (side job)
</div>

<blockquote class="blockquote-qna">

Q: {{'佐'|ruby:'さ'}}{{'藤'|ruby:'とう'}}さんは{{'車'|ruby:'くるま'}}を<span class="red">{{'持'|ruby:'も'}}っています</span>か。
(Does Tanaka <span class="red">have</span> a car?)

A1: はい、<span class="red">{{'持'|ruby:'も'}}っています</span>。
(Yes, he <span class="red">have</span>.)

A2: いいえ、<span class="red">{{'持'|ruby:'も'}}っていません</span>。
(No, he <span class="red">do not have</span>.)

</blockquote>

みんなドラえもんを<span class="red">{{'知'|ruby:'し'}}っています</span>。
(Everyone <span class="red">knows (has known)</span> Doraemon.)

<blockquote class="blockquote-qna">

Q: このキャラクターを<span class="red">{{'知'|ruby:'し'}}っています</span>か。
(Do you <span class="red">know</span> this character?)

A1: はい、<span class="red">{{'知'|ruby:'し'}}っています</span>。
(Yes, I <span class="red">know (have known)</span>.)

A2: いいえ、<span class="red">{{'知'|ruby:'し'}}りません</span>。
(No, I <span class="red">do not know</span>.)

</blockquote>

## Continuous tense {#continuous-tense}

{{5|jlpt}} {{'E01.L15'|icls}}

アップルは［いつも］パソコンを{{'作'|ruby:'つく'}}って<span class="red">います</span>。
(Apple (usually) <span class="red">has been</span> produc<span class="red">ing</span> personal computers.)

(Rule: ［いつも］ verb て形 <span class="red">います</span>)

<blockquote class="blockquote-qna">

Q: アップルは<span class="red">{{'何'|ruby:'なに'}}</span>を{{'作'|ruby:'つく'}}って<span class="orange">います</span>か。
(<span class="red">What</span> <span class="orange">have been</span> Apple produc<span class="orange">ing</span>?)

A: パソコンを{{'作'|ruby:'つく'}}って<span class="orange">います</span>。
(It <span class="orange">has been</span> produc<span class="orange">ing</span> personal computers.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どこ</span>でアップルのパソコンを{{'売'|ruby:'う'}}って<span class="orange">います</span>か。
(<span class="red">Where</span> <span class="orange">have been</span> sell<span class="orange">ing</span> Apple computers?)

A: {{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}{{'屋'|ruby:'や'}}で{{'売'|ruby:'う'}}って<span class="orange">います</span>。
(The electronic shop <span class="orange">has been</span> sell<span class="orange">ing</span> them.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どこの／{{'何'|ruby:'なん'}}の</span>パソコンを{{'使'|ruby:'つか'}}って<span class="orange">います</span>か。
(<span class="red">What</span> computer <span class="orange">have been</span> you us<span class="orange">ing</span>?)

A: アップルのパソコンを{{'使'|ruby:'つか'}}って<span class="orange">います</span>。
(I <span class="orange">have been</span> us<span class="orange">ing</span> Apple computer.)

</blockquote>

{{'私'|ruby:'わたし'}}は{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}で{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'教'|ruby:'おし'}}えて<span class="orange">います</span>。
(I <span class="orange">have been</span> teach<span class="orange">ing</span> Japanese language at university.)

{{'私'|ruby:'わたし'}}は{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}で{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'習'|ruby:'なら'}}って<span class="orange">います</span>。
(I <span class="orange">have been</span> learn<span class="orange">ing</span> Japanese language at university.)

{{'私'|ruby:'わたし'}}は{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}で{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}して<span class="orange">います</span>。
(I <span class="orange">have been</span> study<span class="orange">ing</span> Japanese language at university.)

Type | Meaning | Example
-- | -- | --
{{'今'|ruby:'いま'}}|Currently doing|{{'食'|ruby:'た'}}べて<span class="red">います</span> (eating)
ずっと|Continuing since the past|{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}して<span class="red">います</span> (has married)
いつも|Always have been doing|{{'売'|ruby:'う'}}って<span class="red">います</span> (have been selling)

{.table .table-hover .table-sm .align-text-bottom}

## Sequential actions {#sequential-actions}

{{5|jlpt}} {{'E01.L16'|icls}}

{{'私'|ruby:'わたし'}}は{{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}
７{{'時'|ruby:'じ'}}に{{'起'|ruby:'お'}}き<span class="red">て</span>、
{{'朝'|ruby:'あさ'}}ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べ<span class="red">て</span>、
［それから］{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}します。
(Every day I woke up at 7 o' clock, (then) eat breakfast, (and then) study.)

(Rule: verb て形、verb て形、［それから］verb)

{{'空'|ruby:'くう'}}{{'港'|ruby:'こう'}}までタクシーで{{'行'|ruby:'い'}}っ<span class="red">て</span>、
１０{{'時'|ruby:'じ'}}の{{'飛'|ruby:'ひ'}}{{'行'|ruby:'こう'}}{{'機'|ruby:'き'}}に{{'乗'|ruby:'の'}}ります。
(Go to airport by taxi, (and then) ride the 10 o' clock's airplane.)

{{'昨日'|ruby:'きのう'}}、{{'家'|ruby:'うち'}}へ{{'帰'|ruby:'かえ'}}っ<span class="red">て</span>、
シャワーを{{'浴'|ruby:'あ'}}び<span class="red">て</span>、
ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べました。
(Yesterday I went back home, (then) took a shower, (and then) ate a meal.)

<blockquote class="blockquote-qna">

Q: <span class="red">どうやって</span>、{{'切'|ruby:'きっ'}}{{'符'|ruby:'ぷ'}}を{{'買'|ruby:'か'}}いますか。
(<span class="red">How</span> to buy the ticket?)

A: ここにお{{'金'|ruby:'かね'}}を{{'入'|ruby:'い'}}れ<span class="red">て</span>、
このボタンを{{'押'|ruby:'お'}}します。
(Insert money here, (and then) press this button.)

</blockquote>

<blockquote class="blockquote-qna">

Q: <span class="red">どうやって</span>、{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'帰'|ruby:'かえ'}}りますか。
(<span class="red">How</span> to go back to Japan?)

A: {{'空'|ruby:'くう'}}{{'港'|ruby:'こう'}}までタクシーで{{'行'|ruby:'い'}}っ<span class="red">て</span>、
{{'空'|ruby:'くう'}}{{'港'|ruby:'こう'}}から{{'飛'|ruby:'ひ'}}{{'行'|ruby:'こう'}}{{'機'|ruby:'き'}}に{{'乗'|ruby:'の'}}ります。
(Go to airport by taxi, (and then) ride the airplane from the airport.)

</blockquote>

お{{'金'|ruby:'かね'}}を{{'払'|ruby:'はら'}}っ<span class="red">てから</span>、{{'食'|ruby:'しょく'}}{{'事'|ruby:'じ'}}します。
(<span class="red">After</span> I pay with money, I have the meal.)

(Rule: verb て形 <span class="red">から</span>、verb)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
With this rule, the verb preceding <span class="red">から</span> should happen first (being a precondition), before the next verb.
</div>

{{'電'|ruby:'でん'}}{{'話'|ruby:'わ'}}をかけ<span class="red">てから</span>、{{'飲'|ruby:'の'}}みに{{'行'|ruby:'い'}}きます。
(<span class="red">After</span> I make a phone call, I go to drinking (alcohol).)

<blockquote class="blockquote-qna">

Q: もう、{{'晩'|ruby:'ばん'}}ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べましたか。
(Had you eat dinner already?)

A: いいえ、まだです。クラスが{{'終'|ruby:'お'}}わっ<span class="red">てから</span>、{{'食'|ruby:'た'}}べます。
(No, not yet. To eat <span class="red">after</span> the class finishes.)

</blockquote>

## Multiple subjects {#multiple-subjects}

{{5|jlpt}} {{'E01.L16'|icls}}

カリナさん<span class="orange">は</span>{{'背'|ruby:'せ'}}<span class="red">が</span>{{'高'|ruby:'たか'}}いです。
(<span class="orange">As for</span> Karina, her height <span class="red">is</span> tall.)

(Rule: topic <span class="orange">は</span> subject <span class="red">が</span> ...)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The topic precedes <span class="orange">は</span>, a subject precedes <span class="red">が</span>.
There can only be one topic and any number of subjects in a sentence.
</div>

<blockquote class="blockquote-qna">

Q: {{'象'|ruby:'ぞう'}}<span class="orange">は</span><span class="blue">どんな</span>{{'動'|ruby:'どう'}}{{'物'|ruby:'ぶつ'}}ですか。
(<span class="orange">As for</span> elephant, <span class="blue">what kind of</span> animal is it?)

A: {{'象'|ruby:'ぞう'}}<span class="orange">は</span>{{'鼻'|ruby:'はな'}}<span class="red">が</span>{{'長'|ruby:'なが'}}いです。
(<span class="orange">As for</span> elephant, its nose <span class="red">is</span> long.)

</blockquote>

## Multiple adjectives {#multiple-adjectives}

{{5|jlpt}} {{'E01.L16'|icls}}

Type | Rule | Polite form | て形 (te-form)
-- | -- | -- | --
Noun|add で|２６{{'歳'|ruby:'さい'}}|２６{{'歳'|ruby:'さい'}}<span class="red">で</span>
「な」adjective|add で|きれい|きれい<span class="red">で</span>
「い」adjective|い➝くて|{{'高'|ruby:'たか'}}<span class="blue">い</span>|{{'高'|ruby:'たか'}}<span class="red">くて</span>
いい（{{'良'|ruby:'よ'}}い）|いい➝よくて|<span class="blue">いい</span>|<span class="red">よくて</span>

{.table .table-hover .table-sm .align-text-bottom}

このビルは{{'高'|ruby:'たか'}}い。<span class="blue">そして</span>、きれいです。
(This building is tall <span class="blue">and (also)</span> beautiful.)

このビルは{{'高'|ruby:'たか'}}く<span class="red">て</span>、きれいです。
(This building is tall <span class="red">and (also)</span> beautiful.)

(Rule: adjective て形、similar-adjective)

<blockquote class="blockquote-qna">

Q: このビルはく<span class="red">どう</span>ですか。
(<span class="red">How</span> is this building?)

A: {{'高'|ruby:'たか'}}く<span class="red">て</span>、きれいです。
(Tall <span class="red">and (also)</span> beautiful.)

</blockquote>

<blockquote class="blockquote-qna">

Q: これは<span class="red">どんな</span>ビルですか。
(<span class="red">What kind of</span> building is this?)

A: {{'高'|ruby:'たか'}}く<span class="red">て</span>、きれいなビルです。
(Tall <span class="red">and (also)</span> beautiful building.)

</blockquote>

<blockquote class="blockquote-qna">

Q: すみません。{{'木'|ruby:'き'}}{{'村'|ruby:'むら'}}さんは<span class="red">どの</span>{{'人'|ruby:'ひと'}}ですか。
(Excuse me. <span class="red">Which</span> person is Kimura?)

A: {{'木'|ruby:'き'}}{{'村'|ruby:'むら'}}さんですか。
あの{{'髪'|ruby:'かみ'}}が{{'黒'|ruby:'くろ'}}く<span class="red">て</span>、
きれいな{{'人'|ruby:'ひと'}}です。
(Kimura is it? That black hair, <span class="red">(and also)</span> beautiful person over there.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'島'|ruby:'しま'}}{{'村'|ruby:'むら'}}さまの{{'鞄'|ruby:'かばん'}}は<span class="red">どれ</span>ですか。
(<span class="red">Which (one)</span> is Shimamura's bag?)

A: あの{{'赤'|ruby:'あか'}}く<span class="red">て</span>、{{'大'|ruby:'おお'}}きい{{'鞄'|ruby:'かばん'}}です。
(That red, <span class="red">(and also)</span> large bag over there.)

</blockquote>

## Please do not {#please-do-not}

{{5|jlpt}} {{'E02.L17'|icls}}

Group | Rule | ます形 (masu-form) | ない形 (nai-form)
-- | -- | -- | --
1 (五段)|います➝わない|{{'吸'|ruby:'す'}}<span class="blue">い</span>ます|{{'吸'|ruby:'す'}}<span class="red">わ</span>ない
1 (五段)|きます➝かない|{{'行'|ruby:'い'}}<span class="blue">き</span>ます|{{'行'|ruby:'い'}}<span class="red">か</span>ない
1 (五段)|ぎます➝がない|{{'急'|ruby:'いそ'}}<span class="blue">ぎ</span>ます|{{'急'|ruby:'いそ'}}<span class="red">が</span>ない
1 (五段)|します➝さない|{{'話'|ruby:'はな'}}<span class="blue">し</span>ます|{{'話'|ruby:'はな'}}<span class="red">さ</span>ない
1 (五段)|ちます➝たない|{{'待'|ruby:'ま'}}<span class="blue">ち</span>ます|{{'待'|ruby:'ま'}}<span class="red">た</span>ない
1 (五段)|びます➝ばない|{{'呼'|ruby:'よ'}}<span class="blue">び</span>ます|{{'呼'|ruby:'よ'}}<span class="red">ば</span>ない
1 (五段)|みます➝まない|{{'飲'|ruby:'の'}}<span class="blue">み</span>ます|{{'飲'|ruby:'の'}}<span class="red">ま</span>ない
1 (五段)|ります➝らない|{{'帰'|ruby:'かえ'}}<span class="blue">り</span>ます|{{'帰'|ruby:'かえ'}}<span class="red">ら</span>ない
1 (五段)|-|<span class="blue">あります</span>|<span class="red">ない</span>
2 (一段)|ます➝ない|{{'食'|ruby:'た'}}べ<span class="blue">ます</span>|{{'食'|ruby:'た'}}べ<span class="red">ない</span>
2 (一段)|ます➝ない|{{'見'|ruby:'み'}}<span class="blue">ます</span>|{{'見'|ruby:'み'}}<span class="red">ない</span>
3 (カ)|{{'来'|ruby:'き'}}ます➝{{'来'|ruby:'こ'}}ない|<span class="blue">{{'来'|ruby:'き'}}ます</span>|<span class="red">{{'来'|ruby:'こ'}}ない</span>
3 (サ)|します➝しない|し<span class="blue">ます</span>|し<span class="red">ない</span>
3 (サ)|します➝しない|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="blue">ます</span>|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="red">ない</span>

{.table .table-hover .table-sm .align-text-bottom}

たばこを{{'吸'|ruby:'す'}}い<span class="blue">ません</span>。<span class="blue">お{{'願'|ruby:'ねが'}}いします</span>。
(<span class="blue">Please do not</span> smoke.)

たばこを{{'吸'|ruby:'す'}}わ<span class="red">ないでください</span>。
(<span class="red">Please do not</span> smoke.)

(Rule: verb ない形 <span class="red">でください</span>)

{{'今日'|ruby:'きょう'}}は{{'試'|ruby:'し'}}{{'験'|ruby:'けん'}}ですから、
{{'本'|ruby:'ほん'}}や{{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}を{{'使'|ruby:'つか'}}わ<span class="red">ないでください</span>。
(Because there is an exam today, <span class="red">please do not</span> use book and dictionary.)

## Need {#need}

{{5|jlpt}} {{'E02.L17'|icls}}

{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'人'|ruby:'じん'}}ですから、
パスポートを{{'見'|ruby:'み'}}せ<span class="red">なければなりません</span>。
(Lit: Because you are Japanese, you <span class="red">must not to not</span> show your passport.)
(Because you are Japanese, you <span class="red">need to</span> show your passport.)

(Rule: verb ない形 <span class="red">なければ なりません</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Shorthand for 「なければ なりません」 is 「<span class="red">ないと</span>」.
</div>

マレーシア{{'人'|ruby:'じん'}}はパスポートを{{'見'|ruby:'み'}}せ<span class="red">なくてもいい</span>です。
(Malaysians <span class="red">do not need to</span> show their passport.)

(Rule: verb ない形 <span class="red">なくて もいい</span> ［です］)

まだ{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}があります。{{'急'|ruby:'いそ'}}が<span class="red">なくてもいい</span>です。
(There is still some time. <span class="red">Do not need to</span> hurry.)

もう{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}がありませんから、{{'急'|ruby:'いそ'}}が<span class="red">なければなりません</span>。
(Because there is no more time, <span class="red">need to</span> hurry.)

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}の{{'家'|ruby:'うち'}}は{{'靴'|ruby:'くつ'}}を{{'脱'|ruby:'ぬ'}}が<span class="orange">なければなりません</span>か。
(Do I <span class="orange">need to</span> take off my shoes in a house in Japan?)

A: はい、{{'脱'|ruby:'ぬ'}}が<span class="orange">なければなりません</span>。
(Yes, you <span class="orange">need to</span> take them off.)

</blockquote>

<blockquote class="blockquote-qna">

Q: マレーシア{{'人'|ruby:'じん'}}はパスポートを{{'見'|ruby:'み'}}せ<span class="orange">なければなりません</span>か。
(Do Malaysians <span class="orange">need to</span> show their passport?)

A: いいえ、{{'見'|ruby:'み'}}せ<span class="orange">なくてもいい</span>です。
(No, <span class="orange">do not need to</span> show passport.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}とエアコンを{{'消'|ruby:'け'}}しますか。
(Do I turn off the lights and the air conditioner?)

A: {{'電'|ruby:'でん'}}{{'気'|ruby:'き'}}<span class="red">は</span>{{'消'|ruby:'け'}}してください。
エアコン<span class="red">は</span>{{'消'|ruby:'け'}}さなくてもいいです。
(<span class="red">As for</span> the light, please turn it off. <span class="red">As for</span> the air conditioner, do not need to turn it off.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}も{{'書'|ruby:'か'}}きますか。
(Do I also write the address?)

A: {{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}<span class="red">は</span>{{'書'|ruby:'か'}}かなくてもいいです。
(<span class="red">As for</span> the address, do not need to write it.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'名'|ruby:'な'}}{{'前'|ruby:'まえ'}}と{{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}<span class="orange">どちらも</span>{{'書'|ruby:'か'}}きますか。
(Do I write <span class="orange">both</span> the name and address?)

A: {{'名'|ruby:'な'}}{{'前'|ruby:'まえ'}}を{{'書'|ruby:'か'}}いてください。
{{'住'|ruby:'じゅう'}}{{'所'|ruby:'しょ'}}<span class="red">は</span>{{'書'|ruby:'か'}}かなくてもいいです。
(Please write the name. <span class="red">As for</span> the address, do not need to write it.)

</blockquote>

## こと {#koto}

{{5|jlpt}} {{'E02.L18'|icls}}

Group | Rule | ます形 (masu-form) | {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 (dictionary form)
-- | -- | -- | --
1 (五段)|います➝う|{{'吸'|ruby:'す'}}<span class="blue">います</span>|{{'吸'|ruby:'す'}}<span class="red">う</span>
1 (五段)|きます➝く|{{'行'|ruby:'い'}}<span class="blue">きます</span>|{{'行'|ruby:'い'}}<span class="red">く</span>
1 (五段)|ぎます➝ぐ|{{'急'|ruby:'いそ'}}<span class="blue">ぎます</span>|{{'急'|ruby:'いそ'}}<span class="red">ぐ</span>
1 (五段)|します➝す|{{'話'|ruby:'はな'}}<span class="blue">します</span>|{{'話'|ruby:'はな'}}<span class="red">す</span>
1 (五段)|ちます➝つ|{{'待'|ruby:'ま'}}<span class="blue">ちます</span>|{{'待'|ruby:'ま'}}<span class="red">つ</span>
1 (五段)|びます➝ぶ|{{'呼'|ruby:'よ'}}<span class="blue">びます</span>|{{'呼'|ruby:'よ'}}<span class="red">ぶ</span>
1 (五段)|みます➝む|{{'飲'|ruby:'の'}}<span class="blue">みます</span>|{{'飲'|ruby:'の'}}<span class="red">む</span>
1 (五段)|ります➝る|{{'帰'|ruby:'かえ'}}<span class="blue">ります</span>|{{'帰'|ruby:'かえ'}}<span class="red">る</span>
2 (一段)|ます➝る|{{'食'|ruby:'た'}}べ<span class="blue">ます</span>|{{'食'|ruby:'た'}}べ<span class="red">る</span>
2 (一段)|ます➝る|{{'見'|ruby:'み'}}<span class="blue">ます</span>|{{'見'|ruby:'み'}}<span class="red">る</span>
3 (カ)|{{'来'|ruby:'き'}}ます➝{{'来'|ruby:'く'}}る|<span class="blue">{{'来'|ruby:'き'}}ます</span>|<span class="red">{{'来'|ruby:'く'}}る</span>
3 (サ)|します➝する|<span class="blue">します</span>|<span class="red">する</span>
3 (サ)|します➝する|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}<span class="blue">します</span>|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}<span class="red">する</span>

{.table .table-hover .table-sm .align-text-bottom}

{{'私'|ruby:'わたし'}}の{{'趣'|ruby:'しゅ'}}{{'味'|ruby:'み'}}はスポーツです。
(My hobby is sports.)

{{'私'|ruby:'わたし'}}の{{'趣'|ruby:'しゅ'}}{{'味'|ruby:'み'}}は{{'絵'|ruby:'え'}}を{{'書'|ruby:'か'}}く<span class="red">こと</span>です。
(My hobby is to draw pictures.)

(Rule: verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 <span class="red">こと</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in dictionary form combined with <span class="red">こと</span> becomes a noun.
</div>

<blockquote class="blockquote-qna">

Q: {{'趣'|ruby:'しゅ'}}{{'味'|ruby:'み'}}は<span class="red">{{'何'|ruby:'なん'}}</span>ですか。
(<span class="red">What</span> is your hobby?)

A: {{'絵'|ruby:'え'}}を{{'書'|ruby:'か'}}く<span class="orange">こと</span>です。
(To draw pictures.)

</blockquote>

{{'逆'|ruby:'さか'}}{{'立'|ruby:'だ'}}ち<span class="orange">が</span>できます。
(I can do a handstand.)

(Rule: ability (noun) <span class="orange">が</span> できます)

{{'私'|ruby:'わたし'}}は{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}を{{'話'|ruby:'はな'}}す<span class="red">こと</span><span class="orange">が</span>できます。
(I can speak Japanese language.)

<blockquote class="blockquote-qna">

Q: {{'何'|ruby:'なん'}}メートルぐらい{{'泳'|ruby:'およ'}}ぐ<span class="red">こと</span><span class="orange">が</span>できますか。
(About how many meters can you swim?)

A: １００メートルぐらい{{'泳'|ruby:'およ'}}ぐ<span class="red">こと</span><span class="orange">が</span>できます。
(I can swim for about 100 meters.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'誰'|ruby:'だれ'}}の{{'曲'|ruby:'きょく'}}を{{'弾'|ruby:'ひ'}}く<span class="red">こと</span><span class="orange">が</span>できますか。
(Whose music can you play?)

A: バッハの{{'曲'|ruby:'きょく'}}を{{'弾'|ruby:'ひ'}}く<span class="red">こと</span><span class="orange">が</span>できます。
(I can play Bach's music.)

</blockquote>

<blockquote class="blockquote-qna">

Q: どんな{{'曲'|ruby:'きょく'}}を{{'弾'|ruby:'ひ'}}く<span class="red">こと</span><span class="orange">が</span>できますか。
(What kind of music can you play?)

A: クラシックの{{'曲'|ruby:'きょく'}}を{{'弾'|ruby:'ひ'}}く<span class="red">こと</span><span class="orange">が</span>できます。
(I can play classical music.)

</blockquote>

パソコン<span class="orange">で</span>{{'買'|ruby:'か'}}い{{'物'|ruby:'もの'}}することができます。
(I can do shopping <span class="orange">via</span> personal computer.)

{{'図'|ruby:'と'}}{{'書'|ruby:'しょ'}}{{'館'|ruby:'かん'}}<span class="orange">で</span>{{'本'|ruby:'ほん'}}を{{'借'|ruby:'か'}}りることができます。
(I can borrow books <span class="orange">via</span> the library.)

コンビニ<span class="orange">で</span>２４{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}{{'買'|ruby:'か'}}うことができます。
(I can buy things for 24 hours <span class="orange">via</span> the convenience store.)

## Before {#before}

{{5|jlpt}} {{'E02.L18'|icls}}

{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}に{{'行'|ruby:'い'}}く<span class="red">{{'前'|ruby:'まえ'}}に</span>、{{'鞄'|ruby:'かばん'}}を{{'買'|ruby:'か'}}います。
(<span class="red">Before</span> I go to the trip, I buy a bag.)

(Rule: verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 <span class="red">{{'前'|ruby:'まえ'}}に</span>)

{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}<span class="orange">の</span><span class="red">{{'前'|ruby:'まえ'}}に</span>、{{'鞄'|ruby:'かばん'}}を{{'買'|ruby:'か'}}います。
(<span class="red">Before</span> the trip, I buy a bag.)

(Rule: noun <span class="orange">の</span> <span class="red">{{'前'|ruby:'まえ'}}に</span>)

１{{'年'|ruby:'ねん'}}<span class="red">{{'前'|ruby:'まえ'}}に</span>、マレーシアへ{{'来'|ruby:'き'}}ました。
(1 year <span class="red">ago</span>, I came to Malaysia.)

<blockquote class="blockquote-qna">

Q: いつ、{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}へ{{'行'|ruby:'い'}}きましたか。
(When did you go to Japan?)

A1: ６か{{'月'|ruby:'げつ'}}<span class="red">{{'前'|ruby:'まえ'}}に</span>、{{'行'|ruby:'い'}}きました。
(6 months <span class="red">ago</span>, I went.)

A2: {{'働'|ruby:'はたら'}}く<span class="red">{{'前'|ruby:'まえ'}}に</span>、{{'行'|ruby:'い'}}きました。
(<span class="red">Before</span> I work, I went.)

</blockquote>

## Experience {{'経'|ruby:'けい'}}{{'験'|ruby:'けん'}} {#experience}

{{5|jlpt}} {{'E02.L19'|icls}}

Group | Rule | ます形 (masu-form) | た形 (ta-form)
-- | -- | -- | --
1 (五段)|います➝った|{{'吸'|ruby:'す'}}<span class="blue">います</span>|{{'吸'|ruby:'す'}}<span class="red">った</span>
1 (五段)|ちます➝った|{{'待'|ruby:'ま'}}<span class="blue">ちます</span>|{{'待'|ruby:'ま'}}<span class="red">った</span>
1 (五段)|ります➝った|{{'帰'|ruby:'かえ'}}<span class="blue">ります</span>|{{'帰'|ruby:'かえ'}}<span class="red">った</span>
1 (五段)|みます➝んだ|{{'飲'|ruby:'の'}}<span class="blue">みます</span>|{{'飲'|ruby:'の'}}<span class="red">んだ</span>
1 (五段)|びます➝んだ|{{'呼'|ruby:'よ'}}<span class="blue">びます</span>|{{'呼'|ruby:'よ'}}<span class="red">んだ</span>
1 (五段)|ぎます➝いだ|{{'急'|ruby:'いそ'}}<span class="blue">ぎます</span>|{{'急'|ruby:'いそ'}}<span class="red">いだ</span>
1 (五段)|します➝した|{{'話'|ruby:'はな'}}<span class="blue">します</span>|{{'話'|ruby:'はな'}}<span class="red">した</span>
1 (五段)|-|{{'行'|ruby:'い'}}<span class="blue">きます</span>|{{'行'|ruby:'い'}}<span class="red">った</span>
2 (一段)|ます➝た|{{'食'|ruby:'た'}}べ<span class="blue">ます</span>|{{'食'|ruby:'た'}}べ<span class="red">た</span>
2 (一段)|ます➝た|{{'見'|ruby:'み'}}<span class="blue">ます</span>|{{'見'|ruby:'み'}}<span class="red">た</span>
3 (カ)|{{'来'|ruby:'き'}}ます➝{{'来'|ruby:'き'}}た|{{'来'|ruby:'き'}}<span class="blue">ます</span>|{{'来'|ruby:'き'}}<span class="red">た</span>
3 (サ)|します➝した|し<span class="blue">ます</span>|し<span class="red">た</span>
3 (サ)|します➝した|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="blue">ます</span>|{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="red">た</span>

{.table .table-hover .table-sm .align-text-bottom}

{{'私'|ruby:'わたし'}}は{{'北'|ruby:'ほっ'}}{{'海'|ruby:'かい'}}{{'道'|ruby:'どう'}}へ{{'行'|ruby:'い'}}った<span class="red">こと</span>があります。
(I had gone to Hokkaido.)

(Rule: verb た形 <span class="red">こと</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
A verb in ta-form combined with <span class="red">こと</span> becomes a noun.
</div>

{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}を{{'休'|ruby:'やす'}}んだ<span class="red">こと</span>がありません。
(I had not taken a rest from school.)

<blockquote class="blockquote-qna">

Q: {{'北'|ruby:'ほっ'}}{{'海'|ruby:'かい'}}{{'道'|ruby:'どう'}}へ{{'行'|ruby:'い'}}った<span class="red">こと</span>がありますか。
(Have you gone to Hokkaido?)

A1: はい、{{'一'|ruby:'いち'}}{{'度'|ruby:'ど'}}あります。
１０{{'年'|ruby:'ねん'}}{{'前'|ruby:'まえ'}}に{{'行'|ruby:'い'}}きました。
(Yes, for once. 10 years ago I went (there).)

A2: いいえ、{{'一'|ruby:'いち'}}{{'度'|ruby:'ど'}}もありません。
{{'是'|ruby:'ぜ'}}{{'非'|ruby:'ひ'}}、{{'行'|ruby:'い'}}きたいです。
(No, not once. I really want to go.)

</blockquote>

## Multiple actions {#multiple-actions}

{{5|jlpt}} {{'E02.L19'|icls}}

{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'見'|ruby:'み'}}ます。{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}します。
(Watch movie. Take a walk.)

{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}を{{'見'|ruby:'み'}}<span class="red">たり</span>、{{'散'|ruby:'さん'}}{{'歩'|ruby:'ぽ'}}し<span class="red">たり</span>します。
(Watch movie <span class="red">and</span> take a walk.)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Add 「り」to a verb's ta-form to get the <span class="red">たり</span>.
</div>

テニスをし<span class="red">たり</span>、{{'本'|ruby:'ほん'}}を{{'読'|ruby:'よ'}}ん<span class="red">だり</span>しました。
(Played tennis <span class="red">and</span> read book.)

## Become {#become}

{{5|jlpt}} {{'E02.L19'|icls}}

Type | Rule | Polite form | Adverbial form
-- | -- | -- | --
Noun|add に|２６{{'歳'|ruby:'さい'}}|２６{{'歳'|ruby:'さい'}}<span class="red">に</span>
「な」adjective|add に|きれい|きれい<span class="red">に</span>
「い」adjective|い➝く|{{'高'|ruby:'たか'}}<span class="blue">い</span>|{{'高'|ruby:'たか'}}<span class="red">く</span>
いい（{{'良'|ruby:'よ'}}い）|いい➝よく|<span class="blue">いい</span>|<span class="red">よく</span>

{.table .table-hover .table-sm .align-text-bottom}

{{'今日'|ruby:'きょう'}}は{{'暖'|ruby:'あたた'}}か<span class="orange">く</span>なりました。
{{'明日'|ruby:'あした'}}は{{'寒'|ruby:'さむ'}}<span class="orange">く</span>なります。
(It became warm today. It will become cold tomorrow.)

{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}が{{'上'|ruby:'じょう'}}{{'手'|ruby:'ず'}}<span class="red">に</span>なりました。
(You became good at Japanese language.)

１５{{'歳'|ruby:'さい'}}<span class="red">に</span>なりました。
(You became 15 years old.)

## Informal {#informal}

{{5|jlpt}} {{'E02.L20'|icls}}

Term | Meaning
-- | --
{{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}{{'体'|ruby:'たい'}}|Formal sentence
{{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}{{'体'|ruby:'たい'}}|Informal sentence
{{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形|Formal form
{{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形|Informal form

{.table .table-hover .table-sm .align-text-bottom .table-ruby-1}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
{{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 is used when:

- Speaking to friends / family
- Writing a report
</div>

{{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}体 | {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}体
-- | --
{{'行'|ruby:'い'}}<span class="blue">きませんか。</span>|{{'行'|ruby:'い'}}<span class="red">かない？</span>
{{'行'|ruby:'い'}}きたい<span class="blue">です</span>。|{{'行'|ruby:'い'}}きたい。
{{'行'|ruby:'い'}}<span class="blue">きます</span>。|{{'行'|ruby:'い'}}<span class="red">く</span>。

{.table .table-hover .table-sm .align-text-bottom}

Verb {{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形 | Verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 | Rule
-- | -- | --
{{'書'|ruby:'か'}}<span class="blue">きます</span>|{{'書'|ruby:'か'}}<span class="red">く</span>|Dictionary form
{{'書'|ruby:'か'}}<span class="blue">きません</span>|{{'書'|ruby:'か'}}<span class="red">かない</span>|Nai-form
{{'書'|ruby:'か'}}<span class="blue">きました</span>|{{'書'|ruby:'か'}}<span class="red">いた</span>|Ta-form
{{'書'|ruby:'か'}}<span class="blue">きませんでした</span>|{{'書'|ruby:'か'}}<span class="red">かなかった</span>|Nakatta-form
あ<span class="blue">ります</span>|あ<span class="red">る</span>|Dictionary form
<span class="blue">ありません</span>|<span class="red">ない</span>|Nai-form (ない)
あ<span class="blue">りました</span>|あ<span class="red">った</span>|Ta-form
<span class="blue">ありませんでした</span>|<span class="red">なかった</span>|Nakatta-form (なかった)

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: コンビニへ{{'行'|ruby:'い'}}く？
(Go to the convenience store?)

A1: うん、{{'行'|ruby:'い'}}く。
(Yes, go.)

A2: ううん、{{'行'|ruby:'い'}}かない。
(No, do not go.)

</blockquote>

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
When asking a question with {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}体, omit the 「か」 at the end.

When such question is presented verbally, the intotation is important.
</div>

い adjective {{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形 | い adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 | Rule
-- | -- | --
{{'大'|ruby:'おお'}}きい<span class="blue">です</span>|{{'大'|ruby:'おお'}}きい|Omit です (Dictionary form)
{{'大'|ruby:'おお'}}きくない<span class="blue">です</span>|{{'大'|ruby:'おお'}}きくない|Omit です (Nai-form)
{{'大'|ruby:'おお'}}きかった<span class="blue">です</span>|{{'大'|ruby:'おお'}}きかった|Omit です (Ta-form)
{{'大'|ruby:'おお'}}きくなかった<span class="blue">です</span>|{{'大'|ruby:'おお'}}きくなかった|Omit です (Nakatta-form)

{.table .table-hover .table-sm .align-text-bottom}

な adjective {{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形 | な adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 | Rule
-- | -- | --
きれい<span class="blue">です</span>|きれい<span class="red">だ</span>|Dictionary form
きれいじゃ<span class="blue">ありません</span>|きれいじゃ<span class="red">ない</span>|Nai-form
きれい<span class="blue">でした</span>|きれい<span class="red">だった</span>|Ta-form
きれいじゃ<span class="blue">ありませんでした</span>|きれいじゃ<span class="red">なかった</span>|Nakatta-form

{.table .table-hover .table-sm .align-text-bottom}

Noun {{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形 | Noun {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 | Rule
-- | -- | --
{{'雨'|ruby:'あめ'}}<span class="blue">です</span>|{{'雨'|ruby:'あめ'}}<span class="red">だ</span>|Dictionary form
{{'雨'|ruby:'あめ'}}じゃ<span class="blue">ありません</span>|{{'雨'|ruby:'あめ'}}じゃ<span class="red">ない</span>|Nai-form
{{'雨'|ruby:'あめ'}}<span class="blue">でした</span>|{{'雨'|ruby:'あめ'}}<span class="red">だった</span>|Ta-form
{{'雨'|ruby:'あめ'}}じゃ<span class="blue">ありませんでした</span>|{{'雨'|ruby:'あめ'}}じゃ<span class="red">なかった</span>|Nakatta-form

{.table .table-hover .table-sm .align-text-bottom}

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}{{'暇'|ruby:'ひま'}}？
(Free tomorrow?)

A1: うん、{{'暇'|ruby:'ひま'}}。
(Yes, free.)

A2: ううん、{{'暇'|ruby:'ひま'}}じゃない。
(No, not free.)

</blockquote>

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
When asking/answering a question with {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}体,
omit the 「だ」 at the end of the noun / the な adjective.
</div>

Question {{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}形 | Question {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形
-- | --
なんじ<span class="blue">ですか。</span>|なん<span class="red">じ？</span>
どう<span class="blue">ですか。</span>|どう<span class="red">？</span>
どう<span class="blue">でしたか。</span>|どう<span class="red">だった？</span>
<span class="blue">なんですか。</span>|<span class="red">なに？</span>

{.table .table-hover .table-sm .align-text-bottom}

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
「なに」 becomes 「なん」 if one of the following is true:

- a *t-* / *d-* / *n-* sound comes after it, e.g. なんの (「の」 is an *n-* sound)
- the answer to the question is a number, e.g. なんじ (what time)
- day of week, e.g. なんようび (what day of week)
</div>

<blockquote class="blockquote-qna">

Q: このビルはどう？
(How is this building?)

A1: {{'古'|ruby:'ふる'}}い<span class="red">けど</span>きれい。
(Old <span class="red">but</span> beautiful.)

(Rule: い adjective <span class="red">けど</span>)

A2: きれい<span class="orange">だ</span><span class="red">けど</span>{{'古'|ruby:'ふる'}}い。
(Beautiful <span class="red">but</span> old.)

(Rule: Noun / な adjective <span class="orange">だ</span> <span class="red">けど</span>)

</blockquote>

## Probably {#probably}

{{5|jlpt}} {{'E02.L21'|icls}}

{{'多'|ruby:'た'}}{{'分'|ruby:'ぶん'}}、{{'女'|ruby:'おんな'}}です。
(Probably, that is a female.)

{{'女'|ruby:'おんな'}}だ<span class="orange">と</span><span class="red">{{'思'|ruby:'おも'}}います</span>。
(I <span class="red">think</span> that is a female.)

(Rule: noun {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">と</span> <span class="red">{{'思'|ruby:'おも'}}います</span>)

{{'暇'|ruby:'ひま'}}だ<span class="orange">と</span><span class="red">{{'思'|ruby:'おも'}}います</span>。
(I <span class="red">think</span> it is free.)

(Rule: な adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">と</span> <span class="red">{{'思'|ruby:'おも'}}います</span>)

{{'辛'|ruby:'から'}}い<span class="orange">と</span><span class="red">{{'思'|ruby:'おも'}}います</span>。
(I <span class="red">think</span> it is spicy.)

(Rule: い adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">と</span> <span class="red">{{'思'|ruby:'おも'}}います</span>)

{{'多'|ruby:'た'}}{{'分'|ruby:'ぶん'}}、{{'降'|ruby:'ふ'}}りました。
(Probably, it rained.)

{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}った<span class="orange">と</span><span class="red">{{'思'|ruby:'おも'}}います</span>。
(I <span class="red">think</span> it rained.)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">と</span> <span class="red">{{'思'|ruby:'おも'}}います</span>)

{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}らなかった<span class="orange">と</span><span class="red">{{'思'|ruby:'おも'}}います</span>。
(I <span class="red">think</span> it didn't rain.)

<blockquote class="blockquote-qna">

Q: {{'午'|ruby:'ご'}}{{'後'|ruby:'ご'}}{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}りますか。
(Will it rain in the evening?)

A1: はい、<span class="orange">{{'多'|ruby:'た'}}{{'分'|ruby:'ぶん'}}</span>{{'降'|ruby:'ふ'}}ると{{'思'|ruby:'おも'}}います。
(Yes, I think it <span class="orange">probably</span> will rain.)

A2: はい、<span class="orange">きっと</span>{{'降'|ruby:'ふ'}}ると{{'思'|ruby:'おも'}}います。
(Yes, I think it <span class="orange">surely</span> will rain.)

A3: いいえ、<span class="orange">{{'多'|ruby:'た'}}{{'分'|ruby:'ぶん'}}</span>{{'降'|ruby:'ふ'}}らないと{{'思'|ruby:'おも'}}います。
(No, I think it <span class="orange">probably</span> will not rain.)

A4: いいえ、<span class="orange">きっと</span>{{'降'|ruby:'ふ'}}らないと{{'思'|ruby:'おも'}}います。
(No, I think it <span class="orange">surely</span> will not rain.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'試'|ruby:'し'}}{{'験'|ruby:'けん'}}は{{'難'|ruby:'むずか'}}しいですか。
(Is the test difficult?)

A: {{'難'|ruby:'むずか'}}しいと{{'思'|ruby:'おも'}}います。
(I think it is difficult.) (probably, as the event haven't happen)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}は{{'難'|ruby:'むずか'}}しいですか。
(Is Japanese language difficult?)

A: {{'難'|ruby:'むずか'}}しいと{{'思'|ruby:'おも'}}います。
(I think it is difficult.) (opinion, as the event already happen)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}のアニメ<span class="orange">について</span>どう{{'思'|ruby:'おも'}}いますか。
(<span class="orange">About</span> Japanese anime, what do you think about it?)

A: {{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}いと{{'思'|ruby:'おも'}}います。
(I think it is interesting.)

</blockquote>

## Say {#say}

{{5|jlpt}} {{'E02.L21'|icls}}

ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べます。「いただきます」<span class="orange">と</span><span class="red">{{'言'|ruby:'い'}}います</span>。
(I (am going to) eat my meal. I <span class="red">say</span> "itadakimasu".)

(Rule: 「～」<span class="orange">と</span> <span class="red">{{'言'|ruby:'い'}}います</span>)

ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べました。「ごちそうさまでした」<span class="orange">と</span><span class="red">{{'言'|ruby:'い'}}います</span>。
(I ate my meal. I <span class="red">say</span> "gochisousama-deshita".)

<blockquote class="blockquote-qna">

Q: {{'出'|ruby:'で'}}かけます。<span class="red">{{'何'|ruby:'なん'}}</span>と{{'言'|ruby:'い'}}いますか。
(Someone goes out. <span class="red">What</span> to say?)

A1: 「{{'行'|ruby:'い'}}って{{'来'|ruby:'き'}}ます」と{{'言'|ruby:'い'}}います。
(Say "ittekimasu".) (By the person who goes out)

A2: 「いってらっしゃい」と{{'言'|ruby:'い'}}います。
(Say "itterasshai".) (By the person who fares well)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'帰'|ruby:'かえ'}}りました。<span class="red">{{'何'|ruby:'なん'}}</span>と{{'言'|ruby:'い'}}いますか。
(Someone returned home. <span class="red">What</span> to say?)

A1: 「ただいま」と{{'言'|ruby:'い'}}います。
(Say "tadaima".) (By the person who returned)

A2: 「お{{'帰'|ruby:'かえ'}}りなさい」と{{'言'|ruby:'い'}}います。
(Say "okaerinasai".) (By the person who welcomes)

</blockquote>

{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんは３０１{{'教'|ruby:'きょう'}}{{'室'|ruby:'しつ'}}に{{'来'|ruby:'き'}}て<span class="orange">と</span><span class="red">{{'言'|ruby:'い'}}いました</span>。
(Tanaka <span class="red">said</span> to come to classroom 301.)

(Rule: verb/adjective/noun {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">と</span> <span class="red">{{'言'|ruby:'い'}}いました</span>)

<blockquote class="blockquote-qna">

Q: {{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}は<span class="red">{{'何'|ruby:'なん'}}</span>と{{'言'|ruby:'い'}}いましたか。
(<span class="red">What</span> did the teacher say?)

A: {{'明日'|ruby:'あした'}}１０{{'時'|ruby:'じ'}}から{{'漢'|ruby:'かん'}}{{'字'|ruby:'じ'}}の{{'試'|ruby:'し'}}{{'験'|ruby:'けん'}}があると{{'言'|ruby:'い'}}いました。
(The teacher said to have Kanji test at 10 o' clock tomorrow.)

</blockquote>

## Seek agreement {#seek-agreement}

{{5|jlpt}} {{'E02.L21'|icls}}

{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}の{{'映'|ruby:'えい'}}{{'画'|ruby:'が'}}は{{'高'|ruby:'たか'}}い<span class="red">でしょう？</span>
(Japanese movie is expensive, <span class="red">isn't it?</span>)

(Rule: い adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 でしょう？)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
「<span class="red">でしょう</span>」 is for informal use of seeking agreement.
</div>

きれい<span class="red">でしょう？</span>
(It is beautiful, <span class="red">isn't it?</span>)

(Rule: noun / な adjective {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 でしょう？)

<div class="alert alert-warning">
<i class="bi bi-exclamation-circle"></i>
When used with 「<span class="red">でしょう</span>」, omit the 「だ」 at the end of the noun / the な adjective.
</div>

{{'明日'|ruby:'あした'}}パーティーに{{'来'|ruby:'く'}}る<span class="red">でしょう？</span>
(Come to the party tomorrow, <span class="red">will you?</span>)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 でしょう？)

<blockquote class="blockquote-qna">

Q: {{'明日'|ruby:'あした'}}パーティーに{{'来'|ruby:'く'}}る<span class="red">でしょう？</span>
(Come to the party tomorrow, <span class="red">will you?</span>)

A: ええ、{{'行'|ruby:'い'}}きます。
(Yes, I (will) go.)

</blockquote>

## Describing {#describing}

{{5|jlpt}} {{'E02.L22'|icls}}

{{'母'|ruby:'はは'}}にもらったシャツ。
(A shirt (that I) received from my mother.)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 noun)

これ<span class="orange">は</span>{{'去'|ruby:'きょ'}}{{'年'|ruby:'ねん'}}{{'買'|ruby:'か'}}ったカメラです。
(This is a camera bought last year.)

これ<span class="orange">は</span>{{'私'|ruby:'わたし'}}<span class="orange">が</span>{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}している{{'本'|ruby:'ほん'}}です。
(This is a book that I am using to study.)

<blockquote class="blockquote-qna">

Q: {{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さんは<span class="red">どの</span>{{'人'|ruby:'ひと'}}ですか。
(<span class="red">Which</span> person is Yamada?)

A: コピーしている{{'人'|ruby:'ひと'}}です。
(The person that is copying stuff (there).)

</blockquote>

［ずっと］シャツを{{'着'|ruby:'き'}}て<span class="red">います</span>。
(I am wear<span class="red">ing</span> a shirt.)

(Rule: ［ずっと］ verb て形 <span class="red">います</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The ずっと here implies "a present and continuous state", instead of "a state that is continuing since the long past".

For example, 「シャツを{{'着'|ruby:'き'}}ています」 is "a present and continuous state" as the speaker is in a present state of "wearing a shirt".

On the other hand, 「{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}しています」 is "a state that is continuing since the long past" as the speaker is married and continues to be married presently since then.
</div>

<blockquote class="blockquote-qna">

Q: あなたは<span class="red">{{'何'|ruby:'なに'}}</span>を{{'着'|ruby:'き'}}ていますか。
(<span class="red">What</span> are you wearing?)

A: スーツを{{'着'|ruby:'き'}}ています。
(I am wearing a suit.)

</blockquote>

{{'着'|ruby:'き'}}{{'物'|ruby:'もの'}}を{{'着'|ruby:'き'}}ている{{'人'|ruby:'ひと'}}<span class="orange">は</span>{{'鈴'|ruby:'すず'}}{{'木'|ruby:'き'}}さんです。
(The person wearing a kimono is Suzuki.)

<blockquote class="blockquote-qna">

Q: {{'黒'|ruby:'くろ'}}い{{'靴'|ruby:'くつ'}}を{{'履'|ruby:'は'}}いている{{'人'|ruby:'ひと'}}は<span class="red">{{'誰'|ruby:'だれ'}}</span>ですか。
(<span class="red">Who</span> is the person wearing black shoes?)

A: {{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんです。
(Tanaka.)

</blockquote>

{{'私'|ruby:'わたし'}}が{{'生'|ruby:'う'}}まれました。{{'所'|ruby:'ところ'}}<span class="orange">は</span>{{'福'|ruby:'ふく'}}{{'岡'|ruby:'おか'}}です。
(I have born. The place is Fukuoka.)

{{'私'|ruby:'わたし'}}が{{'生'|ruby:'う'}}まれた{{'所'|ruby:'ところ'}}<span class="orange">は</span>{{'福'|ruby:'ふく'}}{{'岡'|ruby:'おか'}}です。
(The place I am born at is Fukuoka.)

{{'私'|ruby:'わたし'}}がよく{{'行'|ruby:'い'}}く{{'喫'|ruby:'きっ'}}{{'茶'|ruby:'さ'}}{{'店'|ruby:'てん'}}<span class="orange">は</span>{{'駅'|ruby:'えき'}}の{{'隣'|ruby:'となり'}}にあります。
(The cafe that I often go to is at next to the station.)

{{'昨日'|ruby:'きのう'}}{{'見'|ruby:'み'}}たテレビ<span class="red">は</span>{{'面'|ruby:'おも'}}{{'白'|ruby:'しろ'}}かったです。
(The television (programme) I watched yesterday was interesting.)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 noun <span class="red">は</span> adjective)

{{'父'|ruby:'ちち'}}にもらった{{'時'|ruby:'と'}}{{'計'|ruby:'けい'}}<span class="red">を</span>{{'毎'|ruby:'まい'}}{{'日'|ruby:'にち'}}{{'使'|ruby:'つか'}}います。
(Every day I use the watch received from my father.)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 noun <span class="red">を</span> verb)

プールがある{{'家'|ruby:'うち'}}<span class="red">が</span>{{'欲'|ruby:'ほ'}}しいです。
(I want a house that has a swimming pool.)

(Rule: verb {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 noun <span class="red">が</span> {{'欲'|ruby:'ほ'}}しい)

{{'図'|ruby:'と'}}{{'書'|ruby:'しょ'}}{{'館'|ruby:'かん'}}で{{'借'|ruby:'か'}}りた{{'本'|ruby:'ほん'}}<span class="orange">を</span>{{'読'|ruby:'よ'}}みたいです。
(I want to read a book borrowed from the library.)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
Verb in tai-form (～たい) is usually preceded by either 「を」 or 「が」 particle.
</div>

{{'店'|ruby:'みせ'}}がたくさんある{{'町'|ruby:'まち'}}<span class="orange">に</span>{{'住'|ruby:'す'}}みたいです。
(I want to live in a town with a lot of shops.)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The verbs that mean staying for a long time, such as 「{{'町'|ruby:'まち'}}<span class="orange">に</span>{{'住'|ruby:'す'}}む」(live at a town), uses the 「に」 particle instead.

Other examples include parking a vehicle, staying at a hotel, etc.
</div>

{{'朝'|ruby:'あさ'}}ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べる{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}<span class="orange">が</span>あります。
(I have the time to eat breakfast.)

(Rule: <span class="red">verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形</span> noun <span class="orange">が</span> あります/ありません)

## When 2 {#when-2}

{{5|jlpt}} {{'E02.L23'|icls}}

{{'暑'|ruby:'あつ'}}い<span class="orange">{{'時'|ruby:'とき'}}</span>、{{'窓'|ruby:'まど'}}を{{'開'|ruby:'あ'}}けます。
(<span class="orange">When</span> it is hot, open the window.)

(Rule: い adjective <span class="orange">{{'時'|ruby:'とき'}}</span>)

{{'暇'|ruby:'ひま'}}<span class="red">な</span><span class="orange">{{'時'|ruby:'とき'}}</span>、{{'本'|ruby:'ほん'}}を{{'読'|ruby:'よ'}}みます。
(<span class="orange">When</span> free, read book.)

(Rule: な adjective <span class="red">な</span> <span class="orange">{{'時'|ruby:'とき'}}</span>)

{{'雨'|ruby:'あめ'}}<span class="red">の</span><span class="orange">{{'時'|ruby:'とき'}}</span>、{{'家'|ruby:'うち'}}でゲームをします。
(<span class="orange">When</span> it rains, play game at home.)

(Rule: noun <span class="red">の</span> <span class="orange">{{'時'|ruby:'とき'}}</span>)

２７{{'歳'|ruby:'さい'}}<span class="red">の</span><span class="orange">{{'時'|ruby:'とき'}}</span>、{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}しました。
(<span class="orange">When</span> I was 27 years old, I married.)

{{'新'|ruby:'しん'}}{{'聞'|ruby:'ぶん'}}を{{'読'|ruby:'よ'}}む<span class="orange">{{'時'|ruby:'とき'}}</span>、{{'眼'|ruby:'め'}}{{'鏡'|ruby:'がね'}}をかけます。
(<span class="orange">When</span> reading newspaper, put on eyeglasses.)

(Rule: verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 <span class="orange">{{'時'|ruby:'とき'}}</span>)

{{'言'|ruby:'こと'}}{{'葉'|ruby:'ば'}}がわからない<span class="orange">{{'時'|ruby:'とき'}}</span>、{{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}を{{'使'|ruby:'つか'}}います。
(<span class="orange">When</span> don't understand the word, use the dictionary.)

(Rule: verb ない形 <span class="orange">{{'時'|ruby:'とき'}}</span>)

Form | Meaning | Example
-- | -- | --
{{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形|これからします<br/>(Going to do)|ご{{'飯'|ruby:'はん'}}を<span class="red">{{'食'|ruby:'た'}}べる</span><span class="orange">{{'時'|ruby:'とき'}}</span>、「いただきます」と{{'言'|ruby:'い'}}います
た形|もうしました/{{'終'|ruby:'お'}}わりました<br/>(Almost done / already finished)|ご{{'飯'|ruby:'はん'}}を<span class="red">{{'食'|ruby:'た'}}べた</span><span class="orange">{{'時'|ruby:'とき'}}</span>、「ごちそうさま」と{{'言'|ruby:'い'}}います

{.table .table-hover .table-sm .align-text-bottom}

{{'朝'|ruby:'あさ'}}<span class="red">{{'起'|ruby:'お'}}きた</span><span class="orange">{{'時'|ruby:'とき'}}</span>、「おはよう」と{{'言'|ruby:'い'}}います。
(<span class="orange">When</span> <span class="red">woke up</span>, say "Good morning".)

{{'夜'|ruby:'よる'}}<span class="red">{{'寝'|ruby:'ね'}}る</span><span class="orange">{{'時'|ruby:'とき'}}</span>、「おやすみなさい」と{{'言'|ruby:'い'}}います。
(<span class="orange">When</span> <span class="red">going to sleep</span>, say "Good night".)

<blockquote class="blockquote-qna">

Q: {{'暇'|ruby:'ひま'}}な{{'時'|ruby:'とき'}}、<span class="orange">{{'何'|ruby:'なに'}}</span>をしますか。
(When free, <span class="orange">what</span> do you do?)

A: ゲームをします。
(Play game.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'頭'|ruby:'あたま'}}が{{'痛'|ruby:'いた'}}い{{'時'|ruby:'とき'}}、<span class="orange">どう</span>しますか。
(When headache, <span class="orange">what</span> to do?)

A: {{'寝'|ruby:'ね'}}ます。
(Sleep.)

</blockquote>

## Event {#event}

{{5|jlpt}} {{'E02.L23'|icls}}

このボタンを{{'押'|ruby:'お'}}します。お{{'茶'|ruby:'ちゃ'}}が{{'出'|ruby:'で'}}ます。
(Press this button. The tea comes out.)

このボタンを{{'押'|ruby:'お'}}す<span class="red">と</span>、お{{'茶'|ruby:'ちゃ'}}が{{'出'|ruby:'で'}}ます。
(Press this button <span class="red">to cause</span> the tea to come out.)

(Rule: verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 <span class="red">と</span>、event)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The event mentioned after this <span class="red">と</span> particle will definitely happen and cannot be controlled. The event should be an involuntary verb ({{'無'|ruby:'む'}}{{'意'|ruby:'い'}}{{'識'|ruby:'し'}}{{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}}), such as:

- なります (to become)
- あります (to exist (inanimate object))
- います (to exist (animate object))
- {{'出'|ruby:'で'}}ます (to come out)
- {{'動'|ruby:'うご'}}きます (to move / to work)
</div>

ここを{{'回'|ruby:'まわ'}}す<span class="red">と</span>、お{{'釣'|ruby:'つ'}}りが{{'出'|ruby:'で'}}ます。
(Turn this, <span class="red">(so that)</span> the (money) change comes out.)

{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}する<span class="red">と</span>、{{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}が{{'上'|ruby:'じょう'}}{{'手'|ruby:'ず'}}になります。
(Study, <span class="red">(so that)</span> you become good at Japanese language.)

{{'一'|ruby:'いっ'}}{{'緒'|ruby:'しょ'}}にゲームする<span class="red">と</span>、{{'楽'|ruby:'たの'}}しくなります。
(Play game together, <span class="red">(so that)</span> it becomes enjoyable.)

<blockquote class="blockquote-qna">

Q: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は<span class="orange">どこ</span>にありますか。
(<span class="orange">Where</span> is the hospital at?)

A1: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、まっすぐ{{'行'|ruby:'い'}}く<span class="red">と</span>、{{'左'|ruby:'ひだり'}}にあります。
(As for the hospital, go straight, <span class="red">(so that)</span> it is on your left.)

A2: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、{{'交'|ruby:'こう'}}{{'差'|ruby:'さ'}}{{'点'|ruby:'てん'}}を{{'左'|ruby:'ひだり'}}へ{{'曲'|ruby:'ま'}}がる<span class="red">と</span>、{{'右'|ruby:'みぎ'}}にあります。
(As for the hospital, turn left at the crossroads, <span class="red">(so that)</span> it is on your right.)

A3: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、{{'交'|ruby:'こう'}}{{'差'|ruby:'さ'}}{{'点'|ruby:'てん'}}を{{'渡'|ruby:'わた'}}る<span class="red">と</span>、{{'右'|ruby:'みぎ'}}にあります。
(As for the hospital, cross the crossroads, <span class="red">(so that)</span> it is on your right.)

A4: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、{{'二'|ruby:'ふた'}}つ{{'目'|ruby:'め'}}の{{'角'|ruby:'かど'}}を{{'左'|ruby:'ひだり'}}へ{{'曲'|ruby:'ま'}}がる<span class="red">と</span>、{{'右'|ruby:'みぎ'}}にあります。
(As for the hospital, turn left at the second corner, <span class="red">(so that)</span> it is on your right.)

A5: {{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、まっすぐ１００メートル{{'行'|ruby:'い'}}く<span class="red">と</span>、{{'右'|ruby:'みぎ'}}にあります。
(As for the hospital, go straight for 100 meters, <span class="red">(so that)</span> it is on your right.)

</blockquote>

{{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}は、{{'交'|ruby:'こう'}}{{'差'|ruby:'さ'}}{{'点'|ruby:'てん'}}を{{'渡'|ruby:'わた'}}っ<span class="orange">て</span>、{{'二'|ruby:'ふた'}}つ{{'目'|ruby:'め'}}の{{'角'|ruby:'かど'}}を{{'右'|ruby:'みぎ'}}へ{{'曲'|ruby:'ま'}}がる<span class="red">と</span>、{{'左'|ruby:'ひだり'}}にあります。
(As for the hospital, cross the crossroads, then turn right at the second corner, <span class="red">(so that)</span> it is on your left.)

(Rule: verb て形、 verb て形、 ... verb {{'辞'|ruby:'じ'}}{{'書'|ruby:'しょ'}}形 <span class="red">と</span>、event)

## Favor {#favor}

{{5|jlpt}} {{'E02.L24'|icls}}

{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さんは［{{'私'|ruby:'わたし'}}に］チョコレートを<span class="orange">くれました</span>。
(Tanaka <span class="orange">gave (me)</span> a chocolate.)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
<span class="orange">くれます</span> implies giving to the speaker, hence 「{{'私'|ruby:'わたし'}}に」 is omitted.
</div>

<blockquote class="blockquote-qna">

Q: この{{'帽'|ruby:'ぼう'}}{{'子'|ruby:'し'}}は<span class="orange">どう</span>しましたか。
(<span class="orange">What</span> about this hat?)

A: この{{'帽'|ruby:'ぼう'}}{{'子'|ruby:'し'}}は{{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}さん<span class="red">が</span><span class="orange">くれました</span>。
(Tanaka <span class="orange">gave (me)</span> this hat.)

(Rule: thing は person <span class="red">が</span> <span class="orange">くれました</span>)

</blockquote>

すみません、ペンを{{'貸'|ruby:'か'}}してください。
(Excuse me, please lend a pen.)

{{'私'|ruby:'わたし'}}は{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">に</span>ペンを{{'貸'|ruby:'か'}}して<span class="orange">もらいました</span>。
(I <span class="orange">requested</span> Yamada to lend a pen.)

(Rule: requester は requestee <span class="red">に</span> verb て形 <span class="orange">もらいました</span>)

{{'私'|ruby:'わたし'}}は{{'吉'|ruby:'よし'}}{{'沢'|ruby:'ざわ'}}さん<span class="red">に</span>サインを{{'書'|ruby:'か'}}いて<span class="orange">もらいました</span>。
(I <span class="orange">requested</span> Yoshizawa to write a signature.)

<blockquote class="blockquote-qna">

Q: みんなさんは{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}や{{'家'|ruby:'か'}}{{'族'|ruby:'ぞく'}}<span class="red">に</span>{{'何'|ruby:'なに'}}をして<span class="orange">もらいました</span>か。
(Everyone, what did you <span class="orange">request</span> your friends and family to do?)

A: {{'私'|ruby:'わたし'}}は{{'妹'|ruby:'いもうと'}}<span class="red">に</span>{{'料'|ruby:'りょう'}}{{'理'|ruby:'り'}}を{{'作'|ruby:'つく'}}って<span class="orange">もらいました</span>。
(I <span class="orange">requested</span> my sister to cook meals.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'日'|ruby:'に'}}{{'本'|ruby:'ほん'}}{{'語'|ruby:'ご'}}が{{'上'|ruby:'じょう'}}{{'手'|ruby:'ず'}}ですね。{{'誰'|ruby:'だれ'}}<span class="red">に</span>{{'教'|ruby:'おし'}}えて<span class="orange">もらいました</span>か。
(You are good at Japanese language. Who did you <span class="orange">request</span> to teach you?)

A: {{'田'|ruby:'た'}}{{'中'|ruby:'なか'}}{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}<span class="red">に</span>{{'教'|ruby:'おし'}}えて<span class="orange">もらいました</span>。
(<span class="orange">Requested</span> teacher Tanaka to teach me.)

</blockquote>

<span class="orange">どうぞ</span>、この{{'傘'|ruby:'かさ'}}を{{'使'|ruby:'つか'}}ってください。
(Please <span class="orange">(feel free to)</span> use this umbrella.)

{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">は</span>{{'傘'|ruby:'かさ'}}を{{'貸'|ruby:'か'}}して<span class="orange">くれました</span>。
(Yamada <span class="orange">offered (me)</span> to lend the umbrella.)

(Rule: person <span class="red">が/は</span> verb て形 <span class="orange">くれました</span>)

<blockquote class="blockquote-qna">

Q: {{'誰'|ruby:'だれ'}}<span class="red">が</span>{{'傘'|ruby:'かさ'}}を{{'貸'|ruby:'か'}}して<span class="orange">くれました</span>か。
(Who <span class="orange">offered (you)</span> to lend the umbrella.)

A: {{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">が</span>{{'傘'|ruby:'かさ'}}を{{'貸'|ruby:'か'}}して<span class="orange">くれました</span>。
(Yamada <span class="orange">offered (me)</span> to lend the umbrella.)

</blockquote>

{{'私'|ruby:'わたし'}}は{{'山'|ruby:'やま'}}{{'田'|ruby:'だ'}}さん<span class="red">に</span>{{'道'|ruby:'みち'}}を{{'教'|ruby:'おし'}}えて<span class="orange">{{'上'|ruby:'あ'}}げました</span>。
(I <span class="orange">offered</span> Yamada to tell the way.)

(Rule: giver は ［receiver <span class="red">に</span>］ verb て形 <span class="orange">{{'上'|ruby:'あ'}}げました</span>)

{{'私'|ruby:'わたし'}}は{{'友'|ruby:'とも'}}{{'達'|ruby:'だち'}}を{{'家'|ruby:'うち'}}へ{{'送'|ruby:'おく'}}って<span class="orange">{{'上'|ruby:'あ'}}げました</span>。
(I <span class="orange">offered</span> my friend to pick him up from his house.)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The following verbs use the 「person を place へ ~」 pattern:

- {{'送'|ruby:'おく'}}ります (to escort)
- {{'連'|ruby:'つ'}}れて{{'行'|ruby:'い'}}きます (to take someone)
- {{'連'|ruby:'つ'}}れて{{'来'|ruby:'き'}}ます (to bring someone)
- {{'迎'|ruby:'むか'}}えに{{'行'|ruby:'い'}}きます (to go to meet someone)
- {{'迎'|ruby:'むか'}}えに{{'来'|ruby:'き'}}ます (to come to meet someone)
</div>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
The following verbs use the 「thing を place へ ~」 pattern:

- {{'持'|ruby:'も'}}って{{'行'|ruby:'い'}}きます (to take something)
- {{'持'|ruby:'も'}}って{{'来'|ruby:'き'}}ます (to bring something)
</div>

## If & Even {#if-even}

{{5|jlpt}} {{'E02.L25'|icls}}

<span class="red">［もし］</span>お{{'金'|ruby:'かね'}}がたくさんあった<span class="red">ら</span>、
{{'大'|ruby:'おお'}}きい{{'家'|ruby:'うち'}}を{{'買'|ruby:'か'}}いたいです。
(<span class="red">If</span> I have a lot of money, I want to buy a big house.)

(Rule: <span class="red">［もし］</span> verb た形 <span class="red">ら</span>、)

<span class="red">［もし］</span>{{'宿'|ruby:'しゅく'}}{{'題'|ruby:'だい'}}がわからなかった<span class="red">ら</span>、
{{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}に{{'聞'|ruby:'き'}}きます。
(<span class="red">If</span> I don't understand the homework, I ask the teacher.)

(Rule: <span class="red">［もし］</span> verb なかった形 <span class="red">ら</span>、)

<span class="red">［もし］</span>{{'天'|ruby:'てん'}}{{'気'|ruby:'き'}}がよかった<span class="red">ら</span>、
{{'山'|ruby:'やま'}}に{{'登'|ruby:'のぼ'}}ります。
(<span class="red">If</span> the weather is good, I climb the mountain.)

(Rule: <span class="red">［もし］</span> い adjective た形 <span class="red">ら</span>、)

<span class="red">［もし］</span>{{'暇'|ruby:'ひま'}}だった<span class="red">ら</span>、
{{'手'|ruby:'て'}}{{'伝'|ruby:'つだ'}}ってください。
(<span class="red">If</span> you are free, please help.)

(Rule: <span class="red">［もし］</span> な adjective た形 <span class="red">ら</span>、)

<span class="red">［もし］</span>タクシーだった<span class="red">ら</span>、
{{'家'|ruby:'うち'}}まで２０{{'分'|ruby:'ぷん'}}かかります。
(<span class="red">If</span> it is (by) taxi, it will take 20 minutes to (reach) the house.)

(Rule: <span class="red">［もし］</span> noun た形 <span class="red">ら</span>、)

<blockquote class="blockquote-qna">

Q: ［もし］{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}ったら
<span class="orange">{{'何'|ruby:'なに'}}</span>をしますか。
(If it rains, <span class="orange">what</span> to do?)

A: ゲームをします。
(Play game.)

</blockquote>

<blockquote class="blockquote-qna">

Q: ［もし］{{'会'|ruby:'かい'}}{{'議'|ruby:'ぎ'}}の{{'時'|ruby:'とき'}}{{'眠'|ruby:'ねむ'}}かったら、
<span class="orange">どう</span>をしますか。
(If it gets sleepy during a meeting, <span class="orange">what</span> to do?)

A: {{'顔'|ruby:'かお'}}を{{'洗'|ruby:'あら'}}います。
(Wash face.)

</blockquote>

Type of 「たら」 | Use | Example
-- | -- | --
{{'仮'|ruby:'か'}}{{'定'|ruby:'てい'}}の「たら」|if|{{'明日'|ruby:'あした'}}{{'晴'|ruby:'は'}}れたら、バーベキューします (If it is sunny tomorrow, I do BBQ)
{{'完'|ruby:'かん'}}{{'了'|ruby:'りょう'}}の「たら」|after|{{'勉'|ruby:'べん'}}{{'強'|ruby:'きょう'}}{{'終'|ruby:'お'}}わったら、ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べます (If I finish study, I will eat meal)

{.table .table-hover .table-sm .align-text-bottom}

{{'大'|ruby:'だい'}}{{'学'|ruby:'がく'}}を{{'出'|ruby:'で'}}たら、{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}します。
(If I graduate from university, I will marry.)

７{{'時'|ruby:'じ'}}になったら、ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べます。
(If it becomes 7 o' clock, I will eat meal.)

{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}って<span class="red">も</span>、{{'行'|ruby:'い'}}きます。
(<span class="red">Even</span> it rains, I will go.)

(Rule: verb て形 <span class="red">も</span>、)

{{'安'|ruby:'やす'}}くて<span class="red">も</span>、{{'買'|ruby:'か'}}いません。
(<span class="red">Even</span> it is cheap, I won't buy.)

(Rule: い adjective て形 <span class="red">も</span>、)

{{'下'|ruby:'へ'}}{{'手'|ruby:'た'}}で<span class="red">も</span>、{{'歌'|ruby:'うた'}}が{{'好'|ruby:'す'}}きです。
(<span class="red">Even</span> I am poor at it, I still like the song.)

(Rule: noun / な adjective て形 <span class="red">も</span>、)

{{'熱'|ruby:'ねつ'}}があった<span class="orange">ら</span>、{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}を{{'休'|ruby:'やす'}}みます。
(<span class="orange">If</span> I have fever, I take a rest from school.)

{{'熱'|ruby:'ねつ'}}があって<span class="orange">も</span>、{{'学'|ruby:'がっ'}}{{'校'|ruby:'こう'}}へ{{'行'|ruby:'い'}}きます。
(<span class="orange">Even</span> I have fever, I will go to school.)

<blockquote class="blockquote-qna">

Q: {{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}って<span class="orange">も</span>、{{'行'|ruby:'い'}}きますか。
(<span class="orange">Even</span> it rains, will you go?)

A1: はい、{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}って<span class="orange">も</span>、{{'行'|ruby:'い'}}きます。
(Yes, <span class="orange">even</span> it rains, I will go.)

A2: いいえ、{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}った<span class="orange">ら</span>、{{'行'|ruby:'い'}}きません。
(No, <span class="orange">if</span> it rains, I won't go.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'安'|ruby:'やす'}}かった<span class="orange">ら</span>、{{'買'|ruby:'か'}}いますか。
(<span class="orange">If</span> it is cheap, will you buy?)

A1: はい、{{'安'|ruby:'やす'}}かった<span class="orange">ら</span>、{{'買'|ruby:'か'}}います。
(Yes, <span class="orange">if</span> it is cheap, I will buy.)

A2: いいえ、{{'安'|ruby:'やす'}}くて<span class="orange">も</span>、{{'買'|ruby:'か'}}いません。
(No, <span class="orange">even</span> it is cheap, I won't buy.)

</blockquote>

## んです {#n-desu}

{{4|jlpt}} {{'I01.L26'|icls}}

{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}した<span class="orange">ん</span>ですか。
(Did you get married?)

(Rule: {{'普'|ruby:'ふ'}}{{'通'|ruby:'つう'}}形 <span class="orange">ん</span>ですか)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
んです (in the question) here is used when questioning a guess or confirming something, upon seeing or hearing about it.
</div>

{{'旅'|ruby:'りょ'}}{{'行'|ruby:'こう'}}する<span class="orange">ん</span>ですか。
(Are you going for a trip?)

{{'眠'|ruby:'ねむ'}}い<span class="orange">ん</span>ですか。
(Are you sleepy?)

わからない<span class="orange">ん</span>ですか。
(Don't you understand?)

{{'雨'|ruby:'あめ'}}が{{'降'|ruby:'ふ'}}っている<span class="orange">ん</span>ですか。
(Is it raining?)

{{'暇'|ruby:'ひま'}}な<span class="orange">ん</span>ですか。
(Are you free?)

かぜな<span class="orange">ん</span>ですか。
(Are you sick?)

どこへ{{'行'|ruby:'い'}}く<span class="orange">ん</span>ですか。
(Where are you going to?)

(Rule: {{'疑'|ruby:'ぎ'}}{{'問'|ruby:'もん'}}{{'字'|ruby:'じ'}} ... <span class="orange">ん</span>ですか)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
んです (in the question) here is used when there is an interest or curiosity.
</div>

{{'誰'|ruby:'だれ'}}と{{'行'|ruby:'い'}}く<span class="orange">ん</span>ですか。
(Who are you going with?)

<blockquote class="blockquote-qna">

Q: いつ{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}した<span class="orange">ん</span>ですか。
(When did you get married?)

A: {{'去'|ruby:'きょ'}}{{'年'|ruby:'ねん'}}の{{'八'|ruby:'はち'}}{{'月'|ruby:'がつ'}}{{'結'|ruby:'けっ'}}{{'婚'|ruby:'こん'}}しました。
(I got married on August last year.)

</blockquote>

<blockquote class="blockquote-qna">

Q: どうした<span class="orange">ん</span>ですか。
(What happened?)

A: {{'鍵'|ruby:'かぎ'}}をなくした<span class="orange">ん</span>です。
(I lost the key.)

</blockquote>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
んです in the answer here is used to specify the reason (as a direct response to the question).
</div>

<blockquote class="blockquote-qna">

Q: どうした<span class="orange">ん</span>ですか。
(What happened?)

A: {{'電'|ruby:'でん'}}{{'車'|ruby:'しゃ'}}が{{'来'|ruby:'こ'}}ない<span class="orange">ん</span>です。
(The train doesn't come.)

</blockquote>

<blockquote class="blockquote-qna">

Q: どうして{{'掃'|ruby:'そう'}}{{'除'|ruby:'じ'}}しない<span class="orange">ん</span>ですか。
(Why don't you clean up?)

A: {{'仕'|ruby:'し'}}{{'事'|ruby:'ごと'}}が{{'忙'|ruby:'いそが'}}しい<span class="orange">ん</span>です。
(I am busy with work.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'先'|ruby:'せん'}}{{'生'|ruby:'せい'}}もパーティーへ{{'行'|ruby:'い'}}きましたか。
(Did the teacher also go to the party?)

A: いいえ、{{'行'|ruby:'い'}}きませんでした。
{{'病'|ruby:'びょう'}}{{'院'|ruby:'いん'}}へ{{'行'|ruby:'い'}}く{{'用'|ruby:'よう'}}{{'事'|ruby:'じ'}}があった<span class="orange">ん</span>です。
(No, didn't go. There was an errand that requires going to the hospital.)

</blockquote>

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
んです in the answer here is used to explain the reason.
Japanese often state the reason of not doing something out of courtesy.
</div>

<blockquote class="blockquote-qna">

Q: {{'朝'|ruby:'あさ'}}ご{{'飯'|ruby:'はん'}}を{{'食'|ruby:'た'}}べましたか。
(Did you eat breakfast?)

A: いいえ、{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}がなかった<span class="orange">ん</span>です。
(No, I didn't have the time.)

</blockquote>

<blockquote class="blockquote-qna">

Q: {{'毎'|ruby:'まい'}}{{'朝'|ruby:'あさ'}}{{'新'|ruby:'しん'}}{{'聞'|ruby:'ぶん'}}を{{'読'|ruby:'よ'}}みますか。
(Do you read newspaper every morning?)

A1: はい、{{'読'|ruby:'よ'}}みます。
いろいろなニュースを{{'読'|ruby:'よ'}}むのが{{'好'|ruby:'す'}}き<span class="orange">ん</span>です。
(Yes, I read. I like reading various news.)

A2: いいえ、{{'読'|ruby:'よ'}}みません。
{{'朝'|ruby:'あさ'}}は{{'忙'|ruby:'いそが'}}しくて{{'時'|ruby:'じ'}}{{'間'|ruby:'かん'}}がない<span class="orange">ん</span>です。
(No, I don't read. It is busy in the morning and I don't have the time.)

</blockquote>

{{'駅'|ruby:'えき'}}へ{{'行'|ruby:'い'}}きたい<span class="orange">ん</span>です<span class="orange">が</span>、
{{'道'|ruby:'みち'}}を{{'教'|ruby:'おし'}}えて<span class="red">いただけませんか</span>。
(<span class="orange">As (because)</span> I want to go to the station, could you tell me the way <span class="red">please?</span>)

(Rule: reason <span class="orange">ん</span>です<span class="orange">が</span>、て形 <span class="red">いただけませんか</span>)

<div class="alert alert-info">
<i class="bi bi-info-circle"></i>
んです here is used to specify the reason (before asking the question).

いただけませんか here is an honorific ({{'丁'|ruby:'てい'}}{{'寧'|ruby:'ねい'}}) used to ask something politely.
</div>

## Seek advice {#seek-advice}

{{4|jlpt}} {{'I01.L26'|icls}}

<blockquote class="blockquote-qna">

Q: {{'噴'|ruby:'ふん'}}{{'水'|ruby:'すい'}}ショーを{{'見'|ruby:'み'}}たいんですが、
{{'何'|ruby:'なん'}}{{'時'|ruby:'じ'}}に{{'行'|ruby:'い'}}っ<span class="orange">たら</span><span class="red">いいです</span>か。
(As I want to see the fountain show, what time <span class="red">would be good</span> to go (there)?)

A: ９{{'時'|ruby:'じ'}}に{{'行'|ruby:'い'}}っ<span class="orange">たら</span><span class="red">いいです</span>よ。
(9 o' clock <span class="red">would be good</span> to go.)

</blockquote>

(Rule: verb た形 ら </span><span class="red">いいです</span>か)
