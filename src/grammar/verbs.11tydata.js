module.exports = {
    tableOfContent: [
        {id: 'groups', name: 'Groups', children: [
            {id: 'godan', name: 'Godan'},
            {id: 'ichidan', name: 'Ichidan'},
            {id: 'irregular', name: 'Irregular'},
        ]},
        {id: 'forms', name: 'Forms', children: [
            {id: 'stems', name: 'Stems'},
            {id: 'conjugations', name: 'Conjugations', children: [
                {id: 'imperfective', name: 'Imperfective'},
                {id: 'perfective', name: 'Perfective'},
                {id: 'gerundive', name: 'Gerundive'},
                {id: 'continuative', name: 'Continuative'},
                {id: 'conditional', name: 'Conditional'},
                {id: 'volitional', name: 'Volitional'},
                {id: 'passive', name: 'Passive'},
                {id: 'causative', name: 'Causative'},
                {id: 'potential', name: 'Potential'},
            ]},
        ]},
        {id: 'auxiliary', name: 'Auxiliary', children: [
            {id: 'auxiliary-pure', name: 'Pure'},
            {id: 'auxiliary-helper', name: 'Helper'},
        ]},
        {id: 'transitive-intransitive', name: 'Transitive & Intransitive'},
        {id: 'will-involuntary', name: 'Will & Involuntary'},
    ],
}
