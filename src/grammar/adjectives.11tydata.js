module.exports = {
    tableOfContent: [
        {id: 'categories', name: 'Categories', children: [
            {id: 'i-adjective', name: 'i-adjective'},
            {id: 'na-adjective', name: 'na-adjective'},
        ]},
        {id: 'forms', name: 'Forms', children: [
            {id: 'stems', name: 'Stems'},
            {id: 'conjugations', name: 'Conjugations', children: [
                {id: 'imperfective', name: 'Imperfective'},
                {id: 'perfective', name: 'Perfective'},
                {id: 'gerundive', name: 'Gerundive'},
                {id: 'conditional', name: 'Conditional'},
                {id: 'volitional', name: 'Volitional'},
                {id: 'adverbial', name: 'Adverbial'},
                {id: 'degree-ness', name: 'Degree-ness'},
            ]},
        ]},
    ],
}
