---
layout: simple.html
title: 'Grammar: Kana'
navbar:
  grammar: 'active'
---

# Kana

## Gojuon {{'五'|ruby:'ご'}}{{'十'|ruby:'じゅう'}}{{'音'|ruby:'おん'}}

ン|ワ|ラ|ヤ|マ|ハ|ナ|タ|サ|カ|ア
-|-|-|-|-|-|-|-|-|-|-
ん<br/>ン<br/>n|わ<br/>ワ<br/>wa|ら<br/>ラ<br/>ra|や<br/>ヤ<br/>ya|ま<br/>マ<br/>ma|は<br/>ハ<br/>ha|な<br/>ナ<br/>na|た<br/>タ<br/>ta|さ<br/>サ<br/>sa|か<br/>カ<br/>ka|あ<br/>ア<br/>a
-|-<br/>ウィ<br/>wi|り<br/>リ<br/>ri|-|み<br/>ミ<br/>mi|ひ<br/>ヒ<br/>hi|に<br/>ニ<br/>ni|ち<br/>チ<br/>chi|し<br/>シ<br/>shi|き<br/>キ<br/>ki|い<br/>イ<br/>i
-|-|る<br/>ル<br/>ru|ゆ<br/>ユ<br/>yu|む<br/>ム<br/>mu|ふ<br/>フ<br/>fu|ぬ<br/>ヌ<br/>nu|つ<br/>ツ<br/>tsu|す<br/>ス<br/>su|く<br/>ク<br/>ku|う<br/>ウ<br/>u
-|-<br/>ウェ<br/>we|れ<br/>レ<br/>re|-|め<br/>メ<br/>me|へ<br/>ヘ<br/>he|ね<br/>ネ<br/>ne|て<br/>テ<br/>te|せ<br/>セ<br/>se|け<br/>ケ<br/>ke|え<br/>エ<br/>e
-|を<br/>ヲ<br/>wo|ろ<br/>ロ<br/>ro|よ<br/>ヨ<br/>yo|も<br/>モ<br/>mo|ほ<br/>ホ<br/>ho|の<br/>ノ<br/>no|と<br/>ト<br/>to|そ<br/>ソ<br/>so|こ<br/>コ<br/>ko|お<br/>オ<br/>o

{.table .table-hover .table-sm .table-bordered .align-middle}
