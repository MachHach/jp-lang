---
layout: reader.html
title: 'Misc: JLPT'
navbar:
  misc: 'active'
---

# JLPT

Refer to the [official site][jlpt] for more info.

[jlpt]: https://www.jlpt.jp/e/index.html

## N5 {#n5}

__Language Knowledge (Grammar) - Reading / {{'言'|ruby:'げん'}}{{'語'|ruby:'ご'}}{{'知'|ruby:'ち'}}{{'識'|ruby:'しき'}}（{{'文'|ruby:'ぶん'}}{{'法'|ruby:'ほう'}}）・{{'読'|ruby:'どっ'}}{{'解'|ruby:'かい'}}__

*32 multiple-choice questions, 50 minutes*

Section | Description | Questions
-- | -- | --
1|Identify which word to fill in the blank|16
2|Identify which word to fill in the specific blank|5
3|Identify which word to fill in the blanks in the article|5
4|Comprehend the article and answer|3
5|Comprehend the article and answer|2
6|Comprehend the diagram and answer|1

{.table .table-hover .table-sm}

__Listening / {{'聴'|ruby:'ちょう'}}{{'解'|ruby:'かい'}}__

*19 questions, 30 minutes*

Section | Description | Questions
-- | -- | --
1|Listen then answer the multiple-choice question|7
2|Listen then answer the multiple-choice question|6
3|Listen then answer what the person (specified in picture) is doing|5
4|Listen then answer (no picture)|1

{.table .table-hover .table-sm}

__Language Knowledge (Vocabulary) / {{'言'|ruby:'げん'}}{{'語'|ruby:'ご'}}{{'知'|ruby:'ち'}}{{'識'|ruby:'しき'}}（{{'文'|ruby:'も'}}{{'字'|ruby:'じ'}}・{{'語'|ruby:'ご'}}{{'彙'|ruby:'い'}}）__

*33-35 multiple-choice questions, 25 minutes*

Section | Description | Questions
-- | -- | --
1|Identify written hiragana from the kanji|10-12
2|Identify written kanji/katakana from the hiragana|8
3|Identify which word to fill in the blank|10
4|Identify which sentence has the same meaning|5

{.table .table-hover .table-sm}
