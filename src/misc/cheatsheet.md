---
layout: reader.html
title: 'Misc: Cheat sheet'
navbar:
  misc: 'active'
---

# Cheat sheet

## Verb {{'動'|ruby:'どう'}}{{'詞'|ruby:'し'}} {#verb}

<b>Groups</b>：{{'五'|ruby:'ご'}}{{'段'|ruby:'だん'}}、{{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}（<i>-e・-i</i>）、irregular（<i>{{'来'|ruby:'く'}}る・する</i>）

<b>{{'五'|ruby:'ご'}}{{'段'|ruby:'だん'}}「て・た」形rule</b>：いちり<span class="red">って</span>、みび<span class="red">んで</span>、き<span class="red">いて</span>、ぎ<span class="red">いで</span>、し<span class="red">して</span>

Type|{{'五'|ruby:'ご'}}{{'段'|ruby:'だん'}}{{furi.formalForm}}|{{'五'|ruby:'ご'}}{{'段'|ruby:'だん'}}{{furi.informalForm}}|{{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}{{furi.formalForm}}|{{'一'|ruby:'いち'}}{{'段'|ruby:'だん'}}{{furi.informalForm}}|{{'来'|ruby:'く'}}る{{furi.formalForm}}|{{'来'|ruby:'く'}}る{{furi.informalForm}}|する{{furi.formalForm}}|する{{furi.informalForm}}
--|--|--|--|--|--|--|--|--
Present|{{'飲'|ruby:'の'}}<span class="orange">み</span><span class="green">ます</span>|{{'飲'|ruby:'の'}}<span class="orange">む</span>|{{'食'|ruby:'た'}}べ<span class="green">ます</span>|{{'食'|ruby:'た'}}べ<span class="green">る</span>|{{'来'|ruby:'き'}}<span class="green">ます</span>|{{'来'|ruby:'く'}}<span class="green">る</span>|し<span class="green">ます</span>|す<span class="green">る</span>
Present negative|{{'飲'|ruby:'の'}}<span class="orange">み</span><span class="green">ません</span>|{{'飲'|ruby:'の'}}<span class="orange">ま</span><span class="green">ない</span>|{{'食'|ruby:'た'}}べ<span class="green">ません</span>|{{'食'|ruby:'た'}}べ<span class="green">ない</span>|{{'来'|ruby:'き'}}<span class="green">ません</span>|<span class="orange">{{'来'|ruby:'こ'}}</span><span class="green">ない</span>|し<span class="green">ません</span>|<span class="orange">ない</span>
Past|{{'飲'|ruby:'の'}}<span class="orange">み</span><span class="green">ました</span>|{{'飲'|ruby:'の'}}<span class="orange">んだ</span>|{{'食'|ruby:'た'}}べ<span class="green">ました</span>|{{'食'|ruby:'た'}}べ<span class="green">た</span>|{{'来'|ruby:'き'}}<span class="green">ました</span>|{{'来'|ruby:'き'}}<span class="green">た</span>|し<span class="green">ました</span>|し<span class="green">た</span>
Past negative|{{'飲'|ruby:'の'}}<span class="orange">み</span><span class="green">ませんでした</span>|{{'飲'|ruby:'の'}}<span class="orange">ま</span><span class="green">なかった</span>|{{'食'|ruby:'た'}}べ<span class="green">ませんでした</span>|{{'食'|ruby:'た'}}べ<span class="green">なかった</span>|{{'来'|ruby:'き'}}<span class="green">ませんでした</span>|<span class="orange">{{'来'|ruby:'こ'}}</span><span class="green">なかった</span>|し<span class="green">ませんでした</span>|<span class="orange">なかった</span>
て形|-|{{'飲'|ruby:'の'}}<span class="orange">んで</span>|-|{{'食'|ruby:'た'}}べ<span class="green">て</span>|-|{{'来'|ruby:'き'}}<span class="green">て</span>|-|し<span class="green">て</span>
Conditional past|-|{{'飲'|ruby:'の'}}<span class="orange">んだら</span>|-|{{'食'|ruby:'た'}}べ<span class="green">たら</span>|-|{{'来'|ruby:'き'}}<span class="green">たら</span>|-|し<span class="green">たら</span>
Volitional|{{'飲'|ruby:'の'}}<span class="orange">み</span><span class="green">ましょう</span>|{{'飲'|ruby:'の'}}<span class="orange">も</span><span class="green">う</span>|{{'食'|ruby:'た'}}べ<span class="green">ましょう</span>|{{'食'|ruby:'た'}}べ<span class="green">よう</span>|{{'来'|ruby:'き'}}<span class="green">ましょう</span>|<span class="orange">{{'来'|ruby:'こ'}}</span><span class="green">よう</span>|し<span class="green">ましょう</span>|し<span class="green">よう</span>
Potential|-|{{'飲'|ruby:'の'}}<span class="orange">め</span><span class="green">る</span>|-|{{'食'|ruby:'た'}}べ<span class="green">られる</span>|-|<span class="orange">{{'来'|ruby:'こ'}}</span><span class="green">られる</span>|-|<span class="orange">できる</span>

{.table .table-hover .table-sm .align-text-bottom}

## Adjective {{'形'|ruby:'けい'}}{{'容'|ruby:'よう'}}{{'詞'|ruby:'し'}}

<b>Groups</b>：「い」、「な」

Type|「い」{{furi.formalForm}}|「い」{{furi.informalForm}}|「な」{{furi.formalForm}}|「な」{{furi.informalForm}}
--|--|--|--|--
Present|安い<span class="green">です</span>|安い|静か<span class="green">です</span>|静か<span class="green">だ</span>
Present negative|安<span class="orange">く</span><span class="green">ないです</span>|安<span class="orange">く</span><span class="green">ない</span>|静か<span class="green">じゃありません</span>|静か<span class="green">じゃない</span>
Past|安<span class="orange">かった</span><span class="green">です</span>|安<span class="orange">かった</span>|静か<span class="green">でした</span>|静か<span class="green">だった</span>
Past negative|安<span class="orange">く</span><span class="green">なかったです</span>|安<span class="orange">く</span><span class="green">なかった</span>|静か<span class="green">じゃありませんでした</span>|静か<span class="green">じゃなかった</span>
て形|-|安<span class="orange">く</span><span class="green">て</span>|-|静か<span class="green">で</span>
Adverbial|-|安<span class="orange">く</span>|-|静か<span class="green">に</span>

{.table .table-hover .table-sm .align-text-bottom}
