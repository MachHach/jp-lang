const scrollToTop = function() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
};

const getScrollPosition = function() {
    return window.scrollY || window.scrollTop || document.documentElement.scrollTop;
};

const btnScrollTopClass = 'btn-scroll-top';
const btnScrollTop = document.querySelector(`.${btnScrollTopClass}`);
if (btnScrollTop) {
    const svgPath = document.querySelector(`.${btnScrollTopClass} path`);
    const svgPathLength = svgPath.getTotalLength();
    svgPath.style.transition = svgPath.style.WebkitTransition = 'none';
    svgPath.style.strokeDasharray = `${svgPathLength} ${svgPathLength}`;
    svgPath.style.strokeDashoffset = svgPathLength;
    svgPath.style.transition = svgPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';

    const updateScrollTopSvg = function() {
        const scrollPosition = getScrollPosition();
        const documentHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        const windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        const height = documentHeight - windowHeight;
        const svgPathLengthCurrent = svgPathLength - (scrollPosition * svgPathLength / height);
        svgPath.style.strokeDashoffset = svgPathLengthCurrent;
    }

    updateScrollTopSvg();
    const offset = 60;
    const visibilityModifier = 'active';

    window.addEventListener('scroll', function(event) {
        updateScrollTopSvg();
  
        const scrollPosition = getScrollPosition();
        if (scrollPosition > offset) {
            btnScrollTop.classList.add(visibilityModifier);
        } else {
            btnScrollTop.classList.remove(visibilityModifier);
        }
    }, false);
}
